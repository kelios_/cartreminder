/**
 * This file should be located here:
 * https://s3.amazonaws.com/shopify-apps/Plugins/SelectorPicker/selector-picker.js
 */
if (typeof(Spurit) === 'undefined') {
    var Spurit = {};
}
(function () {
    if (typeof(Spurit.selectorPickerEnabled) === 'undefined') {
        Spurit.selectorPickerEnabled = true;
    } else {
        return;
    }

    /**
     * This ID should be set using url-query:
     * http://site.com/?selector-picker={id}
     * And the same value will be sent to opener page
     * @see config.onSave
     * @type {number}
     */
    var scriptId = NaN;

    /**
     * This setting may be overloaded using url-query:
     * http://site.com/?...&settings={"selectPosition":true,"barMessage":"...","demoText":"...","disabledByDefault":true}
     * PS: json-string should be urlencoded (<?=rawurlencode(json_encode([...])?>)
     * @type {{selectPosition: boolean, barMessage: string, demoText: string, disabledByDefault: boolean}}
     */
    var settings = {
        selectPosition: false,  // If FALSE: just element selector will be obtained
        barMessage: 'Set this text using url query: <em>http://site.com/?...&settings={"barMessage"=>"..."}</em>',
        demoText: 'Set this text using url query: <em>http://site.com/?...&settings={"demoText"=>"..."}</em>',
        disabledByDefault: false   // If TRUE: selector picker disabled by default, and can be enabled by clicking the button on the top bar
    };

    /**
     * CSS-selector + One of these values = Final result of plugin
     * @type {string}
     */
    var POSITION_BEFORE = 'before',
        POSITION_INSIDE = 'inside',
        POSITION_AFTER = 'after';

    /**
     * **********************************************
     * ******************* Config *******************
     * **********************************************
     */
    var config = {
        // Will be called before any action in this plugin. E.g. may be used for displaying some alerts/popups etc.
        onLoad: function () {

        },

        // Requests initial values of selector and position from opener window
        loadInitial: function () {
            if (window.opener) {
                if (typeof(window.__selectorPickerMessageHandlerSet) === 'undefined') {
                    window.__selectorPickerMessageHandlerSet = true;
                    lib.wrElement(window).addHandler('message', function (e) {
                        var data = e.data || e.originalEvent.data;
                        if (typeof(data.id) === 'undefined' || data.id !== scriptId) {
                            return;
                        }
                        if (data.inital) {
                            if (data.inital.selector) {
                                var position = data.inital.position ? data.inital.position : POSITION_BEFORE;
                                Demo.show(
                                    document.querySelector(data.inital.selector),
                                    position
                                );
                                Result.set(data.inital.selector, position);
                            }
                        }
                    });
                }
                window.opener.postMessage({id: scriptId, loadInitial: true}, '*');
            }
        },

        //
        onSave: function (selector, position) {
            if (window.opener) {
                window.opener.postMessage({id: scriptId, selector: selector, position: position}, '*');
                if (window.confirm('Element position has been saved. Close the current window?')) {
                    window.close();
                }
            }
        },

        // Link or Button with attr [rel="save"] are required
        barHtml: '<div style="display:block;position:fixed;left:0;right:0;top:0;height:0;text-align:center">' +
        '<style type="text/css">.__selector-picker-btn:hover{background:#e6e6e6!important;border-color:#adadad!important;}</style>' +
        '<div style="display:inline-block;min-width:500px;padding:8px 14px;background:#fcf8e3;color:#000;line-height:1;text-align:center;font-size:14px;border-radius:0 0 4px 4px;border:1px solid #faebcc;border-top:0;box-shadow:0 1px 5px 0 rgba(0,0,0,0.4)">' +
        '[bar-message]<br>' +
        '<button type="button" rel="enable" class="__selector-picker-btn" style="font-size:14px;margin:5px 2px 0 2px;color:#333;background:#fff;border:1px solid #ccc;border-radius:4px;line-height:32px;padding:0 12px">Enable</button>' +
        '<button type="button" rel="save" class="__selector-picker-btn" style="font-size:14px;margin:5px 2px 0 2px;color:#333;background:#fff;border:1px solid #ccc;border-radius:4px;line-height:32px;padding:0 12px">Save</button>' +
        '</div>' +
        '</div>',
        barZIndex: 999999993,

        // Each side of hover rect. is separate element; CSS-props "display", "position", "z-index", "width", "height" will be set automatically
        hoverRectangleSidesCss: 'background:red',
        hoverRectangleZIndex: 999999991,

        // Using when settings.selectPosition is TRUE and demo-element may be placed before/inside/after selected element
        // CSS-property "position" will be set automatically
        demoStaticHtml: '<div style="display:block;margin:10px 0;background:#dff0d8;color:#333;border:1px solid #333;padding:10px;font-size:16px;font-weight:normal">' +
        '[demo-text]' +
        '</div>',

        // Using when settings.selectPosition is FALSE and demo-element is a rectangle that just highlighting selected element
        // Each side of this rect. is separate element; CSS-props "display", "position", "z-index", "width", "height" will be set automatically
        demoOverlaySidesCSS: 'background:green',
        demoOverlayZIndex: 999999990,

        // CSS-props "position", "z-index" and "left/top" will be set automatically
        menuHtml: '<div style="display:block;background:#fcf8e3;color:#333;padding:4px 16px;border-radius:4px;border:1px solid #faebcc;box-shadow:0 1px 5px 0 rgba(0,0,0,0.4)">' +
        '<a href="#" rel="' + POSITION_AFTER + '" style="display:block;line-height:24px;color:#333;">After element</a>' +
        '<a href="#" rel="' + POSITION_BEFORE + '" style="display:block;line-height:24px;color:#333;">Before element</a>' +
        '</div>',
        menuZIndex: 999999992
    };

    if(typeof(Spurit.selectorPickerConfig) !== 'undefined'){
        for(var key in Spurit.selectorPickerConfig){
            config[key] = Spurit.selectorPickerConfig[key];
        }
    }
    /**
     * **********************************************
     * ***************** Config END *****************
     * **********************************************
     */

    /**
     * @type {{}}
     */
    var getVars = window.location.search.replace('?', '').split('&').reduce(
        function (p, e) {
            var a = e.split('=');
            p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
            return p;
        },
        {}
    );
    if (typeof(getVars['selector-picker']) !== 'undefined') {
        scriptId = parseInt(getVars['selector-picker']);
    }
    if (isNaN(scriptId)) {
        console.error('Its required to set script id using url query: http://.../?selector-picker={int}');
    }
    if (typeof(getVars.settings) !== 'undefined') {
        getVars.settings = JSON.parse(getVars.settings);
        if (getVars.settings) {
            if (typeof(getVars.settings.selectPosition) !== 'undefined') {
                settings.selectPosition = !!(getVars.settings.selectPosition);
            }
            if (typeof(getVars.settings.barMessage) !== 'undefined') {
                settings.barMessage = getVars.settings.barMessage;
            }
            if (typeof(getVars.settings.demoText) !== 'undefined') {
                settings.demoText = getVars.settings.demoText;
            }
            if (typeof(getVars.settings.disabledByDefault) !== 'undefined') {
                settings.disabledByDefault = !!(getVars.settings.disabledByDefault);
            }
        }
    }

    /**
     * Plugin library
     */
    var lib = new function () {
        /**
         * @returns {{left: number, top: number}}
         */
        this.scrollPos = function () {
            var html = document.documentElement;
            var body = document.body;
            return {
                left: (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0),
                top: (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
            };
        };

        /**
         * @param {Array|NodeList|HTMLCollection} list
         * @returns {{forEach: function()}}
         */
        this.wrList = function (list) {
            return new (function (list) {
                /**
                 * @param {function(*, Number, Object)} callback
                 * @param {Object} [thisArg]
                 */
                this.forEach = function (callback, thisArg) {
                    if (list == null) {
                        console.error('Argument is null or not defined');
                        return;
                    }
                    var O = Object(list);
                    if (typeof(O.length) === 'undefined') {
                        console.error(O + ' don\'t have length attribute');
                        return;
                    }
                    var len = O.length >>> 0;

                    if (typeof(callback) !== 'function') {
                        console.error('"' + callback + '" is not a function');
                        return;
                    }
                    var T;
                    if (arguments.length > 1) T = thisArg;
                    var k = 0;
                    while (k < len) {
                        var kValue;
                        if (k in O) {
                            kValue = O[k];
                            if (callback.call(T, kValue, k, O) === false) break;
                        }
                        k++;
                    }
                };
            })(list);
        };

        /**
         * @param {Window|Node} element
         * @returns {{addHandler: function(), removeHandler: function(), onChangeContents: function(), size: function(), pos: function(), getSelector: function(), markAsSpecial: function()}}
         */
        this.wrElement = function (element) {
            return new (function (element) {
                /**
                 * @param    {string}    event
                 * @param    {function}    handler
                 * @param    {boolean}    useCapture
                 */
                this.addHandler = function (event, handler, useCapture) {
                    if (event === '') return;
                    if (element.addEventListener) {
                        element.addEventListener(event, handler, useCapture ? useCapture : false);
                    } else if (element.attachEvent) {
                        element.attachEvent('on' + event, handler);
                    } else {
                        element['on' + event] = handler;
                    }
                };

                /**
                 * @param {function(<MutationRecord>[])} handler
                 */
                this.onChangeContents = function (handler) {
                    if (element === window || element === document) {
                        lib.onLoad(function () {
                            lib.wrElement(document.body).onChangeContents(handler);
                        });
                        return;
                    }
                    new MutationObserver(
                        function (mutations) {
                            handler(mutations);
                        }
                    ).observe(
                        element,
                        {childList: true, subtree: true}
                    );
                };

                /**
                 * @param    {string}    event
                 * @param    {function}    handler
                 * @param    {boolean}    useCapture
                 */
                this.removeHandler = function (event, handler, useCapture) {
                    if (event === '') return;
                    if (this.addEventListener) {
                        this.removeEventListener(event, handler, useCapture ? useCapture : false);
                    } else if (this.attachEvent) {
                        this.detachEvent('on' + event, handler);
                    } else {
                        delete(this['on' + event]);
                    }
                };

                /**
                 * @returns {{w: Number, h: Number}}
                 */
                this.size = function () {
                    if (element === window) {
                        return {
                            w: window.innerWidth ? window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.offsetWidth),
                            h: window.innerHeight ? window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.offsetHeight)
                        };
                    } else if (typeof(element.offsetWidth) !== 'undefined') {
                        return {w: element.offsetWidth, h: element.offsetHeight};
                    } else {
                        return {w: 0, h: 0};
                    }

                };

                /**
                 * @returns {{x: Number, y: Number}}
                 */
                this.pos = function () {
                    /**
                     * @param {Node} element
                     * @returns {{x: number, y: number}}
                     */
                    function getOffsetRect(element) {
                        // Вычисление координат объекта "правильным методом"

                        // Получить ограничивающий прямоугольник для элемента
                        var box = element.getBoundingClientRect();

                        // Задать две переменных для удобства
                        var body = document.body;
                        var docElem = document.documentElement;

                        // Вычислить прокрутку документа
                        var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

                        // Документ(html или body) бывает сдвинут относительно окна (IE). Получаем этот сдвиг.
                        var clientTop = docElem.clientTop || body.clientTop || 0;
                        var clientLeft = docElem.clientLeft || body.clientLeft || 0;

                        // Прибавляем к координатам относительно окна прокрутку и вычитаем сдвиг html/body, чтобы получить координаты относительно документа
                        var top = box.top + scrollTop - clientTop;
                        var left = box.left + scrollLeft - clientLeft;

                        return {x: Math.round(left), y: Math.round(top)};
                    }

                    /**
                     * @param {Node} element
                     * @returns {{x: number, y: number}}
                     */
                    function getOffsetSum(element) {
                        // Вычисление координат объекта суммированием offset'ов
                        if (typeof(element.offsetTop) === 'undefined') {
                            return {x: 0, y: 0};
                        }
                        var top = 0, left = 0;
                        while (element) {
                            top = top + parseFloat(element.offsetTop);
                            left = left + parseFloat(element.offsetLeft);
                            element = element.offsetParent;
                        }
                        return {x: Math.round(left), y: Math.round(top)};
                    }

                    // Вычисляем результат подходящим способом
                    if (element.getBoundingClientRect) {
                        // "правильный" вариант
                        return getOffsetRect(element);
                    } else {
                        // пусть работает хоть как-то
                        return getOffsetSum(element);
                    }
                };

                /**
                 * Get CSS-selector coincident to the element
                 * @returns {string}
                 */
                this.getSelector = function () {
                    if (!element instanceof HTMLElement) return '';
                    if (element.tagName === 'BODY') return 'body';
                    var tmp;

                    // Element base selector
                    var selector = element.tagName.toLowerCase();
                    if (element.hasAttribute('id')) {
                        selector += '#' + element.getAttribute('id');
                    } else if (element.hasAttribute('class')) {
                        //selector += '.'+element.className.replace(/\s+/g, '.');
                        selector += '[class="' + element.getAttribute('class') + '"]';
                    }

                    // Check uniqueness over document
                    tmp = document.querySelectorAll(selector);
                    if (tmp.length === 1 && tmp[0] === element) {
                        return selector;
                    }

                    // Check uniqueness over closest parent
                    var parent = element.parentNode;
                    tmp = parent.querySelectorAll(selector);
                    if (tmp.length === 1 && tmp[0] === element) {
                        // Element only one in its parent
                        return lib.wrElement(parent).getSelector() + ' > ' + selector;

                    } else if (tmp.length > 1) {
                        // Find the element number in its parent
                        selector = element.tagName.toLowerCase();   // :nth-of-type ignores element ID and CLASS
                        var num = 0;
                        lib.wrList(parent.querySelectorAll(':scope > ' + selector)).forEach(function (t, n) {
                            if (t === element) {
                                num = n;
                                return false;
                            }
                            return true;
                        });
                        return lib.wrElement(parent).getSelector() + ' > ' + selector + ':nth-of-type(' + (num + 1) + ')';
                    }

                    return selector;
                };

                /**
                 *
                 */
                this.markAsSpecial = function () {
                    if (element.setAttribute) {
                        element.setAttribute('data-selector-picker-special', '1');
                    }
                    lib.wrList(element.querySelectorAll('*')).forEach(function (el) {
                        if (el.setAttribute) {
                            el.setAttribute('data-selector-picker-special', '1');
                        }
                    });
                };
            })(element);
        };

        /**
         * @param {Event} event
         * @returns {{cancelDefault: function(), mouseCoords: function()}}
         */
        this.wrEvent = function (event) {
            return new (function (event) {
                /**
                 *
                 */
                this.cancelDefault = function () {
                    event.stopPropagation ? event.stopPropagation() : event.cancelBubble = true;
                    event.preventDefault ? event.preventDefault() : event.returnValue = false;
                };

                /**
                 * Определение координат мыши в документе
                 * @returns {{x: Number, y: Number}}
                 */
                this.mouseCoords = function () {
                    if (!event instanceof MouseEvent) {
                        return {x: 0, y: 0};
                    }
                    if (event.pageX === null && event.clientX !== null) {
                        var scroll = lib.scrollPos();
                        event.pageX = event.clientX + scroll.left;
                        event.pageY = event.clientY + scroll.top;
                    }
                    return {x: event.pageX, y: event.pageY};
                };
            })(event);
        };

        /**
         * @param    {function}    callback
         */
        this.onLoad = function (callback) {
            if (
                document.readyState === 'complete' ||
                ( document.readyState === 'interactive' && !/MSIE *\d+\.\w+/i.test(window.navigator.userAgent) ) ||	// в IE в этом режиме работа с DOM возможна, только если все скрипты разместить в футере
                document.readyState === 'loaded'	// Вариант для некоторых старых браузеров
            ) {
                callback();
            } else {
                lib.wrElement(document).addHandler('DOMContentLoaded', function () {
                    callback()
                });
            }
        };
    };

    /**
     *
     */
    var Result, HoverRectangle, Demo, Menu, Loading;

    /**
     *
     */
    var Picker = new function () {
        /**
         * {NodeList}
         */
        var elements;

        /**
         * @type {boolean}
         */
        var pickerEnabled = false;

        /**
         * @param {Event} e
         */
        var handlerClick = function (e) {
            if (!pickerEnabled) {
                return;
            }
            if (e.stopImmediatePropagation) {
                e.stopImmediatePropagation();
            }
            lib.wrEvent(e).cancelDefault();
            if (settings.selectPosition) {
                if (Menu.isShown()) {
                    Menu.hide();
                    return;
                }
                var pos = lib.wrEvent(e).mouseCoords();
                Menu.show(pos.x, pos.y);
            } else {
                Demo.hide();

                var selector = lib.wrElement(HoverRectangle.getElement()).getSelector();
                Result.set(selector, POSITION_INSIDE);
                Demo.show(document.querySelector(selector), POSITION_INSIDE);
            }
        };

        /**
         * @param {Event} e
         */
        var handlerMouseover = function (e) {
            if (!pickerEnabled) {
                return;
            }
            if (Menu.isShown()) {
                return;
            }
            lib.wrEvent(e).cancelDefault();
            HoverRectangle.show(this);
        };

        /**
         *
         */
        var setHandlers = function () {
            // Get all elements, except created by this plugin and already processed by this function
            elements = document.body.querySelectorAll('*:not([data-selector-picker-special]):not([data-selector-picker-processed])');

            // Handlers for all elements, that can be picked by user
            lib.wrList(elements).forEach(function (element) {
                if (!element instanceof HTMLElement || element instanceof SVGSVGElement || element instanceof SVGPathElement) {
                    // Skip SVGSVGElement and SVGPathElement
                    return;
                }
                lib.wrElement(element).addHandler('mouseover', handlerMouseover);
                lib.wrElement(element).addHandler('click', handlerClick);
                element.setAttribute('data-selector-picker-processed', '1');
            });
        };

        /**
         * @type {boolean}
         */
        var changeContentsWatch_called = false;

        /**
         *
         */
        var changeContentsWatch = function () {
            if (changeContentsWatch_called) {
                return;
            }
            changeContentsWatch_called = true;
            var handleMutations = true;

            /**
             *
             */
            var mutationsFixer = new function () {
                /**
                 * @type {[]}
                 */
                var elements = [];

                /**
                 * @type {number}
                 */
                var T = 0;

                /**
                 * @param {HTMLElement} element
                 */
                this.add = function (element) {
                    if (element.nodeType !== 1) {
                        return;
                    }
                    elements.push(element);
                    if (!T) {
                        T = setTimeout(
                            function () {
                                handleMutations = false;
                                var specials = [];
                                var tmpSpan;
                                lib.wrList(elements).forEach(function (element) {
                                    var p = element.parentNode;
                                    if (!p) {
                                        return;
                                    }
                                    var specialsOrig = [];
                                    lib.wrList(p.querySelectorAll('[data-selector-picker-special="1"]')).forEach(function (spec) {
                                        if(spec.parentNode && !spec.parentNode.hasAttribute('data-selector-picker-special')){
                                            specialsOrig.push(spec);
                                        }
                                    });
                                    lib.wrList(specialsOrig).forEach(function (spec) {
                                        if(!spec || !spec.parentNode){
                                            return;
                                        }
                                        tmpSpan = document.createElement('span');
                                        tmpSpan.setAttribute('data-selector-picker-special-temp', specials.length);
                                        specials.push(spec);
                                        spec.parentNode.insertBefore(tmpSpan, spec);
                                        spec.parentNode.removeChild(spec);
                                    });
                                    p.innerHTML = p.innerHTML.replace(/data-selector-picker-processed="1"/g, '');
                                });

                                var tmpSpans = [];
                                lib.wrList(document.querySelectorAll('[data-selector-picker-special-temp]')).forEach(function (tmpSpan) {
                                    tmpSpans.push(tmpSpan);
                                });
                                lib.wrList(tmpSpans).forEach(function (tmpSpan) {
                                    var n = parseInt(tmpSpan.getAttribute('data-selector-picker-special-temp'));
                                    if(typeof(specials[n]) !== 'undefined'){
                                        tmpSpan.parentNode.insertBefore(specials[n], tmpSpan);
                                    }
                                    tmpSpan.parentNode.removeChild(tmpSpan);
                                });

                                elements = [];
                                setHandlers();
                            },
                            5000
                        );
                    }
                };
            };

            lib.wrElement(window).onChangeContents(function (mutations) {
                if (!handleMutations) {
                    return;
                }
                lib.wrList(mutations).forEach(function (mutation) {
                    lib.wrList(mutation.addedNodes).forEach(function (element) {
                        mutationsFixer.add(element);
                    });
                });
            });
        };

        /**
         *
         */
        this.enable = function () {
            if (pickerEnabled) {
                return;
            }
            setHandlers();
            changeContentsWatch();
            config.loadInitial();
            pickerEnabled = true;
        };

        /**
         *
         */
        this.disable = function () {
            pickerEnabled = false;
            /*lib.wrList(elements).forEach(function (element) {
             if (!element instanceof HTMLElement || element instanceof SVGSVGElement || element instanceof SVGPathElement) {
             // Skip SVGSVGElement and SVGPathElement
             return;
             }
             lib.wrElement(element).removeHandler('mouseover', handlerMouseover);
             lib.wrElement(element).removeHandler('click', handlerClick);
             });*/
        };

        /**
         * @returns {boolean}
         */
        this.isEnabled = function () {
            return pickerEnabled;
        }
    };

    //
    lib.onLoad(function () {
        config.onLoad();

        // Display top bar
        var topBar;
        (function () {
            topBar = document.createElement('div');
            topBar.innerHTML = config.barHtml.replace('[bar-message]', settings.barMessage);
            if (topBar.childNodes.length === 1 && topBar.childNodes[0].nodeType === 1) {
                topBar = topBar.childNodes[0];
            }
            topBar.style.zIndex = config.barZIndex;
            document.body.appendChild(topBar);

            var btnEnable = topBar.querySelector('[rel="enable"]');
            if (settings.disabledByDefault) {
                lib.wrElement(btnEnable).addHandler(
                    'click',
                    function (e) {
                        lib.wrEvent(e).cancelDefault();
                        if (Picker.isEnabled()) {
                            Picker.disable();
                            if (typeof(btnEnable.innerHTML) !== 'undefined') {
                                btnEnable.innerHTML = 'Enable';
                            }
                            if (typeof(btnEnable.value) !== 'undefined') {
                                btnEnable.value = 'Enable';
                            }
                        } else {
                            Picker.enable();
                            if (typeof(btnEnable.innerHTML) !== 'undefined') {
                                btnEnable.innerHTML = 'Disable';
                            }
                            if (typeof(btnEnable.value) !== 'undefined') {
                                btnEnable.value = 'Disable';
                            }
                        }
                    },
                    true
                );
            } else {
                btnEnable.style.display = 'none';
            }
        })();
        lib.wrElement(topBar).markAsSpecial();

        /**
         *
         */
        Result = new function () {
            /**
             * @type {string}
             */
            var resultSelector = '',
                resultPosition = POSITION_BEFORE;

            /**
             * @param {string} selector
             * @param {string} position
             */
            this.set = function (selector, position) {
                resultSelector = selector;
                resultPosition = position;
            };

            var btnSave = topBar.querySelector('[rel="save"]');
            if (btnSave) {
                lib.wrElement(btnSave).addHandler('click', function () {
                    config.onSave(resultSelector, resultPosition);
                });
            } else {
                console.error('Link or Button with attr [rel="save"] are required in top bar');
            }
        };

        /**
         *
         */
        HoverRectangle = new function () {
            /**
             * @type {HTMLElement|null}
             */
            var currentElement = null;

            /**
             * @type {HTMLElement[]}
             */
            var lines = [];
            var line;
            for (var i = 0; i < 4; i++) {
                line = document.createElement('div');
                line.style.cssText = config.hoverRectangleSidesCss;
                line.style.display = 'none';
                line.style.position = 'absolute';
                line.style.zIndex = config.hoverRectangleZIndex;
                line.style.width = '1px';
                line.style.height = '1px';
                document.body.appendChild(line);
                lib.wrElement(line).markAsSpecial();
                lines.push(line);
            }

            /**
             * @param {HTMLElement} element
             */
            this.show = function (element) {
                currentElement = element;
                var pos = lib.wrElement(currentElement).pos();
                var size = lib.wrElement(currentElement).size();
                var bodyPosY = lib.wrElement(document.body).pos().y;
                var
                    x1 = pos.x,
                    x2 = pos.x + size.w - 1,
                    y1 = pos.y - bodyPosY,
                    y2 = pos.y + size.h - bodyPosY - 1;

                lib.wrList(lines).forEach(function (line) {
                    line.style.display = 'block';
                });
                lines[0].style.left = x1 + 'px';
                lines[0].style.top = y1 + 'px';
                lines[0].style.width = (x2 - x1) + 'px';

                lines[1].style.left = x2 + 'px';
                lines[1].style.top = y1 + 'px';
                lines[1].style.height = (y2 - y1) + 'px';

                lines[2].style.left = x1 + 'px';
                lines[2].style.top = y2 + 'px';
                lines[2].style.width = (x2 - x1) + 'px';

                lines[3].style.left = x1 + 'px';
                lines[3].style.top = y1 + 'px';
                lines[3].style.height = (y2 - y1) + 'px';
            };

            /**
             *
             */
            this.hide = function () {
                currentElement = null;
                lib.wrList(lines).forEach(function (line) {
                    line.style.display = 'none';
                });
            };

            /**
             * @returns {HTMLElement|null}
             */
            this.getElement = function () {
                return currentElement;
            };
        };

        /**
         *
         */
        Demo = new function () {
            /**
             *
             */
            var DemoStatic = new function () {
                /**
                 *
                 */
                var self = this;

                /**
                 * @type {HTMLElement}
                 */
                var block = document.createElement('div');
                block.innerHTML = config.demoStaticHtml.replace('[demo-text]', settings.demoText);
                if (block.childNodes.length === 1 && block.childNodes[0].nodeType === 1) {
                    block = block.childNodes[0];
                }
                block.style.position = 'static';
                document.body.appendChild(block);

                /**
                 * @type {string}
                 */
                var visibleCssDisplayValue = block.style.display;
                block.style.display = 'none';
                lib.wrElement(block).markAsSpecial();

                /**
                 *
                 */
                self.hide = function () {
                    document.body.appendChild(block);
                    block.style.display = 'none';
                };

                /**
                 * @param {HTMLElement} refElement
                 * @param {*} refPosition   POSITION_BEFORE, POSITION_INSIDE or POSITION_AFTER
                 */
                self.show = function (refElement, refPosition) {
                    if (!refElement) return;
                    block.style.display = visibleCssDisplayValue;

                    if (refPosition === POSITION_BEFORE) {
                        refElement.parentNode.insertBefore(block, refElement);
                    } else if (refPosition === POSITION_INSIDE) {
                        refElement.appendChild(block);
                    } else if (refPosition === POSITION_AFTER) {
                        if (refElement.nextElementSibling) {
                            self.show(refElement.nextElementSibling, POSITION_BEFORE);
                        } else {
                            self.show(refElement.parentNode, POSITION_INSIDE);
                        }
                    } else {
                        self.hide();
                    }
                };
            };

            /**
             *
             */
            var DemoOverlay = new function () {
                /**
                 *
                 */
                var self = this;

                /**
                 * @type {HTMLElement[]}
                 */
                var lines = [];
                var line;
                for (var i = 0; i < 4; i++) {
                    line = document.createElement('div');
                    line.style.cssText = config.demoOverlaySidesCSS;
                    line.style.display = 'none';
                    line.style.position = 'absolute';
                    line.style.zIndex = config.demoOverlayZIndex;
                    line.style.width = '2px';
                    line.style.height = '2px';
                    document.body.appendChild(line);
                    lib.wrElement(line).markAsSpecial();
                    lines.push(line);
                }

                /**
                 *
                 */
                self.hide = function () {
                    lib.wrList(lines).forEach(function (line) {
                        line.style.display = 'none';
                    });
                };

                /**
                 * @param {HTMLElement} refElement
                 */
                self.show = function (refElement) {
                    if (!refElement) return;
                    var pos = lib.wrElement(refElement).pos();
                    var size = lib.wrElement(refElement).size();
                    var bodyPosY = lib.wrElement(document.body).pos().y;
                    var
                        x1 = pos.x,
                        x2 = pos.x + size.w - 2,
                        y1 = pos.y - bodyPosY,
                        y2 = pos.y + size.h - bodyPosY - 2;

                    lib.wrList(lines).forEach(function (line) {
                        line.style.display = 'block';
                    });
                    lines[0].style.left = x1 + 'px';
                    lines[0].style.top = y1 + 'px';
                    lines[0].style.width = (x2 - x1) + 'px';

                    lines[1].style.left = x2 + 'px';
                    lines[1].style.top = y1 + 'px';
                    lines[1].style.height = (y2 - y1) + 'px';

                    lines[2].style.left = x1 + 'px';
                    lines[2].style.top = y2 + 'px';
                    lines[2].style.width = (x2 - x1) + 'px';

                    lines[3].style.left = x1 + 'px';
                    lines[3].style.top = y1 + 'px';
                    lines[3].style.height = (y2 - y1) + 'px';
                };
            };

            /**
             *
             */
            this.hide = function () {
                if (settings.selectPosition) {
                    DemoStatic.hide();
                } else {
                    DemoOverlay.hide();
                }
            };

            /**
             * @param {HTMLElement} refElement
             * @param {*} refPosition   POSITION_BEFORE, POSITION_INSIDE or POSITION_AFTER
             */
            this.show = function (refElement, refPosition) {
                if (settings.selectPosition) {
                    DemoStatic.show(refElement, refPosition);
                } else {
                    DemoOverlay.show(refElement);
                }
            };
        };

        /**
         *
         */
        Menu = new function () {
            var self = this;
            var shown = false;

            /**
             * @type {HTMLElement}
             */
            var block = document.createElement('div');
            block.innerHTML = config.menuHtml;
            if (block.childNodes.length === 1 && block.childNodes[0].nodeType === 1) {
                block = block.childNodes[0];
            }
            block.style.position = 'absolute';
            block.style.zIndex = config.menuZIndex;
            document.body.appendChild(block);

            /**
             * @type {string}
             */
            var visibleCssDisplayValue = block.style.display;
            block.style.display = 'none';
            lib.wrElement(block).markAsSpecial();

            /**
             * @param {Number} x
             * @param {Number} y
             */
            this.show = function (x, y) {
                shown = true;
                block.style.left = x + 'px';
                block.style.top = (y - lib.wrElement(document.body).pos().y) + 'px';
                block.style.display = visibleCssDisplayValue;
            };

            /**
             *
             */
            this.hide = function () {
                shown = false;
                block.style.display = 'none';
            };

            /**
             * @returns {boolean}
             */
            this.isShown = function () {
                return shown;
            };

            // Handlers for links in menu
            lib.wrList([POSITION_BEFORE, POSITION_INSIDE, POSITION_AFTER]).forEach(function (position) {
                var elLink = block.querySelector('[rel="' + position + '"]');
                if (!elLink) return;
                lib.wrElement(elLink).addHandler('click', function (e) {
                    Demo.hide();

                    var selector = lib.wrElement(HoverRectangle.getElement()).getSelector();
                    Result.set(selector, position);
                    Demo.show(document.querySelector(selector), position);

                    HoverRectangle.hide();
                    self.hide();
                    lib.wrEvent(e).cancelDefault();
                });
            });

            // Hide menu when click outside it
            lib.wrElement(document.body).addHandler('click', function (e) {
                if (!shown) return;
                var target = e.target;
                while (target.tagName !== 'BODY') {
                    if (target === block) {
                        return;
                    }
                    target = target.parentNode;
                }
                self.hide();
            });
        };

        //
        if (!settings.disabledByDefault) {
            Picker.enable();
        }
    });

    /**
     *
     */
    window.SpuritSelectorPickerInit = function () {
        try {
            Picker.enable();
            if (settings.disabledByDefault) {
                Picker.disable();
            }
        } catch (e) {
            console.log(e);
        }
    };

    var html = document.querySelector('html');
    if (html) {
        var script = document.createElement('script');
        script.innerHTML = "if(window.SpuritSelectorPickerInit) { window.SpuritSelectorPickerInit(); }";
        html.appendChild(script);
    }
})();