<?php

/**
 * Amazon Library.
 * Uses Amazon SDK.
 *
 * @copyright Copyright (c) 2016 SpurIT <contact@spur-i-t.com>, All rights reserved
 * @link http://spur-i-t.com
 * @version 1.0.0
 */
class Amazon
{
    /**
     * @var string
     */
    protected $_bucket = null;

    /**
     * @var string
     */
    protected $_key = null;

    /**
     * @var string
     */
    protected $_secret = null;

    /**
     * @var string
     */
    protected $_contentType = 'application/octet-stream';

    /**
     * Amazon constructor.
     * @param string $bucket
     * @param string $key
     * @param string $secret
     */
    public function __construct($bucket, $key, $secret) {
        include_once(__DIR__ . '/AWSSDKforPHP/sdk.class.php');
        $this->_bucket = $bucket;
        $this->_key = $key;
        $this->_secret = $secret;
    }

    /**
     * setBucket
     * @param string $bucket
     */
    public function setBucket($bucket) {
        $this->_bucket = $bucket;
    }

    /**
     * getBucket
     * @return null|string
     */
    public function getBucket() {
        return $this->_bucket;
    }

    /**
     * Amazon Handler
     * @return AmazonS3
     */
    protected function getInstance() {
        static $instance;
        if(is_null($instance)){
            // Instantiate the AmazonS3 class
            $instance = new AmazonS3(array(
                'key'    => $this->_key,
                'secret' => $this->_secret,
            ));
        }
        return $instance;
    }

    /**
     * upload file to Amazon
     *
     * @param string $fileName
     * @param string $content
     * @param null $contentType
     * @return bool
     */
    public function upload($fileName, $content, $contentType = null, $contentEncoding = null) {
        $params = array(
            'body' => $content,
            'acl' => AmazonS3::ACL_PUBLIC,
            'contentType' => (empty($contentType) ? $this->_contentType : $contentType ),
            'storage' => AmazonS3::STORAGE_REDUCED
        );
        if($contentEncoding){
            $params['headers'] = array(
                'Content-Encoding' => $contentEncoding
            );
        }
        $response = $this->getInstance()->create_object(
            $this->getBucket(),
            $fileName,
            $params
        );

        return $response->isOK();
    }

    /**
     * Upload file to Amazon
     * @param string $destFileName
     * @param string $sourceFile
     *
     * @return boolean
     */
    public function uploadFile($destFileName, $sourceFile) {
        $fileData = getimagesize($sourceFile);

        $response = $this->getInstance()->create_object(
            $this->getBucket(),
            $destFileName,
            array(
                'fileUpload'  => $sourceFile,
                'acl'         => AmazonS3::ACL_PUBLIC,
                'contentType' => $fileData['mime'],
                'storage'     => AmazonS3::STORAGE_REDUCED
            )
        );

        return $response->isOK();
    }


    /**
     * remove File from Amazon
     * @param string $fileName
     * @return CFResponse
     */
    public function remove($fileName) {
        $response = $this->getInstance()->delete_object(
            $this->getBucket(),
            $fileName
        );

        return $response;
    }
}