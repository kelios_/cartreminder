<?php

/**
 * Deploy control
 * @author Alexey Sinkevich
 */
class Deploy
{
    /**
     * @param string $message
     * @param int $exitCode
     */
    public static function abort($message, $exitCode = 1)
    {
        print $message . PHP_EOL;
        exit($exitCode);
    }

    /**
     * @param string $message
     */
    public static function display($message)
    {
        print $message . PHP_EOL;
    }

    /**
     * @param string $filePath
     * @param array $requiredParams
     * @return array
     */
    public static function readIniConfig($filePath, array $requiredParams = [])
    {
        if (!file_exists($filePath)) {
            self::abort('Deploy config file not found at "' . $filePath . '"');
        }
        $iniConfig = parse_ini_file($filePath);
        if ($iniConfig === false) {
            self::abort('Fail to parse Deploy config file at "' . $filePath . '"');
        }
        foreach ($requiredParams as $opt) {
            if (empty($iniConfig[$opt])) {
                self::abort('Config option "' . $opt . '" missing in config file "' . $filePath . '"');
            }
        }
        return $iniConfig;
    }

    /**
     * @param string $gitProjectRoot
     * @param string $localTempDir
     * @return string
     */
    public static function makeGitArchive($gitProjectRoot, $localTempDir)
    {
        self::display('Check local temp directory ' . realpath($localTempDir) . '...');
        if (!file_exists($localTempDir)) {
            if (!mkdir($localTempDir)) {
                self::abort('Fail to create deploy temp folder at "' . $localTempDir . '"');
            }
        } elseif (!is_writable($localTempDir) && !chmod($localTempDir, 0777)) {
            self::abort('Deploy tmp folder "' . $localTempDir . '" is not accessable');
        }
        foreach (new DirectoryIterator($localTempDir) as $file) {
            if ($file->isDot()) {
                continue;
            }
            unlink($file->getPathname());
        }
        self::display('Ok');

        self::display('Creating zip-archive with GIT head-revision...');
        $rev = exec('git rev-parse HEAD', $out, $status);
        if ($status != 0 || !$rev) {
            self::abort('Fail to find current git revision hash', $status);
        }
        $filepath = rtrim($localTempDir, '/\\') . '/' . date('Y-m-d_H-i-s') . '.zip';
        exec('cd ' . $gitProjectRoot . '; git archive --worktree-attributes -o ' . $filepath . ' ' . $rev, $out, $status);
        if ($status != 0) {
            self::abort('Failed to make git archive at "' . $filepath . '"', $status);
        }
        self::display('Ok (' . realpath($filepath) . ')');
        return $filepath;
    }
}