<?php

/**
 * Class Compression
 * @author Alexey Sinkevich
 */
class Compression
{

    /**
     *
     */
    const JS_REQUEST_URL = 'http://closure-compiler.appspot.com/compile';
    const JS_REQUEST_POST = 'js_code={CODE}&compilation_level=SIMPLE_OPTIMIZATIONS&output_format=text&output_info=compiled_code';

    /**
     * @param string $code
     * @param string $error
     * @return string
     */
    public static function compressJs($code, &$error = '')
    {
        $code = trim($code);
        if ($code === '') {
            return '';
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::JS_REQUEST_URL);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            str_replace('{CODE}', urlencode($code), self::JS_REQUEST_POST)
        );
        $minCode = curl_exec($ch);
        if ($error = curl_error($ch)) {
            return '';
        }
        curl_close($ch);

        if (preg_match('/^Error\([0-9]+\):/', $minCode)) {
            $error = 'Closure Compiler ' . $minCode;
            return '';
        }

        return $minCode;
    }

    /**
     * @param string $code
     * @param string $error
     * @return string
     */
    public static function compressCss($code, &$error = '')
    {
        $code = trim($code);
        if ($code === '') {
            return '';
        }
        $code = preg_replace('/  \/\*  .*?  \*\/  /iusx', '', $code);
        $code = str_replace(["\n", "\r", "\t"], '', $code);
        $code = preg_replace('/\s{2,}/ius', ' ', $code);
        $code = str_replace([' {', '{ '], '{', $code);
        $code = str_replace([' :', ': '], ':', $code);
        $code = str_replace([' ,', ', '], ',', $code);
        $code = str_replace([' ;', '; '], ';', $code);
        $code = str_replace(';}', '}', $code);
        $code = str_replace('000000000000001', '', $code);
        $code = str_replace('00000000000001', '', $code);
        return $code;
    }
}