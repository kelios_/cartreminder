<?php

include_once(__DIR__ . '/Amazon.php');
include_once(__DIR__ . '/Compression.php');

/**
 * Compression and uploading static files (css, js) to Amazon
 * @author Alexey Sinkevich
 */
class Statics
{
    /**
     * @var Amazon
     */
    protected $oAmazon = null;

    /**
     * @param string $amazonBucket
     * @param string $amazonKey
     * @param string $amazonSecret
     */
    public function __construct($amazonBucket, $amazonKey, $amazonSecret)
    {
        $this->oAmazon = new Amazon($amazonBucket, $amazonKey, $amazonSecret);
    }

    /**
     * @param string $jsDirLocal
     * @param string $jsDirAmazon
     * @param bool $compress
     */
    public function uploadJs($jsDirLocal, $jsDirAmazon, $compress = false)
    {
        $jsDirLocal = rtrim($jsDirLocal, '/\\');
        $jsonFile = $jsDirLocal . '/packages.json';
        if (is_file($jsonFile)) {
            $packages = self::readPackages($jsonFile);
            foreach ($packages as $package) {
                $amazonPath = rtrim($jsDirAmazon, '/\\') . '/' . basename($package['dest']);
                $code = '';
                foreach ($package['src'] as $file) {
                    $code .= file_get_contents($file) . ";\n\n";
                }
                if ($compress) {
                    $code = Compression::compressJs($code, $error);
                    if (!$code) {
                        Deploy::abort($error);
                    }
                }
                if (!$this->oAmazon->upload($amazonPath, $code, 'application/javascript')) {
                    Deploy::abort('Fail uploading "' . $amazonPath . '" to Amazon');
                }
                Deploy::display('Success ' . ($compress ? 'compression and ' : '') . 'uploading "' . $amazonPath . '" to Amazon');
            }

        } else {
            $jsFiles = glob($jsDirLocal . '/*.js');
            foreach ($jsFiles as $filePath) {
                $amazonPath = rtrim($jsDirAmazon, '/\\') . '/' . basename($filePath);
                $code = file_get_contents($filePath);
                if ($compress) {
                    $code = Compression::compressJs($code, $error);
                    if (!$code) {
                        Deploy::abort($error);
                    }
                }
                if (!$this->oAmazon->upload($amazonPath, $code, 'application/javascript')) {
                    Deploy::abort('Fail uploading "' . $amazonPath . '" to Amazon');
                }
                Deploy::display('Success ' . ($compress ? 'compression and ' : '') . 'uploading "' . $amazonPath . '" to Amazon');
            }
        }
    }

    /**
     * @param string $cssDirLocal
     * @param string $cssDirAmazon
     * @param bool $compress
     */
    public function uploadCss($cssDirLocal, $cssDirAmazon, $compress = false)
    {
        $cssFiles = glob(rtrim($cssDirLocal, '/\\') . '/*.css');
        foreach ($cssFiles as $filePath) {
            $amazonPath = rtrim($cssDirAmazon, '/\\') . '/' . basename($filePath);
            $code = file_get_contents($filePath);
            if ($compress) {
                $code = Compression::compressCss($code, $error);
                if (!$code) {
                    Deploy::abort($error);
                }
            }
            if (!$this->oAmazon->upload($amazonPath, $code, 'text/css')) {
                Deploy::abort('Fail uploading "' . $amazonPath . '" to Amazon');
            }
            Deploy::display('Success ' . ($compress ? 'compression and ' : '') . 'uploading "' . $amazonPath . '" to Amazon');
        }
    }

    /**
     * @param string $jsonFile
     * @return array
     */
    protected static function readPackages($jsonFile)
    {
        if (!is_file($jsonFile)) {
            return [];
        }
        $packages = file_get_contents($jsonFile);
        if ($packages === false) {
            Deploy::abort('Unable to read packages file "' . $jsonFile . '"');
        }
        $packages = json_decode($packages, true);
        if (!is_array($packages)) {
            Deploy::abort('Unable to parse packages file "' . $jsonFile . '"');
        }
        $jsonFileDir = realpath(dirname($jsonFile));
        foreach ($packages as $pn => &$data) {
            if (!isset($data['src'])) {
                Deploy::abort('Package "src" is not specified (package "' . $pn . '")');
            }
            if (!is_string($data['src']) && !is_array($data['src'])) {
                Deploy::abort('Package "src" should be a string or an array of strings (package "' . $pn . '")');
            }
            if (is_array($data['src'])) {
                foreach ($data['src'] as $s) {
                    if (!is_string($s)) {
                        Deploy::abort('Package "src" should be a string or an array of strings (package "' . $pn . '")');
                    }
                }
            }
            if (!isset($data['dest'])) {
                Deploy::abort('Package "dest" is not specified (package "' . $pn . '")');
            }
            if (!is_string($data['dest'])) {
                Deploy::abort('Package "dest" should be a string (package "' . $pn . '")');
            }
            if (!is_array($data['src'])) {
                $data['src'] = [$data['src']];
            }
            foreach ($data['src'] as &$src) {
                $src = ltrim($src, '/\\');
                $src = realpath(dirname($jsonFileDir . '/' . $src)) . '/' . basename($src);
            }
            unset($src);
            $data['dest'] = basename($data['dest']);
            $data = [
                'src' => $data['src'],
                'dest' => $data['dest']
            ];
        }
        unset($data);
        return $packages;
    }
}