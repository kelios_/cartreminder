<?php
	include_once APP . 'Controller/AppaController.php';

	/**
	 * Main controller to solve some issues.
	 *
	 * @author Ivan Polyakov
	 * @package Product Selector
	 * @version 1.0.1
	 */
	class IndexaController extends AppaController
	{
        public $helpers = array('Html', 'Money' => array('className' => 'ProductSelector.Money'));

		/**
		 * Renders selected products.
		 */
		public function selectedProducts()
		{
			$this->viewPath = 'Elements';
			$this->set( 'shop', $this->params['named']['shop'] );
			$this->set( 'products', $this->params['named']['products'] );
			$this->set( 'moneyFormat', $this->params['named']['moneyFormat'] );
			$this->set( 'rightBlockId', $this->params['named']['rightBlockId'] );
			$this->set( 'group', false );
			if ( isset ( $this->params['named']['categories'] ) ) {
				$this->set( 'categories', $this->params['named']['categories'] );
			}
			if ( isset ( $this->params['named']['variants'] ) ) {
				$this->set( 'variants', $this->params['named']['variants'] );
			}
			if ( isset ( $this->params['named']['group'] ) ) {
				$this->set( 'group', $this->params['named']['group'] );
			}
			$this->render( 'selected_'. $this->params['named']['platform'] .'_products', 'ajax' );
		}

		/**
		 * Loads BigCommerce products.
		 * @internal AJAX
		 */
		public function getBigCommerceProducts()
		{
			$this->autoRender = false;
			// Params.
            $params = $this->getBCParams();

			// Settings.
            $settings = $this->getPSSettings( $this->shop['id'] );
            extract($settings);
			if ( !isset ( $price_field ) ) $price_field = 'price';
			if ( !isset ( $discount_field ) ) $discount_field = 'retail_price';

			// Products.
			$formattedProducts = array ();
			$products = $this->api->get( 'products', null, $params );
			if ( $products ) {
				foreach ( $products as $product ) {
					$thumb = null;

                    if ( isset($product->primary_image) ) {
                        $thumb = str_replace('http://', 'https://', $product->primary_image->thumbnail_url);
                    }
					$data = array (
						'id' => $product->id,
						'handle' => $product->custom_url,
						'title' => $product->name,
						'isVisible' => $product->is_visible,
						'categories' => $product->categories,
						'images' => $thumb,
						'variants' => array (
							array (
								'id' => $product->id,
								'title' => $product->name,
								'price' => number_format( $product->{$price_field}, 2, '.', ' ' ),
								'compare_at_price' => number_format( $product->{$discount_field}, 2, '.', ' ' )
							)
						)
					);
					if ( !isset ( $_GET['onlyHidden'] ) || ( isset ( $_GET['onlyHidden'] ) && !$product->is_visible ) ) {
						if ( isset ( $_GET['options'] ) && (integer) $_GET['options'] ) {
							$options = $this->api->get( 'products/options', $product->id );
							$data['options'] = $options;
						}
						if ( isset ( $_GET['configurable'] ) && (integer) $_GET['configurable'] ) {
							$configurables = $this->api->get( 'products/configurablefields', $product->id );
							$data['configurables'] = $configurables;
						}
					}
					$formattedProducts[] = $data;
				}
			}
			return json_encode( array (
				'page' => $_GET['page'],
				'products' => $formattedProducts
			) );
		}

        /**
         * Loads BigCommerce product count.
         * @internal AJAX
         */
        public function getBigCommerceProductCount()
        {
            $this->autoRender = false;
            $params = $this->getBCParams('count');

            return $this->api->get( 'products/count', null, $params );
        }

		public function getBigCommerceCategories()
		{
			$this->autoRender = false;
			$params = array('limit' => 250);
			if ( !empty($this->request->query['name']) ) {
				$params['name'] = $_GET['name'];
                $categories = $this->api->get( 'categories', null, $params );
			} else {
                $categories = $this->Product->getAllBCCategories();
            }

			if ( $categories ) {
				$groupped = array ();
				foreach ( $categories as $cat ) {
					$groupped[ $cat->parent_id ][] = $cat;
				}
				if ( isset ( $groupped[0] ) ) {
					$branches = $this->_fillBCCategoryBranches( $groupped[0], $groupped );
				} else {
					$branches = $this->_fillBCCategoryBranches( $categories, array () );
				}
				return json_encode( $branches );
			}
			return '[]';
		}

		private function _fillBCCategoryBranches( $categories, $groupped )
		{
			$branches = array ();
			foreach ( $categories as $cat ) {
				$children = array ();
				if ( array_key_exists( $cat->id, $groupped ) ) {
					$children = $this->_fillBCCategoryBranches( $groupped[ $cat->id ], $groupped );
				}
				$branches[] = array (
					'data' => $cat,
					'children' => $children
				);
			}
			return $branches;
		}

		/**
		 * Loads Shopify products.
		 * @internal AJAX
		 */
		public function getShopifyProducts()
		{
			$this->autoRender = false;
			$params = array (
				'fields' => 'id,handle,title,variants,images,published_at',
				'page' => $_GET['page'],
				'limit' => $_GET['limit']
			);
			if ( isset ( $_GET['name'] ) && !empty ( $_GET['name'] ) ) {
				$params['title'] = $_GET['name'];
			}
			if ( isset ( $_GET['category'] ) && $_GET['category'] ) {
				$params['collection_id'] = $_GET['category'];
			}
			$products = $this->api->get( 'products', null, $params );
			return json_encode( array (
				'page' => $_GET['page'],
				'products' => $products
			) );
		}

		/**
		 * Loads Shopify product count.
		 * @internal AJAX
		 */
		public function getShopifyProductCount()
		{
			$this->autoRender = false;
			$params = array ();
			if ( isset ( $_GET['name'] ) && !empty ( $_GET['name'] ) ) {
				$params['title'] = $_GET['name'];
			}
			if ( isset ( $_GET['category'] ) && $_GET['category'] ) {
				$params['collection_id'] = $_GET['category'];
			}
			return $this->api->get( 'products/count', null, $params );
		}

		public function getShopifyCategories()
		{
			$this->autoRender = false;
			$params = array('limit' => 250);
			if ( isset ( $_GET['name'] ) && !empty ( $_GET['name'] ) ) {
				$params['title'] = $_GET['name'];
			}
			$categories = $this->api->get( 'custom_collections', null, $params );
			if ( $categories ) {
				$groupped = array ();
				foreach ( $categories as $cat ) {
					$groupped[0][] = $cat;
				}
				if ( isset ( $groupped[0] ) ) {
					$branches = $this->_fillBCCategoryBranches( $groupped[0], $groupped );
				} else {
					$branches = $this->_fillBCCategoryBranches( $categories, array () );
				}
				return json_encode( $branches );
			}
			return '[]';
		}

		/**
		 * Loads PrestaShop products.
		 * @internal AJAX
		 */
		public function getPrestaShopProducts()
		{
			$this->autoRender = false;
			$params = array(
				'display' => 'full'
			);
			if ( $_GET['page'] && $_GET['limit'] ) {
				$page = ($_GET['page']-1) * $_GET['limit'];
				$itemsPerPage = $_GET['limit'];
				$params['limit'] = "{$page},{$itemsPerPage}";
			}
			if ( isset ( $_GET['name'] ) && !empty ( $_GET['name'] ) ) {
				$params['filter[name]'] = '%[' . $_GET['name'] . ']%';
			}
			$formattedProducts = array ();
			$products = $this->api->get( 'products', null, $params );
			$products = isset($products->products) ? $products->products->children()->product : null;
			if ( $products ) {
				foreach ( $products as $product ) {
					$image = Router::url(array(
						'plugin' => 'product_selector',
						'controller' => 'indexa',
						'action' => 'getPrestaShopProductImage',
						'ps_id' => (int)$product->id_default_image,
						'ps_rid' => (int)$product->id,
					), true);
					$data = array (
						'id' => (int)$product->id,
						'handle' => '/product.php?id_product=' . (int)$product->id,
						'title' => (string)$product->name->language,
						'isVisible' => ((string)$product->visibility != 'none'),
						'images' => $image,
						'variants' => array (
							array (
								'id' => (int)$product->id,
								'title' => (string)$product->name->language,
								'price' => number_format((float)$product->price, 2, '.', ' '),
								'compare_at_price' => 0
							)
						),
						'options' => (int)$product->id_default_combination ? (int)$product->id_default_combination : false,
					);
					$formattedProducts[] = $data;
				}
			}
			return json_encode( array (
				'page' => $_GET['page'],
				'products' => $formattedProducts
			) );
		}

		/**
		 * Loads PrestaShop product count.
		 * @internal AJAX
		 */
		public function getPrestaShopProductCount()
		{
			$this->autoRender = false;
			$params = array ();
			if ( isset ( $_GET['name'] ) && !empty ( $_GET['name'] ) ) {
				$params['filter[name]'] = '%[' . $_GET['name'] . ']%';
			}
			$products = $this->api->get( 'products', null, $params );
			$products = isset($products->products) ? $products->products->children()->product : null;

			return count($products);
		}

		/**
		 * Get image from PrestaShop store
		 * @param integer ps_id - image id
		 * @param integer ps_rid - product id
		 * @return string - content of image
		 */
		public function getPrestaShopProductImage() {
			$this->autoRender = false;
			$image = '';
			if (isset($this->request->named['ps_id']) && isset($this->request->named['ps_rid'])) {
				$this->api->setParseXml(false);
				$image = $this->api->get( 'images/products', array(
					'id' => (int)$this->request->named['ps_rid'],
					'subId' => (int)$this->request->named['ps_id']
				));
			}

			return $image;
		}

		/**
		 * Loads OpenCart products.
		 * @internal AJAX
		 */
		public function getOpenCartProducts()
		{
			$this->autoRender = false;
			$params = array(
				'display' => 'full'
			);
			if ( $_GET[ 'page' ] && $_GET[ 'limit' ] ) {
				$params[ 'page' ] = $_GET[ 'page' ];
				$params[ 'items_per_page' ] = $_GET[ 'limit' ];
			}
			if ( isset ( $_GET[ 'name' ] ) && !empty ( $_GET[ 'name' ] ) ) {
				$params[ 'filter' ] = array(
					'filter_name' => $_GET[ 'name' ]
				);
			}
			$formattedProducts = array();
			$products = $this->api->get( 'products', null, $params );
			if ( $products ) {
				foreach ( $products as $product ) {
					$data = array(
						'id' => $product->product_id,
						'handle' => '/index.php?route=product/product&product_id=' . $product->product_id,
						'title' => $product->name,
						'isVisible' => $product->status,
						'images' => $product->image ? 'http://' . $this->Product->config( 'shopName' ) . '/image/' . $product->image : '',
						'variants' => array(
							array(
								'id' => $product->product_id,
								'title' => $product->name,
								'price' => $product->special ? number_format( $product->special, 2, '.', ' ' ) : number_format( $product->price, 2, '.', ' ' ),
								'compare_at_price' => $product->special ? number_format( $product->price, 2, '.', ' ' ) : 0,
							)
						),
						'options' => true
					);
					$formattedProducts[ ] = $data;
				}
			}
			return json_encode( array(
				'page' => $_GET[ 'page' ],
				'products' => $formattedProducts
			) );
		}

		/**
		 * Loads OpenCart product count.
		 * @internal AJAX
		 */
		public function getOpenCartProductCount()
		{
			$this->autoRender = false;
			$params = array (
				'resource' => 'products'
			);
			if ( isset ( $_GET[ 'name' ] ) && !empty ( $_GET[ 'name' ] ) ) {
				$params[ 'filter' ] = array(
					'filter_name' => $_GET[ 'name' ]
				);
			}

			return $this->api->get( 'count', null, $params );
		}

        /**
         * Get request(GET) params
         *
         * @param string $mode Params mode: empty - all params, count - without 'limit' and 'page'
         *
         * @return array
         */
        protected function getBCParams($mode = '')
        {
            $params = array ();
            if ( !empty($this->request->query['name']) ) {
                $params['keyword_filter'] = $_GET['name'];
            }
            if ( !empty($this->request->query['category']) ) {
                $params['category'] = $_GET['category'];
            }
            if ($mode != 'count') {
                $params['page'] = !empty($this->request->query['page']) ? $this->request->query['page'] : 1 ;
                $params['limit'] = !empty($this->request->query['limit']) ? $this->request->query['limit'] : 10 ;
            }

            return $params;
        }
	}
