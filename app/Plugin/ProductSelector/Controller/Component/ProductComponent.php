<?php
	App::uses( 'Component', 'Controller' );

	/**
	 * Common methods for controller to interact with product selector.
	 *
	 * @author Ivan Polyakov
	 * @package Product Selector
	 * @version 1.1.1
	 */
	class ProductComponent extends Component
	{
		private $_controller;

		private $_config;

		private $_modelName;

		private $_categoryModelName;

		private $_parentModelName;

		public function startup( Controller $controller )
		{
			$this->_controller = $controller;
			$mainConf = Configure::read( 'main' );
			$this->_modelName = Configure::read( $mainConf['platform'] . '.plugins.ps.modelName' );
			$this->_categoryModelName = Configure::read( $mainConf['platform'] . '.plugins.ps.categoryModelName' );
			$this->_parentModelName = Configure::read( $mainConf['platform'] . '.plugins.ps.parentModelName' );
		}

		public function setConfig( $config ) {
			$this->_config = $config;
		}

		public function config( $name ) {
			return isset ( $this->_config[ $name ] ) ? $this->_config[ $name ] : null;
		}

		/**
		 * Loads all products already used in discounts.
		 * @param array $condition - model condition.
		 * @return string
		 */
		public function getUsedProducts( $condition )
		{
			$vids = array ();
			if ( $items = $this->_controller->{$this->_parentModelName}->find( 'all', $condition ) ) {
				foreach ( $items as $item ) {
					foreach ( $item[ $this->_modelName ] as $subItem ) {
						$vids[] = isset ( $subItem['vid'] ) ? $subItem['vid'] : $subItem['pid'];
					}
				}
			}
			return json_encode( $vids );
		}

		/**
		 * Loads all categories already used in discounts.
		 * @param array $condition - model condition.
		 * @return string
		 */
		public function getUsedCategories( $condition )
		{
			$cids = array ();
			if ( $items = $this->_controller->{$this->_parentModelName}->find( 'all', $condition ) ) {
				foreach ( $items as $item ) {
					foreach ( $item[ $this->_categoryModelName ] as $subItem ) {
						$cids[] = $subItem['cid'];
					}
				}
			}
			return json_encode( $cids );
		}

		/**
		 * Loads BC categories.
         * @param int $limit Limit for request
		 * @return array
		 */
		public function getCategories($limit = null)
		{
			$list = $rootCategories = $children = $items = array ();
            if ($limit) {
                $categories = $this->_controller->api->get( 'categories', null, array('limit' => $limit) );
            } else {
                $categories = $this->getAllBCCategories();
            }

            foreach($categories as $index => $category) {
                if (empty($category->parent_id)) {
                    $rootCategories[$category->id] = $category->name;
                } else {
                    if (!isset($children[$category->parent_id])) {
                        $children[$category->parent_id] = array();
                    }

                    $children[$category->parent_id][$category->id] = $category->name;
                }
            }

            unset($categories);

            foreach($rootCategories as $rootId => $rootName) {
                $list[$rootId] = $rootName;
                if ( isset($children[$rootId]) ) {
                    $this->makeCategoryChildren($list, $rootId, $children);
                }
            }

            return $list;

//			foreach ( $categories as $key => $category ) {
//				if ( !$category->parent_id ) {
//					$stdItem = new stdClass();
//					$stdItem->category = $category;
//					$stdItem->children = array ();
//					$items[] = $stdItem;
//					unset ( $categories[ $key ] );
//				}
//			}
           /*while ( !empty ( $categories ) ) {
               foreach ( $categories as $key => $category ) {
                   foreach ( $items as $ikey => &$item ) {
                       if ( $item->category->id == $category->parent_id ) {
                           $stdItem = new stdClass();
                           $stdItem->category = $category;
                           $stdItem->children = array ();
                           $item->children[] = $stdItem;
                           unset ( $categories[ $key ] );
                           break;
                       }
                   }
               }
           }*/
//
//            if ( !empty ( $categories ) ) {
//				foreach ( $categories as $key => $category ) {
//					foreach ( $items as $ikey => &$item ) {
//						if ( $item->category->id == $category->parent_id ) {
//							$stdItem = new stdClass();
//							$stdItem->category = $category;
//							$stdItem->children = array ();
//							$item->children[] = $stdItem;
//							unset ( $categories[ $key ] );
//							break;
//						}
//					}
//				}
//			}
//
//			return $this->_formCategoryOptions( $items );
		}

        protected function makeCategoryChildren(&$list, $rootCategoryId, $children, $level = 1) {
            if ( isset($children[$rootCategoryId]) ) {
                foreach($children[$rootCategoryId] as $categoryId => $categoryName) {
                    $list[$categoryId] = str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $categoryName;
                    $this->makeCategoryChildren($list, $categoryId, $children, $level+1);
                }
            }
        }

        public function getAllBCCategories()
        {
            $limit = 250;
            $categories = array();
            $page = 1;
            do {
                $cPart = $this->_controller->api->get( 'categories', null, array('limit' => $limit, 'page' => $page) );
                $categories = array_merge($categories, $cPart);
                $page ++;
            } while(count($cPart) >= $limit);

            return $categories;
        }

		protected function _formCategoryOptions( $items, $options = array (), $space = '' )
		{
//			foreach ( $items as $item ) {
//				$options[ $item->category->id ] = $space . $item->category->name;
//				if ( !empty ( $item->children ) ) {
//					$space .= '&nbsp;&nbsp;&nbsp;';
//					$options = $this->_formCategoryOptions( $item->children, $options, $space );
//					$space = substr_replace( $space, '', 0, 18 );
//				}
//			}
//			return $options;
		}

		/**
		 * Loads Shopify collections.
		 * @return array
		 */
		public function getCollections()
		{
			$items = array ();
//			$collections = $this->_controller->api->get( 'custom_collections', null, array('limit' => 250) );
//			$smartCollections = $this->_controller->api->get( 'smart_collections', null, array('limit' => 250) );
            $collections = $this->getAllCollections('custom_collections');
			$smartCollections = $this->getAllCollections('smart_collections');
			$collections = array_merge($collections, $smartCollections);
			foreach ( $collections as $item ) {
				$items[ $item->id ] = $item->title;
			}

			return $items;
		}

        /**
         * Load All Shopify collections
         * @param string $type
         * @return array
         */
        protected function getAllCollections($type = 'custom_collections')
        {
            $limit = 250;
            $collections = array();
            $page = 1;
            do {
                $cPart = $this->_controller->api->get( $type, null, array('limit' => $limit, 'page' => $page) );
                $collections = array_merge($collections, $cPart);
                $page ++;
            } while(count($cPart) >= $limit);

            return $collections;
        }

		/**
		 * Returns selected categories.
		 * @param array $condition - model condition.
		 * @return array
		 */
		public function loadBigCommerceSelectedCategories( $condition )
		{
			$categories = array ();
			if ( isset ( $condition['conditions'] ) ) {
				$_categories = $this->_controller->{$this->_categoryModelName}->find( 'all', $condition );
			} else {
				$_categories = $condition;
			}
			if ( $_categories ) {
				foreach ( $_categories as $_dc ) {
					if ( isset ( $condition['conditions'] ) ) {
						$cid = $_dc[ $this->_categoryModelName ]['cid'];
					} else {
						$cid = $_dc;
					}
					$category = $this->_controller->api->get( 'categories', $cid );
					if ( !$category ) {
						continue;
					}
					$categories[] = (object) array (
						'id' => $category->id,
						'handle' => $category->url,
						'title' => $category->name,
						'description' => $category->description
					);
				}
			}
			return $categories;
		}

		/**
		 * Returns selected products.
		 * @param array $condition - model condition.
		 * @return array
		 */
		public function loadBigCommerceSelectedProducts( $condition )
		{
			$products = array();
			if ( isset ( $condition['conditions'] ) ) {
               $_products = $this->_controller->{$this->_modelName}->find( 'all', $condition );
			} else {
				$_products = $condition;
			}
			if ( $_products ) {
				foreach ( $_products as $_dp ) {
                    $filter = array();
					if ( isset ( $condition['conditions'] ) ) {
                        if (empty($_dp[ $this->_modelName ]['pid']) && !empty($_dp[ $this->_modelName ]['sku'])) {
                            $filter['sku'] = $_dp[ $this->_modelName ]['sku'];
                        } else {
                            $pid = $_dp[ $this->_modelName ]['pid'];
                        }
					} else {
						$pid = $_dp;
					}

                    try {
                        if( count($filter) ) {
                            $shopId = $this->_controller->getShopId();
                            $product = $this->getBCProductByFilter($shopId, $_dp, $filter);
                        } else {
                            $product = $this->_controller->api->get( 'products', $pid );
                        }
                    } catch ( Exception $ex ) {
                        CakeLog::write( LOG_ERROR, $ex->getMessage() );
                        $product = null;
                    }

					if ( !$product ) {
						continue;
					}

					$thumb = null;
                    if ( isset($product->primary_image) ) {
                        $thumb = str_replace('http://', 'https://', $product->primary_image->thumbnail_url);
                    }
					$products[] = (object) array (
						'id' => $product->id,
						'handle' => $product->custom_url,
						'title' => $product->name,
						'images' => $thumb,
						'variants' => array (
							(object) array (
								'id' => $product->id,
								'title' => $product->name,
								'price' => number_format( $product->price, 2, '.', ' ' ),
								'compare_at_price' => number_format( $product->retail_price, 2, '.', ' ' )
							)
						)
					);
				}
			}
			return $products;
		}

		/**
		 * Returns selected categories.
		 * @param array $condition - model condition.
		 * @return array
		 */
		public function loadShopifySelectedCategories( $condition )
		{
			$categories = array ();
			if ( isset ( $condition['conditions'] ) ) {
				$_categories = $this->_controller->{$this->_categoryModelName}->find( 'all', $condition );
			} else {
				$_categories = $condition;
			}
			if ( $_categories ) {
				foreach ( $_categories as $_dc ) {
					if ( isset ( $condition['conditions'] ) ) {
						$cid = $_dc[ $this->_categoryModelName ]['cid'];
					} else {
						$cid = $_dc;
					}
					$category = $this->_controller->api->get( 'custom_collections', $cid );
					if ( !$category ) {
						continue;
					}
					$category = $category->custom_collection;
					$categories[] = (object) array (
						'id' => $category->id,
						'handle' => $category->handle,
						'title' => $category->title,
						'description' => $category->body_html
					);
				}
			}
			return $categories;
		}

		/**
		 * Returns selected products.
		 * @param array $condition - model condition.
		 * @param array|null $_vids - vids in case condition is pids.
		 * @return array
		 */
		public function loadShopifySelectedProducts( $condition, $_vids = null )
		{
			$lastPid = null;
			$products = $vids = array ();
			if ( isset ( $condition['conditions'] ) ) {

				$_products = $this->_controller->{$this->_modelName}->find( 'all', $condition );
			} else {
				$_products = $condition;
			}
            $pids = array();
			if ( $_products ) {
				foreach ( $_products as $key => $_dp ) {
					if ( isset ( $condition['conditions'] ) ) {
//						$pid = $_dp[ $this->_modelName ]['pid'];
						$pids[] = $_dp[ $this->_modelName ]['pid'];
						$vids[] = $_dp[ $this->_modelName ]['vid'];
					} else {
//						$pid = $_dp;
						$pids[] = $_dp;
					}
//					if ( $lastPid != $pid ) {
//						$lastPid = $pid;
//						try {
//							$product = $this->_controller->api->get( 'products', $pid );
//						} catch ( Exception $ex ) {
//							CakeLog::write( LOG_ERROR, $ex->getMessage() );
//							continue;
//						}
//						$products[] = $product;
//					}
				}
                if (isset($pids[0])) { // isset() works a little faster then count
                    try {
                        $products = $this->_controller->api->getAll( 'products', null, array('ids' => array_unique($pids)) );
                    } catch ( Exception $ex ) {
                        CakeLog::write( LOG_ERROR, $ex->getMessage() );
                    }
                }
				if ( $_vids ) {
					$vids = $_vids;
				}
			}
			return array ( $products, $vids );
		}

		/**
		 * Returns selected products.
		 * @param array $condition - model condition.
		 * @return array
		 */
		public function loadPrestaShopSelectedProducts( $condition )
		{
			$products = array ();
			if ( isset ( $condition['conditions'] ) ) {
				$_products = $this->_controller->{$this->_modelName}->find( 'all', $condition );
			} else {
				$_products = $condition;
			}
			if ( $_products ) {
				foreach ( $_products as $_dp ) {
					if ( isset ( $condition['conditions'] ) ) {
						$pid = $_dp[ $this->_modelName ]['pid'];
					} else {
						$pid = $_dp;
					}
					$product = $this->_controller->api->get( 'products', null, array(
						'display' => '[id,name,id_default_image,price]',
						'filter[id]' => "[{$pid}]"
					) );
					$product = isset($product->products) ? $product->products->children()->product : null;
					if ( !$product ) {
						continue;
					}
					$image = Router::url(array(
						'plugin' => 'product_selector',
						'controller' => 'indexa',
						'action' => 'getPrestaShopProductImage',
						'ps_id' => (int)$product->id_default_image,
						'ps_rid' => (int)$product->id,
					), true);
					$products[] = (object) array (
						'id' => (int)$product->id,
						'handle' => '/product.php?id_product=' . (int)$product->id,
						'title' => (string)$product->name->language,
						'images' => $image,
						'variants' => array (
							(object) array (
								'id' => (int)$product->id,
								'title' => (string)$product->name->language,
								'price' => number_format((float)$product->price, 2, '.', ' '),
								'compare_at_price' => 0
							)
						)
					);
				}
			}
			return $products;
		}

		/**
		 * Returns selected products from OpenCart Platform.
		 * @param array $condition - model condition.
		 * @return array
		 */
		public function loadOpenCartSelectedProducts( $condition )
		{
			$products = array();
			if ( isset ( $condition[ 'conditions' ] ) ) {
				$_products = $this->_controller->{$this->_modelName}->find( 'all', $condition );
			} else {
				$_products = $condition;
			}
			if ( $_products ) {
				foreach ( $_products as $_dp ) {
					if ( isset ( $condition[ 'conditions' ] ) ) {
						$pid = $_dp[ $this->_modelName ][ 'pid' ];
					} else {
						$pid = $_dp;
					}
					$product = $this->_controller->api->get( 'products', null, array(
						'id' => $pid
					) );
					if ( !$product ) {
						continue;
					}
					$products[ ] = (object)array(
						'id' => $product->product_id,
						'handle' => '/index.php?route=product/product&product_id=' . $product->product_id,
						'title' => $product->name,
						'images' => $product->image ? 'http://' . $this->config( 'shopName' ) . '/image/' . $product->image : '',
						'variants' => array(
							(object)array(
								'id' => $product->product_id,
								'title' => $product->name,
								'price' => $product->special ? number_format( $product->special, 2, '.', ' ' ) : number_format( $product->price, 2, '.', ' ' ),
								'compare_at_price' => $product->special ? number_format( $product->price, 2, '.', ' ' ) : 0,
							)
						)
					);
				}
			}

			return $products;
		}

        public function getBCProductByFilter($shopId, $dp, $filter)
        {
            $dpModel = $this->_controller->{$this->_modelName};
            $filter = array_map('urlencode', $filter);

            $productList = $this->_controller->api->get( 'products', null, $filter );
            $product = is_array($productList) ? $productList[0] : $productList;

            if ( !empty($product) ) {
                $existentDP = $dpModel->find('first', array(
                    'conditions' => array(
                        $this->_modelName . '.id !=' => $dp[$this->_modelName]['id'],
                        $this->_modelName . '.pid' => $product->id,
                        $this->_parentModelName . '.shop_id' => $shopId
                    ),
                    'fields' => array($this->_modelName . '.id'),
                    'recursive' => 1
                ));

                if ($existentDP) {
                    $dpModel->id = $existentDP[$this->_modelName]['id'];
                    $dpModel->saveField('sku', $product->sku);
                    $dpModel->delete($dp[$this->_modelName]['id']);

                    $product = null;
                } else {
                    $dpModel->id = $dp[$this->_modelName]['id'];
                    $dpModel->saveField('pid', $product->id);
                }
            } else {
                if (!empty($dp[$this->_modelName]['id'])) {
                    $dpModel->delete($dp[$this->_modelName]['id']);
                }

                $product = null;
            }

            return $product;
        }
	}
