/**
 * Common data for product selector instances.
 *
 * @author Alexey Apanovich(since 2015), Polyakov Ivan
 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
 * @link http://spur-i-t.com
 * @package Product selector
 * @version 1.0.1
 */
var ProductData = {};

// Shop.
ProductData.shop = null;

// Shop's plan level.
ProductData.shopLevel = 10;

// Platform.
ProductData.platform = null;

// Plugin folder name.
ProductData.pluginFolder = null;

// Store product count.
ProductData.productCount = null;

// Limit number of selected items, 0 - unlimited
ProductData.selectionLimit = 0;

// Store money format.
ProductData.moneyFormat = null;

// Plugin controller URL.
ProductData.controllerURL = null;

// Enable categories view.
ProductData.categories;

/**
 * Initialization.
 * Loads products and makes paginator.
 */
ProductData.init = function ( instances )
{
    // Init instances.
    for ( var index in instances ) {
        instances[ index ].init();
    }
};

/**
 * Store product selector.
 *
 * @author Alexey Apanovich(since 2015), Polyakov Ivan
 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
 * @link http://spur-i-t.com
 * @package Product selector
 * @version 3.0.0
 */
var ProductSelector = function() {
    // default params
    this.params = {
        'groupFieldName': 'group-variants',
        'groupVariants': true,
        'ignoreVids': false,
        'quantity': false,
        'quantityLabel': 'Quantity: ',
        'pageLimit': 10
    };

    if ( typeof arguments[0] == 'object' ) {
        var params = arguments[0];
        // TODO: remove 4 lines below after refactoring
        this.productContainerId = params.productContainerId || null;
        this.discountContainerId = params.discountContainerId || null;
        this.usedProducts = params.usedProducts || null;
        this.usedCategories = params.usedCategories || null;
        // TODO: leave only this lines after refactoring
        // this.params.modelName = params.discountContainerId.replace('#', '');
        $.extend( this.params, arguments[0] );
        if ( !this.params.modelName ) {
            this.params.modelName = this.params.discountContainerId.replace('#', '');
        }
    } else {
        this.productContainerId = arguments[0] || null;
        this.discountContainerId = arguments[1] || null;
        this.usedProducts = arguments[2] || null;
        this.usedCategories = arguments[3] || null;
        this.params.modelName = arguments[1].replace('#', '');
    }
}

// AJAX data cache.
ProductSelector.prototype.cache;

// Categories already used in discounts.
ProductSelector.prototype.usedCategories = null;

// Products already used in discounts.
ProductSelector.prototype.usedProducts = null;

// Products on page.
ProductSelector.prototype.productsOnPage = 10;

// Page count.
ProductSelector.prototype.pageCount = null;

// Current page.
ProductSelector.prototype.currentPage = null;

// Index of a last product added to current discount.
ProductSelector.prototype.addedIndex = null;

// The sign that left block is loaded.
ProductSelector.prototype.leftBlockLoaded = false;

// The sign that right block is loaded.
ProductSelector.prototype.rightBlockLoaded = false;

// Current view.
ProductSelector.prototype.view = 'product';

// If ignoreVids is true - pids will be used to check is product used or not already
ProductSelector.prototype.ignoreVids = false;

/**
 * Initialization.
 * Loads products and makes paginator.
 */
ProductSelector.prototype.init = function ()
{
    var _this = this;
    $( this.productContainerId ).data( '_this', this );
    $( this.discountContainerId ).data( '_this', this );
    this.pageCount = Math.ceil( ProductData.productCount / this.productsOnPage );
    this.addedIndex = $( this.discountContainerId + ' input[name*="pid"]' ).length;
    this.cache = new Object();
    this.cache.pages = new Array();
    this.cache.categories = null;
    // Blocks.
    this.initFilters();
    this.displayPage( 1 );
    // View switcher event.
    if ( ProductData.categories ) {
        function switchView()
        {
            var boxTitle = $( _this.productContainerId ).parent().parent()
                .find( '.widget_header h4' );
            var categoryTitle = ProductData.platform == 'Shopify' ? 'Collections' : 'Categories';
            if ( _this.view == 'product' ) {
                _this.view = 'category';
                $(_this.productContainerId + '-filter-categories').fadeOut();
                var boxTitleText = boxTitle.html().replace( 'Products', categoryTitle );
            } else {
                _this.view = 'product';
                $(_this.productContainerId + '-filter-categories').fadeIn();
                var boxTitleText = boxTitle.html().replace( categoryTitle, 'Products' );
            }
            _this.displayPage( _this.currentPage );
            boxTitle.html( boxTitleText );
        }
        $( this.productContainerId + '-switch-products' )
            .click( function ( event ) {
                if ( _this.view == 'product' ) return;
                switchView();
            } );
        $( this.productContainerId + '-switch-categories' )
            .click( function ( event ) {
                if ( (ProductData.platform == 'BigCommerce') && (ProductData.shopLevel < 1) ) {
                    alert('Advanced tariff plan is required!');
                    return false;
                }
                if ( _this.view == 'category' ) return;
                switchView();
            } );
    }
    // Sap events.
    $( this.productContainerId + '-sap' )
        .click( function ( event ) {
            _this.addAllToDiscount();
            event.preventDefault();
        } );
    $( this.discountContainerId + '-sap' )
        .click( function ( event ) {
            _this.clearSelected();
            event.preventDefault();
        } );
    // Callback.
    if ( typeof PS_Callback_SelectorReady == 'function' ) {
        PS_Callback_SelectorReady( this );
    }
};

// Filter.
ProductSelector.prototype.filter;

// Filtered.
ProductSelector.prototype.filtered = false;

/**
 * Filter initialization.
 */
ProductSelector.prototype.initFilters = function ()
{
    var _this = this;
    this.filter = new Object();
    // Event for title input.
    $( _this.productContainerId + '-filterTitle' )
        .keypress( function ( event ) {
            if ( event.charCode == 13 ) {
                $( _this.productContainerId + '-fab' ).click();
                event.preventDefault();
            }
        } );
    // Event for filter button.
    $( this.productContainerId + '-fab' ).click( function () {
        _this.filter.category = $( _this.productContainerId + '-filterCategory' ).val();
        _this.filter.name = $( _this.productContainerId + '-filterTitle' ).val();
        _this.displayPage( 1 );
    } );
    // Group variants.
    $( this.productContainerId + '-group' ).click( function () {
        _this.setGroupState(this);
        // reload right block if reload URL is provided by App
        if ( ProductData.reloadProductsURL ) {
            _this.leftBlockLoaded = false; _this.rightBlockLoaded = false;
            _this.reloadSelectedProducts(this);
        }

        _this.displayPage( _this.currentPage );
    } );
};

ProductSelector.prototype.setGroupState = function(checkbox)
{
    this.params.groupVariants = checkbox.checked;
}

/**
 * Displays a page with products in BC Products box.
 * @param integer page - page number.
 */
ProductSelector.prototype.displayPage = function ( page )
{
    var _this = this;
    this.currentPage = page;
    if ( _this.view == 'product' ) {
        this.displayPaginator();
//		this.setPaginatorCurrentPage();
    } else {
        this.hidePaginator();
    }
    // Reloads BC Products box after ajax response coming.
    function reloadContainer( data )
    {
        if ( _this.view == 'product' ) {
            if ( ( _this.cache.pages[ page ] == undefined ) && !_this.filtered ) {
                _this.cache.pages[ page ] = data;
            }
        } else {
            if ( _this.cache.categories == undefined ) {
                _this.cache.categories = data;
            }
        }
        _this.hideLoader(_this.productContainerId);
        $( _this.productContainerId ).fadeOut( 300, function () {
            // Form html.
            var html = '';
            if ( _this.view == 'product' ) {
                var productHtml = '';
                for ( var iteration = 0; iteration < data.products.length; iteration++ ) {
                    product = data.products[ iteration ];
                    if ( typeof PS_Callback_FormProduct == 'function' ) {
                        PS_Callback_FormProduct( product );
                    }
                    eval( 'productHtml = _this.form'+ ProductData.platform +'Product( product );' );
                    if ( typeof PS_Callback_DisplayProduct == 'function' ) {
                        productHtml = PS_Callback_DisplayProduct( productHtml );
                    }
                    html += productHtml;
                }
            }
            else {
                eval( 'html = _this.form'+ ProductData.platform +'Categories( data );' );
            }
            // Insert html.
            $( _this.productContainerId ).html( '<ul>'+ html +'</ul>' );
            $( _this.productContainerId + ' a.add-link' ).click(function(e) {
                if ( $(this).data('disabled') ) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
                // return ($(this).data('disabled')) ? false : true;
            });
            _this.leftBlockLoaded = true;
            if ( (!ProductData.noSelectedProducts && _this.rightBlockLoaded) || ProductData.noSelectedProducts ) {
                _this.setATags();
            }
            $( _this.productContainerId + ' a.add-link' )
                .click( function ( event ) {
                    event.preventDefault();
                    _this.addToDiscount( this );
                } );
            $( _this.productContainerId ).fadeIn( 300 );

        } );
    }
    // Prepare params.
    var params = {
        limit: this.productsOnPage,
        page: page
    };
    if ( typeof PS_Params == 'object' ) {
        $.extend( params, PS_Params );
    }
    var display = 'block';
    this.filtered = false;
    if ( Object.keys( this.filter ).length ) {
        for ( key in this.filter ) {
            if ( this.filter[ key ] ) {
                params[ key ] = this.filter[ key ];
                this.filtered = true;
//				display = 'none';
            }
        }
//		if ( ProductData.platform == 'BigCommerce' ) {
//			$( this.productContainerId + '-paginator .dtPagination').css( 'display', display );
//		}
        if ( this.view == 'product' ) {
            if ( ProductData.platform == 'BigCommerce' || ProductData.platform == 'Shopify' || ProductData.platform == 'PrestaShop' || ProductData.platform == 'OpenCart' ) {
                var url = ProductData.controllerURL +'get'+ ProductData.platform +'ProductCount';
                $.getJSON( url, params, function ( count ) {
                    _this.pageCount = Math.ceil( count / _this.productsOnPage );
                    _this.displayPaginator();
                } );
            }
        }
    }
    // Get page from cache.
    if ( !this.filtered ) {
        if ( this.view == 'product' ) {
            if ( this.cache.pages[ page ] != undefined ) {
                reloadContainer( this.cache.pages[ page ] );
                return;
            }
        } else {
            if ( this.cache.categories != null ) {
                reloadContainer( this.cache.categories );
                return;
            }
        }
    }
    // Load page data.
    this.showLoader(this.productContainerId);
    var url = ProductData.controllerURL +'get'+ ProductData.platform + ( this.view == 'product' ? 'Products' : 'Categories' );
    $.getJSON( url, params, reloadContainer );
};

ProductSelector.prototype.reloadSelectedProducts = function(groupCheckbox) {
    if ( !ProductData.reloadProductsURL ) return true;
    var _this = this;
    this.showLoader(this.discountContainerId);
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: ProductData.reloadProductsURL,
        cache: false,
        data: 'model=' + _this.params.modelName + '&money_format=' + ProductData.moneyFormat + '&'+_this.params.groupFieldName+'=' + ((groupCheckbox.checked) ? 1 : 0) + '&' + $('select,input,textarea', this.discountContainerId).serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            $(_this.discountContainerId).html('');
            _this.hideLoader(_this.discountContainerId);
        },
        success: function(data) {
            $(_this.discountContainerId).html( data );
            _this.rightBlockLoaded = true;
            _this.addedIndex = $(_this.discountContainerId + ' input[name*="pid"]').length;
            if ( _this.leftBlockLoaded ) {
                _this.setATags();
            }
            _this.hideLoader(_this.discountContainerId);
        }
    });
}

/**
 * Check if max number of products is selected already
 */
ProductSelector.prototype.checkSelectionLimit = function() {
    if ( ProductData.selectionLimit ) {
        if ( $('ul>li', this.discountContainerId).length >= ProductData.selectionLimit ) {
            $( '.add-link', this.productContainerId ).data('disabled', true).addClass('disabled');
        } else {
            $( '.add-link', this.productContainerId ).data('disabled', false).removeClass('disabled');
        }
    }
};

/**
 * Forms html view of BC product to insert it in product list.
 * @param object product - product formed data.
 * @return string
 */
ProductSelector.prototype.formOpenCartProduct = function ( product ) {
    return this.formBigCommerceProduct( product );
};

/**
 * Forms html view of BC product to insert it in product list.
 * @param object product - product formed data.
 * @return string
 */
ProductSelector.prototype.formPrestaShopProduct = function ( product ) {
    return this.formBigCommerceProduct( product );
};

/**
 * Forms html view of BC product to insert it in product list.
 * @param object product - product formed data.
 * @return string
 */
ProductSelector.prototype.formBigCommerceProduct = function ( product )
{
    var html = '', image = '', src = '', variantSelect = '', addLink = '';
    var price = '', comparePrice = '';
    // image.
    src = '/'+ ProductData.pluginFolder +'/img/no-image-thumb.gif';
    if ( product.images ) {
        src = product.images;
    }
    image = '<img src="' + src + '" class="product-selector-thumb" alt="thumb" width="50" />';
    // price.
    price = ProductData.moneyFormat.replace( /\{\{amount\}\}/, product.variants[0].price );
    comparePrice = parseFloat( product.variants[0].compare_at_price ) ? ProductData.moneyFormat.replace( /\{\{amount\}\}/, product.variants[0].compare_at_price ) : '';
    // separate variants.
    for ( var iteration2 = 0; iteration2 < product.variants.length; iteration2++ ) {
        addLink = this.formATag( product.id, product.variants[ iteration2 ].id, product.handle );
        html += this.formLiTag(
            product.variants[ iteration2 ].id,
            image, product.handle, product.variants[ iteration2 ].title, variantSelect,
            comparePrice, price, addLink, product.isVisible
        );
    }
    return html;
};

// Category level in tree.
ProductSelector.prototype.level = 1;

// Category level prefix before name.
ProductSelector.prototype.levelPrefix = '';

/**
 * Forms html view of BC categories to insert it in product list.
 * @param object categories - categories formed data.
 * @return string
 */
ProductSelector.prototype.formBigCommerceCategories = function ( categories )
{
    var html = '';
    for ( var key in categories ) {
        var category = categories[ key ].data;
        var children = categories[ key ].children;
        html += '\
		<li id="category-'+ category.id +'" class="level-'+ this.level +'">\
			<div class="title">\
				<a href="http://'+ ProductData.shop + category.url +'" target="_blank">\
					<strong>'+ this.levelPrefix +' '+ category.name +'</strong>\
				</a>\
			</div>\
			<a href="'+ category.id +':'+ category.url +'" class="simple_buttons add-link">Add</a>\
			<div class="clear"></div>\
		</li>\
		';
        if ( children.length ) {
            ++this.level;
            this.levelPrefix += '-';
            html += this.formBigCommerceCategories( children );
            this.levelPrefix = this.levelPrefix.replace( /\-$/, '' );
            --this.level;
        }
    }
    return html;
};

/**
 * Forms html view of Shopify product to insert it in product list.
 * @param object product - product formed data.
 * @return string
 */
ProductSelector.prototype.formShopifyProduct = function ( product )
{
    var html = '', image = '', src = '', variantSelect = '', addLink = '';
    var price = '', comparePrice = '';
    // image.
    src = '/'+ ProductData.pluginFolder +'/img/no-image-thumb.gif';
    if ( product.images.length ) {
        src = product.images[0].src.replace( /\.(?![^\.]*\.)/i, '_thumb.' );
    }
    image = '<img src="' + src + '" class="product-selector-thumb" alt="thumb" width="50" />';
    // price.
    price = this.stripTags(
        ProductData.moneyFormat.replace( /\{\{\s*(\w+)\s*\}\}/, product.variants[0].price )
    );
    comparePrice = parseFloat( product.variants[0].compare_at_price ) ?
        this.stripTags(
            ProductData.moneyFormat
                .replace(/\{\{\s*(\w+)\s*\}\}/, product.variants[0].compare_at_price)
        )
        : '';
    // Grouped variants.
    if ( $( this.productContainerId + '-group').prop('checked') ) {
        // Variant select.
        variantSelect = product.variants[0].title;
        if ( product.variants.length > 1 ) {
            variantSelect = '<select id="variant-select-' + product.id + '" name="variant-select">';
            for ( var iteration2 = 0; iteration2 < product.variants.length; iteration2++ ) {
                variantSelect += '<option value="' + product.variants[ iteration2 ].id + '">' + product.variants[ iteration2 ].title + '</option>';
            }
            variantSelect += '</select>';
        }
        // Product HTML.
        var isVisible = product.published_at ? true : false;
        addLink = this.formATag( product.id, product.variants[0].id, product.handle );
        html += this.formLiTag(
            product.variants[0].id,
            image, '/products/' + product.handle, product.title, variantSelect,
            comparePrice, price, addLink, isVisible
        );
    }
    // Separate variants.
    else {
        for ( var iteration2 = 0; iteration2 < product.variants.length; iteration2++ ) {
            var isVisible = product.published_at ? true : false;
            addLink = this.formATag( product.id, product.variants[ iteration2 ].id, product.handle );
            html += this.formLiTag(
                product.variants[ iteration2 ].id,
                image, '/products/' + product.handle, product.title, product.variants[ iteration2 ].title,
                comparePrice, price, addLink, isVisible
            );
        }
    }
    return html;
};

/**
 * Forms html view of Shopify categories to insert it in product list.
 * @param object categories - categories formed data.
 * @return string
 */
ProductSelector.prototype.formShopifyCategories = function ( categories )
{
    var html = '';
    for ( var key in categories ) {
        var category = categories[ key ].data;
        var children = categories[ key ].children;
        html += '\
		<li id="category-'+ category.id +'" class="level-'+ this.level +'">\
			<div class="title">\
				<a href="http://'+ ProductData.shop +'/collections/'+ category.handle +'" target="_blank">\
					<strong>'+ this.levelPrefix +' '+ category.title +'</strong>\
				</a>\
			</div>\
			<a href="'+ category.id +':'+ category.handle +'" class="simple_buttons add-link">Add</a>\
			<div class="clear"></div>\
		</li>\
		';
        if ( children.length ) {
            ++this.level;
            this.levelPrefix += '-';
            html += this.formShopifyCategories( children );
            this.levelPrefix = this.levelPrefix.replace( /\-$/, '' );
            --this.level;
        }
    }
    return html;
};

/**
 * Forms <a> tag for product add/used button.
 * @param integer pid - product id.
 * @param integer vid - variant id.
 * @param string handle - product url.
 * @returns string
 */
ProductSelector.prototype.formATag = function ( pid, vid, handle )
{
    var link = '', href = '';
    href = pid +':'+ vid +':'+ handle;
    link = '<a href="'+ href +'" class="simple_buttons add-link">Add</a>';
    return link;
};

/**
 * Forms <li> tag for product list.
 * @param integer vid - variant id.
 * @param string image - product tumb url.
 * @param string handle - product url.
 * @param string title - product title.
 * @param string variant - variant title or <select>.
 * @param float comparePrice - product compare price.
 * @param float price - product price.
 * @param string addLink - 'add to discount' link.
 * @param bool isVisible - product visibility.
 * @returns string
 */
ProductSelector.prototype.formLiTag = function ( vid, image, handle, title, variant, comparePrice, price, addLink, isVisible )
{
    var visibilityImage = '';
    if ( !isVisible ) {
        visibilityImage = '<img class="visibility" src="/'+ ProductData.pluginFolder +'/img/show.png" alt="hidden" />';
    }
    return '\
	<li id="product-'+ vid +'">\
		'+ image +'\
		<div class="title"><a href="http://'+ ProductData.shop + handle +'" target="_blank"><strong>'+ title +'</strong></a><br />'+ variant +'</div>\
		<div class="price"><span>'+ comparePrice +'</span>'+ price +'</div>\
		'+ addLink + visibilityImage + '\
		<div class="clear"></div>\
	</li>\
	';
};

/**
 * Changes add links behaviour after all blocks are loaded.
 */
ProductSelector.prototype.setATags = function ()
{
    var _this = this;
    $( this.productContainerId + ' a.add-link' )
        .each( function ( index, element ) {
            var type = ( $( element ).parent().attr( 'id' ).indexOf( 'product' ) != -1 ) ? 'product' : 'category';
            var href = $( element ).attr( 'href' ).split( ':' );
            if ( type == 'product' ) { // @todo common method for all the same code.
                var pid = href[0], vid = href[1], handle = href[2];
                var liId = 'dp-' + vid;
                var findSelector = (_this.params.ignoreVids) ? '#'+ liId +' input[value='+ pid +']' : '#'+ liId +' input[value='+ vid +']';
                var isUsed = ((_this.ignoreVids) ? ( _this.usedProducts.indexOf( pid.toString() ) != -1 )  : ( _this.usedProducts.indexOf( vid.toString() ) != -1 ));
            } else {
                var cid = href[0], handle = href[1];
                var liId = 'dc-' + cid;
                var findSelector = '#'+ liId +' input[value='+ cid +']';
                var isUsed = ( _this.usedCategories.indexOf( cid.toString() ) != -1 );
            }
            if ( $( _this.discountContainerId ).find( findSelector ).size() ) {
                $( element ).addClass( 'added' );
                $( element ).html( '' );
            }
            else if ( isUsed ) {
                $( element ).removeClass( 'add-link' );
                $( element ).addClass( 'used' );
                $( element ).html( 'Used' );
                $( element ).off( 'click' );
                $( element ).click( function ( event ) {
                    event.preventDefault();
                } );
            }
        } );

    _this.checkSelectionLimit();
};

/**
 * Event for 'add to discount' link.
 * Adds product or category to right discount box.
 * @param object link - clicked link.
 */
ProductSelector.prototype.addToDiscount = function ( link )
{
    var href = $( link ).attr( 'href' ).split( ':' );
    var type = ( $( link ).parent().attr( 'id' ).indexOf( 'product' ) != -1 ) ? 'product' : 'category';
    if ( type == 'product' ) {
        var pid = href[0], vid = href[1], handle = href[2];
        var liId = 'dp-' + vid;
        var findSelector = this.getFindSelector(liId, pid, vid);//'#'+ liId +' input[value='+ vid +']';
    } else {
        var cid = href[0], handle = href[1];
        var liId = 'dc-' + cid;
        var findSelector = '#'+ liId +' input[value='+ cid +']';
    }
    // Exit if already added.
    if ( $( this.discountContainerId ).find( findSelector ).size() ) return false;
    // Create and fill li tag.
    var _this = this;
    var li = $( '<li>', {
        id: liId
    } );
    if ( type == 'product' ) {
        var select = $( _this.productContainerId + ' #variant-select-' + pid );
        if ( select.length > 0 && !_this.params.ignoreVids) {
            $( select ).children().each( function () {
                // ++_this.addedIndex;
                // _this.addHiddenTag( li, 'pid', pid );
                // _this.addHiddenTag( li, 'vid', this.value );
                // _this.addHiddenTag( li, 'handle', handle );
                _this.addProductFields(li, pid, this.value, handle);

                // Clear vid if was added earlier.
                var elId = _this.discountContainerId +' #dp-'+ link.value;
                if ( $( elId ) ) {
                    $( elId ).remove();
                }
            } );
        }
        else {
            _this.addProductFields(li, pid, vid, handle);
            // ++_this.addedIndex;
            // _this.addHiddenTag( li, 'pid', pid );
            // _this.addHiddenTag( li, 'vid', vid );
            // _this.addHiddenTag( li, 'handle', handle );
        }
    }
    else {
        ++_this.addedIndex;
        _this.addHiddenTag( li, 'cid', cid );
        _this.addHiddenTag( li, 'handle', handle );
    }

    var dataId = ( type == 'product' ) ? vid : cid;
    li.append( $( link ).parent().html() );
    li.find( 'a.simple_buttons' ).html( '' )
        .attr( 'id', '' )
        .attr( 'data-id', dataId )
        .attr( 'class', 'remove' )
        .attr( 'title', 'remove ' + type )
        .click( function ( event ) {
            _this.removeFromDiscount( dataId, type );
            event.preventDefault();
        } );

    if ( _this.params.quantity ) {
        _this.addInputTag($('.title', li), 'qty', 1, _this.params.quantityLabel, 'quantity');
    }

    // Append li tag.
    if ( !$( _this.discountContainerId ).html().trim() ) {
        $( _this.discountContainerId ).append( $( '<ul>' ) );
    }
    $( _this.discountContainerId + ' ul' ).append( li );
    _this.markAsAdded( link );
    _this.checkSelectionLimit();
};

/**
 * Adds all unused products to discount.
 */
ProductSelector.prototype.addAllToDiscount = function ()
{
    var _this = this, html = '', type;
    $( this.productContainerId ).find( 'li' )
        .each( function ( index, element ) {
            var link = $( element ).find( 'a.add-link' );
            if ( link.attr( 'href' ) ) {
                var href = link.attr( 'href' ).split( ':' );
                type = ( $( link ).parent().attr( 'id' ).indexOf( 'product' ) != -1 ) ? 'product' : 'category';
                if ( type == 'product' ) {
                    var pid = href[0], vid = href[1], handle = href[2];
                    var liId = 'dp-' + vid;
                    var findSelector = _this.getFindSelector(liId, pid, vid);//'#'+ liId +' input[value='+ vid +']';
                } else {
                    var cid = href[0], handle = href[1];
                    var liId = 'dc-' + cid;
                    var findSelector = '#'+ liId +' input[value='+ cid +']';
                }
                var canAdd = (
                    !$( _this.discountContainerId ).find( findSelector ).size()
                );
                if ( canAdd ) {
                    if ( type == 'product' ) {
                        var select = $('#variant-select-' + pid );
                        var inputs = '';
                        if ( select.length > 0 ) {
                            $( select ).children().each( function () {
                                ++_this.addedIndex;
                                inputs += '\
								<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][pid]" value="'+ pid +'" />\
								<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][vid]" value="'+ this.value +'" />\
								<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][handle]" value="'+ handle +'" />\
								';
                            } );
                        }
                        else {
                            ++_this.addedIndex;
                            inputs = '\
							<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][pid]" value="'+ pid +'" />\
							<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][vid]" value="'+ vid +'" />\
							<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][handle]" value="'+ handle +'" />\
							';
                        }
                    }
                    else {
                        ++_this.addedIndex;
                        inputs = '\
						<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][cid]" value="'+ cid +'" />\
						<input type="hidden" name="'+ _this.params.modelName +'['+ _this.addedIndex +'][handle]" value="'+ handle +'" />\
						';
                    }
                    html += '\
					<li id="'+ liId +'">'
                        + inputs + $( element ).html() +
                        '</li>'
                    ;
                }
            }
        } );
    if ( type == 'product' ) {
        var regexp = /<a href="(\d+):(\d+):([\w+\-\/]+)"[^>]+>[^<]*<\/a>/ig;
        var replace = '<a href="$1:$2:$3" class="remove" data-id="$2"></a>';
    } else {
        var regexp = /<a href="(\d+):([\w+\-\/]+)"[^>]+>[^<]*<\/a>/ig;
        var replace = '<a href="$1:$2" class="remove" data-id="$1"></a>';
    }
    html = html.replace( regexp, replace );
    var currentHtml = $( this.discountContainerId ).html()
        .replace( '<ul>', '' ).replace( '</ul>', '' );
    $( this.discountContainerId ).html(
        '<ul>'+ currentHtml + html +'</ul>'
    );
    $( this.discountContainerId ).find( 'a.remove' )
        .click( function ( event ) {
            $( this ).parent().parent().parent().data( '_this' )
                .removeFromDiscount( $( this ).attr( 'data-id' ), type );
            event.preventDefault();
        } );
    $( this.productContainerId ).find( '.add-link' )
        .each( function ( index, element ) {
            if ( $( element ).hasClass( 'add-link' ) ) {
                $( element ).addClass( 'added' );
                $( element ).html( '' );
            }
        } );
};

ProductSelector.prototype.addProductFields = function(parent, pid, vid, handle)
{
    ++this.addedIndex;
    this.addHiddenTag( parent, 'pid', pid );
    this.addHiddenTag( parent, 'vid', vid );
    this.addHiddenTag( parent, 'handle', handle );
}

ProductSelector.prototype.getFindSelector = function(liId, pid, vid)
{
    return (this.params.ignoreVids) ? '#'+ liId +' input[value='+ pid +']' : '#'+ liId +' input[value='+ vid +']';
}

/**
 * Clears all in discount box.
 */
ProductSelector.prototype.clearSelected = function ()
{
    var _this = this;
    $( this.discountContainerId ).find( 'li' )
        .each( function ( index, element ) {
            var id = $( element ).attr( 'id' );
            var type = ( id.indexOf( 'dp-' ) != -1 ) ? 'product' : 'category';
            id = id.replace( /(dp|dc)-/, '' );
            var li = $( _this.productContainerId + ' li#'+ type +'-'+ id );
            var a = li.find( 'a.added' );
            a.removeClass( 'added' );
            a.html( 'Add' );
        } );
    $( this.discountContainerId ).html( '' );
};

/**
 * Adds hidden tag to product selected for discount.
 * @param object li - tag.
 * @param string name - tag name.
 * @param string name - tag value.
 */
ProductSelector.prototype.addHiddenTag = function ( li, name, value )
{
    li.append( jQuery( '<input>' ).attr( {
        'type': 'hidden',
        'name': this.discountContainerId.replace( '#', '' ) +'['+ this.addedIndex +']['+ name +']',
        'value': value
    } ) );
};

/**
 * Adds input tag to product selected for discount.
 * @param object li - tag.
 * @param string name - tag name.
 * @param string name - tag value.
 */
ProductSelector.prototype.addInputTag = function ( li, name, value, label, className )
{
    var inputId = this.params.modelName +'_'+ this.addedIndex +'_'+ name;
    var label = jQuery('<label for="' + inputId + '" class="' + className + '"><span>' + label + '</span></label>');

    label.append(jQuery('<input>').attr({
        'type': 'text',
        'id': inputId,
        'name': this.params.modelName +'['+ this.addedIndex +']['+ name +']',
        'value': value,
    }));

    li.append(label);
};

/**
 * Marks a link as added.
 */
ProductSelector.prototype.markAsAdded = function ( a )
{
    $(a).addClass('added');
    $(a).html('');
};

/**
 * Event for 'remove from discount' link.
 * Removes product or category from right discount box.
 *
 * @param integer id - vid or cid.
 * @param string type - item type ( product, category )
 */
ProductSelector.prototype.removeFromDiscount = function ( id, type )
{
    var _this = this;
    var selElId = this.discountContainerId + ( type == 'product' ? ' #dp-' : ' #dc-' ) + id;
    var select = $( selElId ).find( 'select' );
    $( selElId ).remove();
    // Clear added marks.
    if ( select.length ) {
        $( select ).children().each( function ( index, element ) {
            var vid = $( element ).val();
            var elId = _this.productContainerId +' #product-'+ vid;
            if ( $( elId ) ) {
                $( elId ).children( 'a' ).removeClass( 'added' );
                $( elId ).children( 'a' ).html( 'Add' );
            }
        } );
    }
    else {
        var elId = this.productContainerId + ( type == 'product' ? ' #product-' : ' #category-' ) + id;
        if ( $( elId ) ) {
            $( elId ).children( 'a' ).removeClass( 'added' );
            $( elId ).children( 'a' ).html( 'Add' );
        }
    }

    _this.checkSelectionLimit();
};

/**
 * Displays paginator.
 */
ProductSelector.prototype.displayPaginator = function ()
{
    var innerPages = '', className = '';
    for ( var iteration = 1; iteration <= this.pageCount; iteration++ ) {
        if ( ( Math.abs( this.currentPage - iteration ) ) <= 3 ) {
            innerPages += '<a href="/" class="paginate_button">' + iteration + '</a>';
        }
    }
    var html = '\
	<div class="dtPagination pCenter">\
		<a class="first">First</a>\
		' + innerPages + '\
		<a class="last">Last</a>\
	</div>\
	';
    var _this = this;
    $( this.productContainerId + '-paginator .dtPagination' ).remove();
    $( this.productContainerId + '-paginator' ).append( html );
    $( this.productContainerId + '-paginator .first' )
        .click( function ( event ) {
            _this.displayPage( 1 );
            event.preventDefault();
        } );
    $( this.productContainerId + '-paginator .last' )
        .click( function ( event ) {
            _this.displayPage( _this.pageCount );
            event.preventDefault();
        } );
    $( this.productContainerId + '-paginator .paginate_button' )
        .click( function ( event ) {
            _this.displayPage( $( this ).html() );
            event.preventDefault();
        } );
    $( this.productContainerId + '-paginator .paginate_active' )
        .click( function ( event ) {
            event.preventDefault();
        } );

    this.setPaginatorCurrentPage();
};

/**
 * Hides paginator.
 */
ProductSelector.prototype.hidePaginator = function ()
{
    $( this.productContainerId + '-paginator .dtPagination' ).remove();
};

/**
 * Sets a current page in paginator.
 */
ProductSelector.prototype.setPaginatorCurrentPage = function ()
{
    var _this = this;
    $( this.productContainerId + '-paginator .paginate_active' )
        .attr( 'class', 'paginate_button' );
    $( this.productContainerId + '-paginator .paginate_button' )
        .each( function ( index, element ) {
            if ( _this.currentPage == $( element ).html() ) {
                $( element ).attr( 'class', 'paginate_active' );
            }
        } );
};

/**
 * Strip HTML tags
 */
ProductSelector.prototype.stripTags = function (text)
{
    if (typeof this.processDiv == 'undefined' || !this.processDiv) {
        this.processDiv = document.createElement("DIV");
    }
    this.processDiv.innerHTML = text;
    return this.processDiv.textContent || this.processDiv.innerText;
};

ProductSelector.prototype.showLoader = function(containerId)
{
    var baseImgUrl = ProductData.pluginFolder ? '/' + ProductData.pluginFolder : '';
    $(containerId + '-loader').html(
        '<img src="'+ baseImgUrl +'/img/admin/Icons/Load/load-8.gif" alt="loader" />'
    );
}

ProductSelector.prototype.hideLoader = function(containerId)
{
    $(containerId + '-loader').html('');
}
