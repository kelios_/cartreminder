<style type="text/css">

.Shopify .ps-clear { clear: both; }
.filter-apply-btn { margin-left: 10px; }

.productsBlock ul { margin: 0px; padding: 0px; list-style: none; }
.productsBlock ul li { padding: 5px; font-size: 10px; font-family: 'Droid Sans'; position: relative;}
.productsBlock ul li img { display: block; float: left; }
.productsBlock ul li > div { float: left; padding-left: 5px; }
.productsBlock ul li > div.clear { float: none; }
.productsBlock ul li > div.used { float: right; font-weight: bold; color: #888888; }
.productsBlock ul li a.simple_buttons, .productsBlock ul li a.remove { float: right; text-decoration: none; }
.productsBlock ul li a.added { width: 16px; background: url( <?php echo $this->Html->url( '/img/added.gif' ) ?> ) center top no-repeat; }
.productsBlock ul li a.remove { width: 16px; height: 16px; background: url( <?php echo $this->Html->url( '/img/trash.png' ) ?> ) left top no-repeat; }
.productsBlock ul li a.disabled {opacity: 0.5}
.productsBlock ul li a.add-link.added.disabled {opacity: 1;}
.productsBlock ul li:nth-child(odd) { background-color: #f2f2f2; }
.productsBlock ul li .title { width: 240px; max-width: 100% }
.productsBlock ul li .title p { margin: 0px; }
.productsBlock ul li .price { text-align: center; font-weight: bold; color: #687F55; }
.productsBlock ul li .price span { display: block; text-decoration: line-through; }
.productsBlock ul li select { max-width : 220px; }
.productsBlock ul li .quantity {text-align: right}
.productsBlock ul li .quantity input { max-width : 50px; }
.productsBlock a span { display: block; }
.productsBlock .dtPagination a { margin: 12px 4px; }
.hiddenDuplicates { padding-left: 10px; }
.hiddenDuplicates .label { vertical-align: text-bottom; line-height: 20px; }
.productsBlock .simple_buttons, .sap { 
	float: right; height: 18px; margin: 0px; padding: 0px 5px;
	font-size: 10px; font-weight: normal; line-height: 17px;
	text-decoration: none; white-space: nowrap;
}
.sap { margin-top: 9px; font-size: 11px; }
.productsBlock > div:first-child { max-height: 600px; overflow-y: scroll; } 
.productsBlock .visibility { margin: 4px; float: right; clear: right; }
.ps-block-actions { font-size: 13px; padding-bottom: 5px; }
.ps-block-actions label { display: inline-block; padding-right: 10px; font-weight: bold; color: #ff6600; }
.ps-block-actions label input { vertical-align: -3px; }
.ps-block-no-actions { height: 20px; }
<?php
// Style changes for embedded apps
?>
    .section-cell .product-selector-wrapper .widget_header {height: auto; margin-top: 10px}
    .section-cell .product-selector-wrapper .widget_header .widget_header_title {float: none;}
    .section-cell .product-selector-wrapper .widget_header a {float: none; display: block; text-align: right; margin-top: 1px; margin-bottom: 9px}
@media only screen and (max-width: 722px) {
    .section-cell .product-selector-filter.g_6 {width: 100%}
    .section-cell .product-selector-wrapper .productsBlock img.product-selector-thumb {display: none}
    .section-cell .product-selector-wrapper .productsBlock ul li a.remove {position: absolute; right: 7px; bottom: 7px;}
    .section-cell .dtPagination.pCenter {margin-top: 20px;}
    .section-cell  div.productsBlock .dtPagination a.first,
    .section-cell  div.productsBlock .dtPagination a.last {display: block; margin: 4px auto; max-width: 50px;}
    .section-cell  div.productsBlock .dtPagination a {margin: 7px 7px;}

}
</style>
<div class="clear"></div>
<div class="product-selector-wrapper">
    <div class="g_6">
        <?php if ( !empty($categories) ) : ?>
            <div class="ps-block-actions">
                <label><input type="radio" name="view" id="<?php echo $leftBlockId ?>-switch-products" checked="checked"> Display products' list</label>
                <label><input type="radio" name="view" id="<?php echo $leftBlockId ?>-switch-categories"> Display <?php echo $categoryTitle ?></label>
            </div>
        <?php endif ?>
        <div class="widget_header">
            <h4 class="widget_header_title wwIcon i_16_forms">
                <?php echo $leftBlockTitle ?> <span id="<?php echo $leftBlockId ?>-loader" class="product-loader"></span>
            </h4>
            <a href="#" id="<?php echo $leftBlockId ?>-sap" class="simple_buttons sap">select all on a page</a>
        </div>
        <div class="widget_contents noPadding productsBlock">
            <div id="<?php echo $leftBlockId ?>"></div>
            <div id="<?php echo $leftBlockId ?>-paginator"></div>
        </div>
    </div>
    <div class="g_6">
        <?php if ( !empty($categories) ) : ?>
            <div class="ps-block-no-actions"></div>
        <?php endif ?>
        <div class="widget_header">
            <h4 class="widget_header_title wwIcon i_16_forms">
                <?php echo $rightBlockTitle ?> <span id="<?php echo $rightBlockId ?>-loader" class="product-loader"></span>
            </h4>
            <a href="#" id="<?php echo $rightBlockId ?>-sap" class="simple_buttons sap">clear all</a>
        </div>
        <div class="widget_contents noPadding productsBlock">
            <div id="<?php echo $rightBlockId ?>"></div>
        </div>
    </div>
</div>
<div class="clear"></div>
