<ul><?php

	$index = 0;
	foreach ( $products as $product ) {
		// Image.
		$src = $product->images ?: 'no-image-thumb.gif';
		$image = $this->Html->image( $src, array (
			'alt' => 'thumb',
			'width' => '50'
		) );
		// Price.
		$price = str_replace( '{{amount}}', $product->variants[0]->price, $moneyFormat );
		$comparePrice = (float) $product->variants[0]->compare_at_price ? str_replace( '{{amount}}', $product->variants[0]->compare_at_price, $moneyFormat ) : '';
		// Separate variants.
		foreach ( $product->variants as $variant ) {
			$inputHidden = '<input type="hidden" name="'. $rightBlockId .'['. $index .'][pid]" value="'. $product->id .'" />';
			$index++;
			echo "
			<li id=\"dp-{$variant->id}\">
				<a href=\"#\" class=\"remove\" title=\"remove product\" onclick=\"$( this ).parent().parent().parent().data( '_this' ).removeFromDiscount( {$variant->id} ); return false;\"></a>
				$inputHidden $image
				<div class=\"title\"><a href=\"http://{$shop}/index.php?route=product/product&product_id={$product->id}\" target=\"_blank\"><strong>{$product->title}</strong></a></div>
				<div class=\"price\"><span>{$comparePrice}</span>{$price}</div>
				<div class=\"clear\"></div>
			</li>
			";
		}
	}

?></ul>
