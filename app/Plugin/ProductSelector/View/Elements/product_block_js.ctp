<?php echo $this->Html->script( 'ProductSelector.product-selector.js?v=1' ) ?>
<script type="text/javascript">

	$( document ).ready( function () {
		ProductData.shop = '<?php echo $shopName ?>';
		ProductData.platform = '<?php echo $platform ?>';
		ProductData.pluginFolder = '<?php echo $pluginFolder ?>';
		ProductData.productCount = <?php echo $productCount ?>;
		ProductData.moneyFormat = '<?php echo strip_tags($moneyFormat) ?>';
		ProductData.controllerURL = '<?php echo $this->Html->url( '/product_selector/indexa/' ) ?>';
		ProductData.reloadProductsURL = '<?php echo isset($reloadProductsURL) ? $reloadProductsURL : null ?>';
    <?php if ( isset($categories) ) : ?>
        ProductData.categories = <?php echo json_encode( $categories ) ?>;
    <?php endif ?>
        ProductData.shopLevel = <?php echo isset($shopLevel) ? $shopLevel : 0 ?>;
        ProductData.noSelectedProducts = <?php echo isset($noSelectedProducts) ? $noSelectedProducts : 'false' ?>;
    <?php if ( isset($selectionLimit) ) : ?>
        ProductData.selectionLimit = <?php echo $selectionLimit; ?>;
    <?php endif; ?>
		ProductData.init( PS_Containers );
	} );

</script>
