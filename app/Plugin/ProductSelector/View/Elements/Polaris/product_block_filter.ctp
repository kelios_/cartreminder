<?php
$enableTitle = !empty($enableTitle) ? $enableTitle : '';
$enableCategories = !empty($enableCategories) ? $enableCategories : '';
$platform = !empty($platform) ? $platform : 'shopify';
$collections = !empty($collections) ? $collections : [];
$leftBlockId = !empty($leftBlockId) ? $leftBlockId : '';
$checkboxName = !empty($checkboxName) ? $checkboxName : '';
$checkboxChecked = !empty($checkboxChecked) ? $checkboxChecked : 0;
?>
<div class="Polaris-Card" style="width: 50%; position: relative;"><?php if ($enableTitle) : ?>
        <div class="Polaris-Card__Section"><?php
        echo $this->Form->input('text', [
                'name' => 'filter[text]',
                'label' => 'Filter by Title',
                'type' => 'text',
                'id' => $leftBlockId . '-filterTitle',
            ]
        );
        ?>
        </div><?php endif;
    if ($enableCategories) : ?>
        <div class="Polaris-Card__Section" style="display:none;"><?php echo $this->Form->input('collection_id', [
            'type' => 'select',
            'name' => 'data[collection_id]',
            'options' => [
                @$collections
            ],
            'escape' => false,
            'empty' => 'All products',
            'id' => $leftBlockId . '-filterCategory',
            'label' => 'Filter by ' . (strtolower($platform) == 'shopify' ? 'Collections' : 'Categories'),
        ]) ?>
        </div><?php endif;
    if (strtolower($platform) == 'shopify') : ?>
        <div class="Polaris-Card__Section" id="<?php echo $leftBlockId ?>-group-wrappper" style="display:none;"><?php
        echo $this->Form->input($checkboxName, [
                'help' => false,
                'name' => $checkboxName,
                'label' => 'Group product variants',
                'type' => 'checkbox',
                'id' => $leftBlockId . '-group',
                'checked' => $checkboxChecked,
                'is_wrap' => false,
            ]
        ); ?>
        </div><?php endif ?>
    <div class="Polaris-Card__Section" style="display:none;"><?php
        echo $this->Html->tag('div',
            $this->Form->submit('Search', ['type' => 'button', 'id' => 'shopify-products-fab']),
            ['class' => 'Polaris-Stack__Item']);
        ?>
    </div>
</div>