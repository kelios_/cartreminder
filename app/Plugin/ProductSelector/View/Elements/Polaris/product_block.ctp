<?php
$leftBlockId = !empty($leftBlockId) ? $leftBlockId : '';
$rightBlockId = !empty($rightBlockId) ? $rightBlockId : '';
$rightBlockTitle = !empty($rightBlockTitle) ? $rightBlockTitle : '';
$leftBlockTitle = !empty($leftBlockTitle) ? $leftBlockTitle : '';
$categoryTitle = !empty($categoryTitle) ? $categoryTitle : '';
?>
<style type="text/css">
    .Shopify .ps-clear {
        clear: both;
    }

    .filter-apply-btn {
        margin-left: 10px;
    }

    .productsBlock ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .productsBlock ul li {
        padding: 5px;
        font-size: 10px;
        font-family: 'Droid Sans';
        position: relative;
    }

    .productsBlock ul li img {
        display: block;
        float: left;
    }

    .productsBlock ul li > div {
        float: left;
        padding-left: 5px;
    }

    .productsBlock ul li > div.clear {
        float: none;
    }

    .productsBlock ul li > div.used {
        float: right;
        font-weight: bold;
        color: #888888;
    }

    .productsBlock ul li a.simple_buttons, .productsBlock ul li a.remove {
        float: right;
        text-decoration: none;
    }

    .productsBlock ul li a.added {
        width: 16px;
        background: url( <?php echo $this->Html->url( '/img/added.gif' ) ?> ) center top no-repeat;
    }

    .productsBlock ul li a.remove {
        width: 16px;
        height: 16px;
        background: url( <?php echo $this->Html->url( '/img/trash.png' ) ?> ) left top no-repeat;
    }

    .productsBlock ul li a.disabled {
        opacity: 0.5
    }

    .productsBlock ul li a.add-link.added.disabled {
        opacity: 1;
    }

    .productsBlock ul li:nth-child(odd) {
        background-color: #f4f6f8
    }

    .productsBlock ul li .title {
        width: 240px;
        max-width: 100%
    }

    .productsBlock ul li .title p {
        margin: 0;
    }

    .productsBlock ul li .price {
        text-align: center;
        font-weight: bold;
        color: #687F55;
    }

    .productsBlock ul li .price span {
        display: block;
        text-decoration: line-through;
    }

    .productsBlock ul li select {
        max-width: 168px;
        color: #919eab;
        text-transform: none;
        letter-spacing: normal;
        position: relative;
        z-index: 4;
        width: auto;
        min-height: 2rem;
        margin: 0;
        padding: .25rem 1rem .25rem 1rem;
        background: linear-gradient(180deg,#fff,#f9fafb);
        border: 1px solid #c4cdd5;
        box-shadow: 0 0 0 1px transparent,0 1px 0 0 rgba(22,29,37,.05);
        border-radius: 3px;
        transition-property: box-shadow,background;
        transition-duration: .2s;
        transition-timing-function: cubic-bezier(.64,0,.35,1);
        line-height: 2rem;
        
    }
    .productsBlock ul li select:focus {
        outline:none;
    }
    .productsBlock ul li select:active{
        border-color: #FFFFFF;
        outline:none;
        background: #FFFFFF!important;
        box-shadow: 0 0 0 2px #3f4eae;
        transition-property: box-shadow,background;
        transition-duration: .2s;
        transition-timing-function: cubic-bezier(.64,0,.35,1);
    }
    .productsBlock ul li .quantity {
        text-align: right
    }

    .productsBlock ul li .quantity input {
        max-width: 50px;
    }

    .productsBlock a span {
        display: block;
    }

    .productsBlock .dtPagination a {
        margin: 12px 4px;
    }

    .hiddenDuplicates {
        padding-left: 10px;
    }

    .hiddenDuplicates .label {
        vertical-align: text-bottom;
        line-height: 20px;
    }

    .productsBlock .simple_buttons, .sap {
        float: right;
        height: 18px;
        margin: 0;
        padding: 0 5px;
        font-size: 10px;
        font-weight: normal;
        line-height: 17px;
        text-decoration: none;
        white-space: nowrap;
    }

    .sap {
        margin-top: 9px;
        font-size: 11px;
    }

    .productsBlock > div:first-child {
        max-height: 600px;
        overflow-y: scroll;
    }

    .productsBlock .visibility {
        margin: 4px;
        float: right;
        clear: right;
    }

    .ps-block-actions {
        font-size: 13px;
        padding-bottom: 5px;
    }

    .ps-block-actions label {
        display: inline-block;
        padding-right: 10px;
        font-weight: bold;
        color: #ff6600;
    }

    .ps-block-actions label input {
        vertical-align: -3px;
    }

    .ps-block-no-actions {
        height: 20px;
    }

    .product-selector-wrapper .widget_header {
        height: auto;
        margin-top: 10px;
    }

    .product-selector-wrapper .widget_header .widget_header_title {
        float: none;
    }

    .product-selector-wrapper .widget_header a {
        float: none;
        display: block;
        text-align: right;
        margin-top: 1px;
        margin-bottom: 9px;
    }

    div.product-selector-wrapper > div.switcher {
        max-width: 50%;
        margin-top: 15px;
    }
    div.product-selector-wrapper > div.switcher div.Polaris-FormLayout__Items {
        justify-content: space-between;
    }
    div.product-selector-wrapper > div.switcher div.Polaris-FormLayout__Item {
        max-width: 155px;
        min-width: 155px;
    }
     div.product-selector-wrapper .ps-block-no-actions {
        display: none;
    }
    div.psContent > div.Polaris-Stack__Item {
        max-width: calc(50% - .8rem);
        width: 100%;
       
    }
    div.productsBlock a.add-link:not(.added) {
        color: #fff;
        fill: #fff;
        background: linear-gradient(180deg,#6371c7,#5563c1);
        box-shadow: inset 0 1px 0 0 #6774c8,0 1px 0 0 rgba(22,29,37,.05),0 0 0 0 transparent;
        border: 1px solid #3f4eae;
        border-radius: 3px;
        text-align: center;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        text-decoration: none;
        transition-property: background,border,box-shadow;
        transition-duration: .2s;
        transition-timing-function: cubic-bezier(.64,0,.35,1);
    }
    div.productsBlock a.add-link:not(.added):hover {
        background: linear-gradient(180deg,#5c6ac4,#4959bd);
        border-color: #3f4eae;
    }
    .psContent * {
        font-family: -apple-system,BlinkMacSystemFont,San Francisco,Roboto,Segoe UI,Helvetica Neue,sans-serif;
    }
    div.productsBlock ul li .price {
        font-size: 1em;
    }
    div.productsBlock ul li .price span {
        font-size: 1.2em;   
    }
    div.productsBlock .dtPagination {
        text-align: center;
    }
    div.productsBlock .dtPagination a {
        fill: #637381;
        position: relative;
        background: linear-gradient(180deg,#fff,#f9fafb);
        border: 1px solid #c4cdd5;
        box-shadow: 0 1px 0 0 rgba(22,29,37,.05);
        border-radius: 3px;
        color: #212b36;
        text-align: center;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        text-decoration: none;
        transition-property: background,border,box-shadow;
        transition-duration: .2s;
        transition-timing-function: cubic-bezier(.64,0,.35,1);
        display: inline-block;
        margin: 12px 0px;
        padding: 0 11px;
    }
    div.productsBlock .dtPagination a:hover {
        background: linear-gradient(180deg,#f9fafb,#f4f6f8);
        border-color: #c4cdd5;
    }
    @media only screen and (max-width: 722px) {
        .product-selector-wrapper .productsBlock img.product-selector-thumb {
            display: none;
        }

        .product-selector-wrapper .productsBlock ul li a.remove {
            position: absolute;
            right: 7px;
            bottom: 7px;
        }

        .dtPagination.pCenter {
            margin-top: 20px;
        }

        div.productsBlock .dtPagination a.first,
        div.productsBlock .dtPagination a.last {
            display: block;
            margin: 4px auto;
            max-width: 50px;
        }

        div.productsBlock .dtPagination a {
            margin: 7px 7px;
        }
    }
</style>
<div class="clear"></div>
<div class="product-selector-wrapper"><?php if (!empty($categories)) : ?>
        <div class="switcher"><?php
        echo $this->Form->input('view',
            [
                'id'=>$leftBlockId.'-switch-',
                'name'=>'view',
                'type'=>'radio',
                'options' => [
                    'products'=>[
                        'name' => 'Display products\' list',
                        'checked'=>true,
                    ],
                    'categories'=>[
                        'name' => 'Display '.$categoryTitle,
                    ],
                ]
            ]);
        ?></div><?php endif; ?>
    <div class="Polaris-Stack Polaris-Stack--spacingTight Polaris-Stack--distributionEqualSpacing psContent">
        <div class="Polaris-Stack__Item">
            <div class="widget_header">
                <h4 class="widget_header_title wwIcon i_16_forms"><?php echo $leftBlockTitle ?> <span
                        id="<?php echo $leftBlockId ?>-loader" class="product-loader"></span>
                </h4>
                <a href="#" id="<?php echo $leftBlockId ?>-sap" class="simple_buttons sap">select all on a page</a>
            </div>
            <div class="widget_contents noPadding productsBlock">
                <div id="<?php echo $leftBlockId ?>"></div>
                <div id="<?php echo $leftBlockId ?>-paginator"></div>
            </div>
        </div>
        <div class="Polaris-Stack__Item"><?php if (!empty($categories)) : ?>
                <div class="ps-block-no-actions"></div><?php endif; ?>
            <div class="widget_header">
                <h4 class="widget_header_title wwIcon i_16_forms"><?php echo $rightBlockTitle ?>
                    <span id="<?php echo $rightBlockId ?>-loader" class="product-loader"></span>
                </h4>
                <a href="#" id="<?php echo $rightBlockId ?>-sap" class="simple_buttons sap">clear all</a>
            </div>
            <div class="widget_contents noPadding productsBlock">
                <div id="<?php echo $rightBlockId ?>"></div>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
