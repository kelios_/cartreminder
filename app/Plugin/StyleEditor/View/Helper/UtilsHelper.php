<?php
App::uses('AppHelper', 'View');
//??App::uses('Hash', 'Utility');

/**
 * Utils helper
 *
 * @property FormHelper $Form
  */

class UtilsHelper extends AppHelper
{
    public $helpers = array('Form' => 'StyleEditor.ThemeForm');

    public function link($title, $url = null, $options = array(), $confirmMessage = false)
    {
        if (!empty($title)) {
            $title = "<div>$title</div>";
            $options['escape'] = false;
        }

        return $this->Form->Html->link($title, $url, $options, $confirmMessage);
    }

    public function color($fieldName, $options = array())
    {
        $default = array(
            'between' => '',
            'after' => '
                <div class="color-picker-sliders">
                    <div class="control-slider color-slider color-slider-red"><div class="color-slider-label label">Red:</div><div class="slider"><div class="slider-inner"></div></div><input class="simple_field color-slider-input" type="text" maxlength="3" size="3" /></div>
                    <div class="control-slider color-slider color-slider-green"><div class="color-slider-label label">Green:</div><div class="slider"><div class="slider-inner"></div></div><input class="simple_field color-slider-input" type="text" maxlength="3" size="3" /></div>
                    <div class="control-slider color-slider color-slider-blue"><div class="color-slider-label label">Blue:</div><div class="slider"><div class="slider-inner"></div></div><input class="simple_field color-slider-input" type="text" maxlength="3" size="3" /></div>
                </div>

                <div class="color-picker-swatch ui-widget-content ui-corner-all"></div>
            ',
            'div' => array(
                'class' => 'color-picker-wrapper'
            ),
            'label' => array(
                'class' => 'color-picker-label label'
            ),
            'class' => 'simple_field color-slider-input',
            'type' => 'color'
        );

        if (isset($options['label']) && is_string($options['label'])) {
            $options['label'] = array('text' => $options['label']);
        }
        if (isset($options['div']['class'])) {
            $options['div']['class'] = $default['div']['class'] . ' ' . $options['div']['class'];
        } else {
            $options['div']['class'] = $default['div']['class'] . ' color-picker';
        }

        $options = Hash::merge(
            $default,
            $options
        );

        return $this->Form->input($fieldName, $options);
    }

    public function bgColor($fieldName, $options = array())
    {
        if (!isset($options['div'])) {
            $options['div'] = array();
        }
        $options['div']['class'] = ( isset($options['div']['class']) ? $options['div']['class']  : 'bg-color-picker' );

        return $this->color($fieldName, $options);
    }

    public function borderColor($fieldName, $options = array())
    {
        if (!isset($options['div'])) {
            $options['div'] = array();
        }
        $options['div']['class'] = ( isset($options['div']['class']) ? $options['div']['class'] : 'border-color-picker' );

        return $this->color($fieldName, $options);
    }

    public function numberSlider($fieldName, $options, $addClass = '')
    {
        $default = array(
            'between' => '<div class="slider"><div class="slider-inner"></div></div>',
            'after' => '',
            'div' => array(
                'class' => 'control-slider number-slider ' . $addClass
            ),
            'label' => array(
                'class' => 'number-slider-label'
            ),
            'class' => 'simple_field number-slider-input',
            'type' => 'number-selector',
            'maxlength' => '4',
            'size' => '4',
        );

        if (isset($options['label']) && is_string($options['label'])) {
            $default['label']['text'] = $options['label'];
            unset($options['label']);
        }
        if (isset($options['div']['class'])) {
            $default['div']['class'] = $default['div']['class'] . ' ' . $options['div']['class'];
            unset($options['div']['class']);
        }

        $options = Hash::merge(
            $default,
            $options
        );

        return $this->Form->input($fieldName, $options);
    }

    public function fontSize($fieldName, $options = array())
    {
        return $this->numberSlider($fieldName, $options, 'number-slider-font');
    }

    public function borderWidth($fieldName, $options = array())
    {
        return $this->numberSlider($fieldName, $options, 'number-slider-border');
    }

    public function borderRadius($fieldName, $options = array())
    {
        return $this->numberSlider($fieldName, $options, 'number-slider-border-radius');
    }

    public function height($fieldName, $options = array())
    {
        return $this->numberSlider($fieldName, $options, 'number-slider-height');
    }

    public function opacity($fieldName, $options = array())
    {
        if (!isset($options['div'])) {
            $options['div'] = array();
        }
        $options['div']['class'] = ( isset($options['div']['class']) ? $options['div']['class'] : 'number-slider-opacity' );

        return $this->numberSlider($fieldName, $options);
    }

    public function radioSelector($fieldName, $options = array(), $divClass = '')
    {
        $default = array(
            'between' => '',
            'after' => '',
            'div' => array(
                'class' => 'radio-selector ' . $divClass
            ),
            'label' => array(
                'class' => 'radio-selector-label'
            ),
            'class' => 'simple_field',
            'type' => 'radio-selector',
            'options' => array()
        );

        if (isset($options['label']) && is_string($options['label'])) {
            $default['label']['text'] = $options['label'];
            unset($options['label']);
        }
        if (isset($options['div']['class'])) {
            $default['div']['class'] = $default['div']['class'] . ' ' . $options['div']['class'];
            unset($options['div']['class']);
        }

        $options = Hash::merge(
            $default,
            $options
        );

        return $this->Form->input($fieldName, $options);
    }

    public function fontFamily($fieldName, $options = array())
    {
        $default = array(
            'options' => array(
                'inherit' => 'Default',
                'Arial' => 'Arial',
                'Courier New' => 'Courier New',
                'Georgia' => 'Georgia',
                'sans-serif' => 'Sans-serif',
                'Tahoma' => 'Tahoma',
                'Times New Roman' => 'Times Roman',
                'Trebuchet MS' => 'Trebuchet',
                'Verdana' => 'Verdana',
            ),
            'value' => 'inherit'
        );

        $options = array_merge($default, $options);


        return $this->radioSelector($fieldName, $options, 'font-selector');
    }

    public function borderStyle($fieldName, $options = array())
    {
        if (!isset($options['options'])) {
            $options['options'] = array(
                'default' => 'Auto',
                'solid' => 'Solid',
                'dotted' => 'Dotted',
                'dashed' => 'Dashed',
                'groove' => 'Groove',
                'ridge' => 'Ridge',
                'inset' => 'Inset',
                'outset' => 'Outset',
                'none' => 'None'
            );
        }
        return $this->radioSelector($fieldName, $options, 'border-selector');
    }


    public function fontStyle($fieldName, $options = array())
    {
        $default = array(
            'between' => '<div class="font-style-buttons">',
            'after' => '</div>',
            'div' => array(
                'class' => 'font-style-wrapper '
            ),
            'label' => array(
                'class' => 'font-style-label'
            ),
//            'class' => 'simple_field',
            'type' => 'font-style',
        );

        if (isset($options['label']) && is_string($options['label'])) {
            $default['label']['text'] = $options['label'];
            unset($options['label']);
        }
        if (isset($options['div']['class'])) {
            $default['div']['class'] = $default['div']['class'] . ' ' . $options['div']['class'];
            unset($options['div']['class']);
        } else {
            $default['div']['class'] = $default['div']['class'] . ' ' . 'font-style';
        }
        if (isset($options['id'])) {
            $default['div']['id'] = $options['id'];
            unset($options['id']);
        }

        $options = Hash::merge(
            $default,
            $options
        );

        return  $this->Form->input($fieldName, $options);
    }

}