<?php echo $this->Html->css('StyleEditor.spectrum'); ?>
<?php echo $this->Html->script('StyleEditor.spectrum.min'); ?>
<?php echo $this->Html->css('StyleEditor.controls'); ?>
<?php echo $this->Html->script('StyleEditor.controls'); ?>
<?php if ( !empty($init) ) { echo $this->Html->script('StyleEditor.init'); } ?>