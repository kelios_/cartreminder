/*******************************************************
 *
 * Color Picker with Sliders
 * @author Alexey Apanovich (http://spur-i-t.com)
 *
 *******************************************************/
;(function($, window, document, undefined){
    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "colorSliders",
        defaults = {
            prefix: 'Style',
            testPrefix: 't_',
            type: 'color',
            subClass: ''
        };

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = $(element);
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.red = $('.color-slider-red .slider-inner', this.element);
        this.green = $('.color-slider-green .slider-inner', this.element);
        this.blue = $('.color-slider-blue .slider-inner', this.element);
        this.swatch = $( ".color-picker-swatch", this.element );
        this.input = this.element.children('input');
        if (!this.settings.objectClass) {
            this.objectClass = '.' + this.settings.testPrefix + ((this.settings.prefix == '') ? this.input.attr('id') : this.input.attr('id').replace(this.settings.prefix, ''));
        } else {
            this.objectClass = this.settings.objectClass;
        }


        this.init();
    }

    function hexFromRGB(r, g, b) {
        var hex = [
            r.toString( 16 ),
            g.toString( 16 ),
            b.toString( 16 )
        ];
        $.each( hex, function( nr, val ) {
            if ( val.length === 1 ) {
                hex[ nr ] = "0" + val;
            }
        });
        return hex.join( "" ).toUpperCase();
    }

    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    Plugin.prototype = {
        init: function () {
            var _this = this;
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
            $( ".slider .slider-inner", this.element ).slider({
                orientation: "horizontal",
                range: "min",
                max: 255,
                value: 0,
                slide: this.refreshSwatch,
                change: this.refreshSwatch
            });
            $( ".color-slider-input", this.element).change(function(e,a){
                var val = e.target.value;
                if ( (val < 0) || (val > 255) ) {
                    val = (val > 255) ? 255 : 0;
                    e.target.value = val;
                }
                $(e.target).prev().children().slider("value",val);
            });
            if ( typeof $.fn.spectrum == 'function' ) {
                this.swatch.spectrum({
                    showInput: true,
                    showInitial: true,
                    preferredFormat: "hex",
                    change: function(color) {
                        var rgb = color.toRgb();
                        _this.setRGBValues(rgb.r, rgb.g, rgb.b);
                    },
                    beforeShow: function(color) {
                        _this.swatch.spectrum('set', _this.input.val())
                    }
                });
            }

            this.setEventHandler();
        },
        start: function() {
            this.setInitialColor(this.element, this.settings);
        },
        setEventHandler: function() {
            if (this.settings.subClass == '') {
                return;
            }
            var cssParam = this.settings.type;
            var plugin = this;
            switch(this.settings.subClass) {
                case 'hover':
                    $(this.objectClass).mouseover(function(){
                        var color = plugin.input.val();
                        var el = $(this);
                        var prevCssParam = cssParam;
                        if ( cssParam == 'border-color' ) {
                            prevCssParam = 'borderTopColor';
                        }
                        el.data('prev' + cssParam , el.css(prevCssParam));
                        el.css(cssParam, color);
                    });
                    $(this.objectClass).mouseout(function(){
                        var el = $(this);
                        if (el.data('prev' + cssParam)) {
                            el.css(cssParam, el.data('prev' + cssParam));
                            el.data('prev' + cssParam, null)
                        }
                    });
                    break;
                default:
            }
        },
        setInitialColor: function($pickerParent, settings) {
            var hexColor = $pickerParent.children('input').val();
            if (hexColor == '') return;
            var rgb = hexToRgb(hexColor);

            this.setRGBValues(rgb.r, rgb.g, rgb.b)
        },
        setRGBValues: function(r, g, b) {
            this.red.slider('value', r);
            this.green.slider('value', g);
            this.blue.slider('value', b);
        },

        refreshSwatch : function (a,b) {
            var picker = $(a.target).parents('.color-picker-wrapper');
            var plugin = picker.data('plugin_' + pluginName);
            var red = plugin.red.slider( "value"),
                green = plugin.green.slider( "value"),
                blue = plugin.blue.slider( "value");

            // Update color component(r,g,b) field
            $(a.target).parent().next().val(b.value);

            var hexColor = "#" + hexFromRGB( red, green, blue );
            if (a.type == 'slidechange') {
                // Update hidden color field
                plugin.input.val(hexColor);
                if (plugin.settings.subClass == '') {
                    if ( typeof plugin.settings.type == 'function' ) {
                        plugin.settings.type(hexColor);
                    } else {
                        $(plugin.objectClass).css(plugin.settings.type, hexColor);
                    }

                }
            }
            // Update swatch
            plugin.swatch.css( "background-color", hexColor );
        }

    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ pluginName ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            }

            $.data( this, "plugin_" + pluginName).start();
        });
    };

})(jQuery, window, document);


/*******************************************************
 *
 * Font Style Switcher
 * @author Alexey Apanovich (http://spur-i-t.com)
 *
 *******************************************************/
;(function($, window, document, undefined){
    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "fontStyles",
        defaults = {
            prefix: 'Style',
            testPrefix: 't_',
            subClass: '',
            objectClass: null
        };

    function Button ( element, options ) {
        var defaults = {
            'css' : null,
            'objectClass' : null,
            'values' : { 'on' : '1', 'off' : '0'},
            'subClass' : ''
        };
        this.settings = $.extend( {}, defaults, options );
        this.element = element;
        this.input = $('input', element);
        if (this.settings.objectClass) {
            this.objects = $(this.settings.objectClass);
        } else {
            this.objects = null;
        }

        this.init();
    }

    Button.prototype = {
        init : function() {
            var button = this;
            this.element.click(function(){
                var val = button.settings.values.off;
                if (button.element.hasClass('font-style-button-active')) {
                    button.element.removeClass('font-style-button-active');
                } else {
                    val =  button.settings.values.on;
                    button.element.addClass('font-style-button-active');
                }
                button.setObjectsStyle(val);
                button.input.val(val);
            });

            this.setEventHandler();
        },

        setStyle : function() {
            var val = this.input.val();
            if (val) {
                if (this.settings.css) {
                    this.setObjectsStyle(val);
                }
                if (val == this.settings.values.on) {
                    this.setOnState();
                }
            }
        },

        setObjectsStyle: function(val) {
            if (this.settings.subClass == '') {
                this.objects.css(this.settings.css, val);
            }
        },

        setOnState: function() {
            if (!this.element.hasClass('font-style-button-active')) {
                this.element.addClass('font-style-button-active');
            }
        },

        setEventHandler: function() {
            if (this.settings.subClass == '') {
                return;
            }
            var cssParam = this.settings.css;
            var button = this;
            switch(this.settings.subClass) {
                case 'hover':
                    button.objects.mouseover(function(){
                        var color = button.input.val();
                        var el = $(this);
                        el.data('prev' + cssParam , el.css(cssParam));
                        el.css(cssParam, color);
                    });
                    button.objects.mouseout(function(){
                        var el = $(this);
                        if (el.data('prev' + cssParam)) {
                            el.css(cssParam, el.data('prev' + cssParam));
                        }
                    });
                    break;
                default:
            }
        }
    };

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = $(element);
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        if (!this.settings.objectClass) {
            this.objectClass = '.' + this.settings.testPrefix + ((this.settings.prefix == '') ? this.element.attr('id') : this.element.attr('id').replace(this.settings.prefix, ''));
        } else {
            this.objectClass = this.settings.objectClass;
        }

        this.init();
    }

    Plugin.prototype = {
        init: function () {
            this.buttons = [
                new Button($('.font-style-weight', this.element), {
                    'css' : 'font-weight',
                    'objectClass' : this.objectClass,
                    'values' : {'on' : 'bold', 'off' : 'normal'},
                    'subClass' : this.settings.subClass
                }),
                new Button($('.font-style-style', this.element), {
                    'css' : 'font-style',
                    'objectClass' : this.objectClass,
                    'values' : {'on' : 'italic', 'off' : 'normal'},
                    'subClass' : this.settings.subClass
                }),
                new Button($('.font-style-decoration', this.element), {
                    'css' : 'text-decoration',
                    'objectClass' : this.objectClass,
                    'values' : {'on' : 'underline', 'off' : 'none'},
                    'subClass' : this.settings.subClass
                })
            ];
        },
        start: function() {
            this.setInitialFontStyle(this.element, this.settings, this.buttons);
        },
        setInitialFontStyle: function(element, settings, buttons) {
            for (var index in buttons) {
                buttons[index].setStyle();
            }
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ pluginName ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            }

            $.data( this, "plugin_" + pluginName).start();
        });
    };

})(jQuery, window, document);


/*******************************************************
 *
 * Number input with Slider
 * @author Alexey Apanovich (http://spur-i-t.com)
 *
 *******************************************************/
;(function($, window, document, undefined){
    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "numberSlider",
        defaults = {
            objectClass: null,
            prefix: 'Style',
            testPrefix: 't_',
            type: 'font-size',
            subClass: '',
            min: 10,
            max: 40,
            step: 1,
            units: 'px'
        };

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = $(element);
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.slider = $('.slider-inner', this.element);
        this.input = this.element.children('input');
        if (!this.settings.objectClass) {
            this.objectClass = '.' + this.settings.testPrefix + ((this.settings.prefix == '') ? this.input.attr('id') : this.input.attr('id').replace(this.settings.prefix, ''));
        } else {
            this.objectClass = '.' + this.settings.objectClass;
        }

        this.init();
    }

    Plugin.prototype = {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
            this.slider.slider({
                orientation: "horizontal",
                range: "min",
                max: this.settings.max,
                min: this.settings.min,
                step: this.settings.step,
                value: 0,
                slide: this.updateField,
                change: this.updateField
            });
            var control = this;
            $( ".number-slider-input", this.element).change(function(e,a){
                var val = control.getInputValue();
                if ( (val < control.settings.min) || (val > control.settings.max) ) {
                    val = (val > control.settings.max) ? control.settings.max : control.settings.min;
                }
                control.setInputValue(val);
                control.slider.slider("value",val);
            });

            this.setEventHandler();
        },
        start: function() {
            this.setInitialNumber(this.element, this.settings);
        },
        setEventHandler: function() {
            if (this.settings.subClass == '') {
                return;
            }
            var cssParam = this.settings.type;
            var plugin = this;
            switch(this.settings.subClass) {
                case 'hover':
                    $(this.objectClass).mouseover(function(){
                        var color = plugin.prepareInputValue();
                        var el = $(this);
                        el.data('prev' + cssParam , el.css(cssParam));
                        el.css(cssParam, color);
                    });
                    $(this.objectClass).mouseout(function(){
                        var el = $(this);
                        if (el.data('prev' + cssParam)) {
                            el.css(cssParam, el.data('prev' + cssParam));
                        }
                    });
                    break;
                default:
            }
        },
        setInitialNumber: function($pickerParent, settings) {
            var value = this.getInputValue();

            this.slider.slider('value', value);
        },
        setInputValue: function(val) {
            this.input.val(val + this.settings.units);
        },
        getInputValue: function() {
            var value = parseInt(this.input.val());
            if (isNaN(value)) {
                value = this.settings.min
            }

            return value;
        },
        prepareInputValue: function() {
            var value = this.getInputValue();

            if (this.settings.type == 'opacity') {
                value = value/100;
            }

            return value;
        },
        updateField: function(a,b) {
            var inputValue;
            var control = $(a.target).parents('.number-slider').data('plugin_' + pluginName);
            control.setInputValue(b.value);
            if (a.type == 'slidechange') {
                // Update hidden color field
                if (control.settings.subClass == '') {
                    inputValue = control.prepareInputValue();
                    if ( typeof control.settings.type == 'function') {
                        control.settings.type(inputValue);
                    } else {
                        $(control.objectClass).css(control.settings.type, inputValue); //input.val()
//                        console.log(control.input);
                        control.input.trigger('styles:change', [control.settings.type, inputValue]);
                    }
                }
            }
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ pluginName ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            }

            $.data( this, "plugin_" + pluginName).start();
        });
    };

})(jQuery, window, document);



/*******************************************************
 *
 * Radio selector control
 * @author Alexey Apanovich (http://spur-i-t.com)
 *
 *******************************************************/
;(function($, window, document, undefined){
    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "radioSelector",
        defaults = {
            objectClass: null,
            prefix: 'Style',
            testPrefix: 't_',
            type: '',
            subClass: '',
            useUniform: true
        };

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = $(element);
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.radios = $('.radio-selector-options input:radio', this.element);
        this.input = this.element.children('input');
        if (!this.settings.objectClass) {
            this.objectClass = '.' + this.settings.testPrefix + ((this.settings.prefix == '') ? this.input.attr('id') : this.input.attr('id').replace(this.settings.prefix, ''));
        } else {
            this.objectClass = '.' + this.settings.objectClass;
        }

        this.init();
    }

    Plugin.prototype = {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
           var control = this;
            this.radios.change(function(e) {
                control.setCssValue(e.target.attributes.value.value);
            });

            this.setEventHandler();
        },
        start: function() {
            this.setInitialState(this.element, this.settings);
        },
        setEventHandler: function() {
            if (this.settings.subClass == '') {
                return;
            }
            var cssParam = this.settings.type;
            var control = this;
            var testElement = null;
            switch(this.settings.subClass) {
                case 'hover':
                    $(this.objectClass).mouseover(function(){
                        var value = control.radios.filter(':checked').val();
                        testElement = $(this);
                        testElement.data('prev' + cssParam , testElement.css(cssParam));
                        testElement.css(cssParam, value);
                        testElement = null;
                    });
                    $(this.objectClass).mouseout(function(){
                        testElement = $(this);
                        if (testElement.data('prev' + cssParam)) {
                            testElement.css(cssParam, testElement.data('prev' + cssParam));
                        }
                        testElement = null;
                    });
                    break;
                default:
            }
        },
        setInitialState: function(element, settings) {
            var value = this.getInputValue();
            var selector = '[value="' + value + '"]';
            this.radios.filter(selector).prop('checked', true);
            if (settings.useUniform) {
                $.uniform.update(this.radios);
            }
            this.setCssValue(value);
        },
        getInputValue: function() {
            return this.input.val();
        },
        setCssValue: function(value) {
            if (this.settings.subClass == '') {
                $(this.objectClass).css(this.settings.type, value);
            }
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[ pluginName ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            }

            $.data( this, "plugin_" + pluginName).start();
        });
    };

})(jQuery, window, document);