$(function () {
        // Colors
        $('.color-picker').colorSliders();
        $('.bg-color-picker').colorSliders({
            type: 'background-color'
        });
        $('.border-color-picker').colorSliders({
            type: 'border-color'
        });

        // FontFamily
        $('.font-selector').radioSelector({
            type: 'font-family'
        });

        // Font Sizes
        $('.number-slider-font').numberSlider({
            min: 8,
            max: 24,
            step: 1,
            units: 'px'
        });

        // Border width
        $('.number-slider-border').numberSlider({
            min: 0,
            max: 20,
            step: 1,
            units: 'px',
            type: 'border-width'
        });

        // Border radius
        $('.number-slider-border-radius').numberSlider({
            min: 0,
            max: 20,
            step: 1,
            units: 'px',
            type: 'border-radius'
        });

        // Border styles
        $('.border-selector').radioSelector({
            type: 'border-style'
        });

        // Font styles
        $('.font-style').fontStyles();
});