<?php

/**
 * Configuration information for Application
 */
$config = array(
    'AppConf' => array(
        //General
        'api_format'    => 'json',
        'app_key'       => '8084940a149d498df662d8082e7c6a5b',
        'app_name'      => 'Hurry Up To Buy',
        'appstore_url'  => 'https://apps.shopify.com/hurry_up_to_buy',
        'auth_type'     => CONFIG_AUTH_TYPE_TEST,
        'server_type'   => 'test',
        'session_key'   => '__Shopify_hub',
        'app_id'        => 38,
        'metafields_ns'     => 'sh_hub',

        // Webhooks to install
        'webhooks' => array(
            'app/uninstalled'   => array('controller' => 'webhooks', 'action' => 'uninstall_app'),
            'products/delete'   => array('controller' => 'webhooks', 'action' => 'target_delete', 'target_products'),
            'collections/delete'   => array('controller' => 'webhooks', 'action' => 'target_delete', 'target_collectins')
        ),

        // Plugin Center params
        'app_platform'  => 'shopify',
        'cookie_names'  => array(
            'installed' => 'shopify-hurry-up-to-buy-installed',
            'user'      => 'shopify-hurry-up-to-buy-user'   
        ),
        'crypt_key'     => 'GZNYBWF',
        'plugins_center'=> 'https://ecommerce-pc.spur-i-t.com/plugin-center/shopify',
        'plugins' => array(
            'platform' => 'Shopify',
            'appFolder' => 'hurry-up-to-buy',
            'ps' => array(
                'modelName' => 'Target'
            )
        ),

        // Amazon API params
        'amazonAPI' => array(
            'key'    => 'AKIAJ2NC2OSCM35D4GZA',
            'secret' => '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',
            'host'   => 's3.amazonaws.com',
            'bucket' => 'shopify-apps',
            'folder' => 'julia_app_test',
        ),

        'default_settings' => array(
            'switches' => array(
                'visitors' => 1,
                'quantity' => 1,
                'hurry_up' => 1
            ),
            'messages' => array(
                'visitors' => '🔥 [number] people watching this page',
                'quantity' => '🎁 [quantity] items in stock',
                'hurry_up' => '🛒 Hurry up to buy',
                'alt_visitors' => '🔥 [number] person watching this page',
                'alt_quantity' => '🎁 [quantity] item in stock',
                'alt_hurry_up' => '🛒 Hurry up to buy'
            ),
            'fake_visitors' => 0,
            'fake_stock' => 0,
            'position' => 'after',
            'icon' => '&#125;',
            'alt_icon' => '&#125;',
            'selector' => '#addToCart'
        ),

        'default_styles' => array(
            "bgColor" => "#FFFFFF",
            "borderColor" => "#FFFF00",
            "borderWidth" => "0px",
            "borderRadius" => "0px",
            "borderStyle" => "none",
            "font" => "inherit",
            "leftTextColor" => "#000000",
            "leftFontSize" => "18px",
            "leftStyleWeight" => "normal",
            "leftStyleStyle" => "normal",
            "leftStyleDecoration" => "none",
            "rightTextColor" => "#000000",
            "rightFontSize" => "20px",
            "rightStyleWeight" => "normal",
            "rightStyleStyle" => "normal",
            "rightStyleDecoration" => "",
            "iconBgColor" => "#FFFFFF",
            "iconBorderRadius" => "0px",
            "iconSize" => "45px",
            "iconOpacity" => "100%",
            "iconColor" => "#000000"
        )
    )
);

if ($config['AppConf']['server_type'] == 'dev') {
    $config['AppConf']['plugins_center'] = 'https://ihnat.spurit.loc/plugin-center/shopify';
}elseif ($config['AppConf']['server_type'] == 'test') {
    $config['AppConf']['plugins_center'] = 'http://testing.ecommerce-pc.spur-i-t.com/plugin-center/shopify';
    $config['AppConf']['amazonAPI']['folder'] = $config['AppConf']['amazonAPI']['folder'] . '-testing';
    $config['AppConf']['amazonAPI']['bucket'] = 'shopify-apps';
}

$config['AppConf']['amazonAPI']['folderStore'] = $config['AppConf']['amazonAPI']['folder'] . '/store/';
$config['AppConf']['amazonAPI']['folderStoreImg'] = $config['AppConf']['amazonAPI']['folderStore'] . 'img/';
$config['AppConf']['amazonAPI']['folderJs'] = $config['AppConf']['amazonAPI']['folder'] . '/js/';
$config['AppConf']['amazonAPI']['commonJsUrl'] = 'https://'. $config['AppConf']['amazonAPI']['host']
    .'/'.$config['AppConf']['amazonAPI']['bucket']
    .'/'.$config['AppConf']['amazonAPI']['folderJs'].'common.js';