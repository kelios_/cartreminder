<?php
$config = array('Modify' => array (
    
    /**
     * List of liquid templates to which some data should be added
     * 
     * Key {template key} can be '{template_dir}/{template_name}' or '{template_name}', 
     * if {template_dir} is absent then directory 'templates' will be used.
     * {template_dir} - templates directory in liquid theme(layouts, templates, assets, snippets)
     * {template_name} - name of the template as it shown in template editor of shopify administration
     * 
     * '{template key}' => array( 
     *      'pattern' => {regexp pattern},  //what to find in liquid template
     *      'data'    => '{some string}',   //what to insert in found place
     *      'insert'  => '[before|after|replace]' //insert mode
     * )
     */
    /* Default rules - applies to a theme if not defined a separate rules for the theme. */
    'default'       => array(
    ),
));