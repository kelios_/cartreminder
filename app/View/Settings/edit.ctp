<?php $this->start('script'); ?>
    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
    <?php echo $this->Html->script('jquery-ui-1.9.2.custom.min.js'); ?>
    <?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php $this->end('script'); ?>

<div class="content">
	<div class="page-header">
  		<h1><?php echo $currentPage; ?></h1>
	</div>

	<?php echo $this->Form->create('Shop'); ?>
		<div class="panel panel-default">
  			<div class="panel-heading">
    			<h3>Step 1. Configure Widget Phrases And Icons</h3>
  			</div>
  			<div class="panel-body">
  				<div class="box notice">
    				<div class="media">
  						<div class="media-left">
  							<span class="ico-notice"></span>
  						</div>
  						<div class="media-body">
    						<p class="media-heading">Select and edit phrases to be shown on product page(s). If you check both of them, then you can also customize a motivational message to your customers.</p>
    						<p class="media-heading">Use <strong>[number]</strong> and <strong>[quantity]</strong> variables in order to display them in your message. <?php echo $this->Html->link('Design can be adjusted here.', array('action' => 'design'), array('target' => 'blank')); ?></p>
                            <p class="media-heading">You are allowed to use your own icons, copied from some other websites and paste them right into the fields.</p>
    					</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 left-block">
    					<fieldset>
    						<div class="input-group input-group-lg">
    							<?php $default_options = array('div' => false, 'label' => false, 'class' => 'form-control');
                                if (!$data['Shop']['settings']['switches']['visitors']){
                                    $options = array_merge($default_options, array('readonly' => true));
                                }
                                else $options = $default_options; ?>

                                <span class="input-group-addon">
                                    <?php echo $this->Form->input(
                                        'Shop.settings.switches.visitors',
                                        array('type' => 'checkbox', 'div' => false, 'label' => false, 'default' => 1)
                                    ); ?>
                                </span>
                                <?php echo $this->Form->input('Shop.settings.messages.visitors', $options); ?>
                                <span class="input-group-addon">
                                    <button class="btn btn-default no-submit emojiBtn" data-toggle="popover" <?= ($data['Shop']['settings']['switches']['visitors'])? '' : 'disabled'; ?>>😀</button>
                                </span>
    						</div>
    						<div class="input-group input-group-lg">
    							<?php if (!$data['Shop']['settings']['switches']['quantity']){
                                    $options = array_merge($default_options, array('readonly' => true));
                                }
                                else $options = $default_options; ?>

                                <span class="input-group-addon">
                                    <?php echo $this->Form->input(
                                        'Shop.settings.switches.quantity',
                                        array('type' => 'checkbox', 'div' => false, 'label' => false, 'default' => 1)
                                    ); ?>
                                </span>
    							<?php echo $this->Form->input('Shop.settings.messages.quantity', $options); ?>
                                <span class="input-group-addon">
                                    <button class="btn btn-default no-submit emojiBtn" data-toggle="popover" <?= ($data['Shop']['settings']['switches']['quantity'])? '' : 'disabled'; ?>>😀</button>
                                </span>
    						</div>
    					</fieldset>
    				</div>
    				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 center-block">
    					<div class="input-group input-group-lg">
                            <?php if (!$data['Shop']['settings']['switches']['hurry_up']){
                                $options = array_merge($default_options, array('readonly' => true));
                            }
                            else $options = $default_options; ?>

                            <span class="input-group-addon">
                                <?php echo $this->Form->input('Shop.settings.icon', array('type' => 'hidden')); ?>
                                <button class="icon-picker btn btn-default no-submit" data-toggle="popover">
                                    <?= $data['Shop']['settings']['icon']; ?>
                                </button>
                            </span>
                            <span class="input-group-addon">
                                <?php echo $this->Form->input(
                                    'Shop.settings.switches.hurry_up',
                                    array('type' => 'checkbox', 'div' => false, 'label' => false, 'default' => 1)
                                ); ?>
                            </span>
                            <?php echo $this->Form->input('Shop.settings.messages.hurry_up', $options); ?>
                            <span class="input-group-addon">
                                <button class="btn btn-default no-submit emojiBtn" data-toggle="popover" <?= ($data['Shop']['settings']['switches']['hurry_up'])? '' : 'disabled'; ?>>😀</button>
                            </span>
        				</div>
    				</div>
    			</div>
  			</div>
		</div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Step 2. Alternative Text For 1 Page Visitor & In Stock Item</h3>
            </div>
            <div class="panel-body">
                <div class="box notice">
                    <div class="media">
                        <div class="media-left">
                            <span class="ico-notice"></span>
                        </div>
                        <div class="media-body">
                            <p class="media-heading">Configure phrases to be shown for 1 page visitor or 1 in-stock item only.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 left-block">
                        <fieldset>
                            <div class="input-group input-group-lg">
                                <?php if (!$data['Shop']['settings']['switches']['visitors']){
                                    $options = array_merge($default_options, array('readonly' => true));
                                }
                                else $options = $default_options; ?>

                                <?php echo $this->Form->input('Shop.settings.messages.alt_visitors', $options); ?>
                                <span class="input-group-addon">
                                    <button class="btn btn-default no-submit emojiBtn" data-toggle="popover" <?= ($data['Shop']['settings']['switches']['visitors'])? '' : 'disabled'; ?>>😀</button>
                                </span>
                            </div>
                            <div class="input-group input-group-lg">
                                <?php if (!$data['Shop']['settings']['switches']['quantity']){
                                    $options = array_merge($default_options, array('readonly' => true));
                                }
                                else $options = $default_options; ?>

                                <?php echo $this->Form->input('Shop.settings.messages.alt_quantity', $options); ?>
                                <span class="input-group-addon">
                                    <button class="btn btn-default no-submit emojiBtn" data-toggle="popover" <?= ($data['Shop']['settings']['switches']['quantity'])? '' : 'disabled'; ?>>😀</button>
                                </span>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 center-block">
                        <div class="input-group input-group-lg">
                            <?php if (!$data['Shop']['settings']['switches']['hurry_up']){
                                $options = array_merge($default_options, array('readonly' => true));
                            }
                            else $options = $default_options; ?>

                            <span class="input-group-addon">
                                <?php echo $this->Form->input('Shop.settings.alt_icon', array('type' => 'hidden')); ?>
                                <button class="icon-picker btn btn-default no-submit" data-toggle="popover">
                                    <?= $data['Shop']['settings']['alt_icon']; ?>
                                </button>
                            </span>
                            <?php echo $this->Form->input('Shop.settings.messages.alt_hurry_up', $options); ?>
                            <span class="input-group-addon">
                                <button class="btn btn-default no-submit emojiBtn" data-toggle="popover" <?= ($data['Shop']['settings']['switches']['hurry_up'])? '' : 'disabled'; ?>>😀</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Step 3. Configure Conditions</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-5 col-lg-offset-1">
                        <h4>Visitors:</h4>
                        <?php
                        $minInput = '<input name="data[Shop][settings][rand_visitors][min]" class="mini-input"';
                        $maxInput = '<input name="data[Shop][settings][rand_visitors][max]" class="mini-input"';
                        if ( !$this->request->data['Shop']['settings']['fake_visitors'] ){
                            $minInput .= ' readonly';
                            $maxInput .= ' readonly';
                        }
                        if (isset($this->request->data['Shop']['settings']['rand_visitors']['min'])){
                            $minInput .= ' value="' . $this->request->data['Shop']['settings']['rand_visitors']['min'] . '"';
                        }
                        if (isset($this->request->data['Shop']['settings']['rand_visitors']['max'])){
                            $maxInput .= ' value="' . $this->request->data['Shop']['settings']['rand_visitors']['max'] . '"';
                        }
                        $minInput .= ' valid="min">';
                        $maxInput .= ' valid="max">';

                        echo $this->Form->input(
                            'Shop.settings.fake_visitors',
                            array(
                                'type' => 'radio',
                                'legend' => false,
                                'div' => false,
                                'class' => 'count-mode',
                                'options' => array(
                                    0 => 'Show real page visitors number.',
                                    1 => 'Show random visitors range from ' . $minInput . ' to ' . $maxInput . '.'
                                )
                            )
                        );
                        ?>
                    </div>
                    <div class="col-lg-5">
                        <h4>In Stock Items:</h4>
                        <?php
                        $minInput = '<input name="data[Shop][settings][rand_stock][min]" class="mini-input"';
                        $maxInput = '<input name="data[Shop][settings][rand_stock][max]" class="mini-input"';
                        if ( !$this->request->data['Shop']['settings']['fake_stock'] ){
                            $minInput .= ' readonly';
                            $maxInput .= ' readonly';
                        }
                        if (isset($this->request->data['Shop']['settings']['rand_stock']['min'])){
                            $minInput .= ' value="' . $this->request->data['Shop']['settings']['rand_stock']['min'] . '"';
                        }
                        if (isset($this->request->data['Shop']['settings']['rand_stock']['max'])){
                            $maxInput .= ' value="' . $this->request->data['Shop']['settings']['rand_stock']['max'] . '"';
                        }
                        $minInput .= ' valid="min">';
                        $maxInput .= ' valid="max">';

                        echo $this->Form->input(
                            'Shop.settings.fake_stock',
                            array(
                                'type' => 'radio',
                                'legend' => false,
                                'div' => false,
                                'class' => 'count-mode',
                                'options' => array(
                                    0 => 'Show real in-stock product items data based on your Shopify store parameters.',
                                    1 => 'Show random in-stock product items data from ' . $minInput . ' to ' . $maxInput . '.'
                                )
                            )
                        );
                        ?>
                    </div>
                </div>
                <hr>
                <p style="display: inline-block">Show this widget when a minimum number of product page visitors is</p>
                <?php
                echo $this->Form->input(
                    'Shop.settings.initial_visitors',
                    array(
                        'type' => 'text',
                        'class' => 'mini-input',
                        'div' => array('class' => 'input'),
                        'label' => false,
                        'after' => '<div class="small-descr">optional</div>'
                    )
                );
                echo $this->Form->input(
                    'Shop.settings.operator',
                    array(
                        'type' => 'select',
                        'class' => 'mini-input',
                        'div' => array('class' => 'input'),
                        'label' => false,
                        'options' => array(0 => 'and', 1 => 'or'),
                        'after' => '<div class="small-descr">optional</div>'
                    )
                );
                ?>
                <p style="display: inline-block">minimum products in stock is</p>
                <?php
                echo $this->Form->input(
                    'Shop.settings.initial_stock',
                    array(
                        'type' => 'text',
                        'class' => 'mini-input',
                        'div' => array('class' => 'input'),
                        'label' => false,
                        'after' => '<div class="small-descr">optional</div>'
                    )
                );
                ?>
                <div class="box notice">
                    <div class="media">
                        <div class="media-left">
                            <span class="ico-notice"></span>
                        </div>
                        <div class="media-body">
                            <p class="media-heading">Please note: if your product(s) is not in stock or inventory control is disabled, then only visitors information will be displayed, according to your settings.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Step 4. Choose Products Where Widget Will Be Shown</h3>
            </div>
            <div class="panel-body">
                <div class="box notice">
                    <div class="media">
                        <div class="media-left">
                            <span class="ico-notice"></span>
                        </div>
                        <div class="media-body">
                            <p class="media-heading">Select products from the list bellow where widget will be shown. In case you want to show this widget for all products, then don't choose any.</p>
                        </div>
                    </div>
                </div>
                <div id="product_selector">
                    <?php
                    echo $this->element('ProductSelector.product_block_filter',array(
                        'platform' => Configure::read('AppConf.plugins.platform'),
                        'leftBlockId' => 'shopify-products',
                        'enableCategories' => false,
                        'checkboxName' => 'check',
                        'checkboxChecked' => 1,
                        'enableTitle' => true
                    ));
                    echo $this->element('ProductSelector.product_block', array(
                        'leftBlockTitle' => 'Select target products',
                        'rightBlockTitle' => 'Target products',
                        'leftBlockId' => 'shopify-products',
                        'rightBlockId' => 'Target',
                        'categories' => true,
                        'categoryTitle' => 'Collections'
                    ));
                    echo $this->element('ProductSelector.product_block_js', array(
                        'platform' => Configure::read('AppConf.plugins.platform'),
                        'pluginFolder' => Configure::read('AppConf.plugins.appFolder'),
                        'productCount' => $productCount,
                        'moneyFormat' => $shopifyInfo['money_format'],
                        'shopName' => $shopName,
                        'categories' => true
                    ));
                    ?>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Step 5. Place A Widget On A Product Page</h3>
            </div>
            <div class="panel-body">
                <div class="box notice">
                    <div class="media">
                        <div class="media-left">
                            <span class="ico-notice"></span>
                        </div>
                        <div class="media-body">
                            <p class="media-heading">In order for the app to work you need to perform a setup by choosing a place where widget will be shown on a product page.</p>
                            <p class="media-heading">In case you've switched your theme this action should be made again.</p>
                        </div>
                    </div>
                </div>
                <div id="selectorPickerForm" class="form-inline row">
                    <?php echo $this->Form->input(
                        'Shop.settings.position',
                        array(
                            'type' => 'select',
                            'label' => false,
                            'div' => false,
                            'style' => 'display: none',
                            'class' => 'form-control',
                            'options' => array('after' => 'After element', 'before' => 'Before element')
                        )
                    );
                    ?>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">
                            <?php echo $this->element(
                                'SelectorPicker.link',
                                array(
                                    'link'  => array(
                                        'url'   => $selectorPickingURL,
                                        'text'  => 'Choose and Click',
                                        'class' => 'btn primary',
                                        'sign' => 'spurit_hub',
                                        'id'    => 'pickSelector'
                                    ),
                                    'selector'  => array(
                                        'selectPosition'=> true,
                                        'barMessage'    => 'Choose the place where widget will be shown.',
                                        'demoText'      => 'Element will be placed here',
                                        'disabledByDefault' => false
                                    ),
                                    'form'  => array(
                                        'elementSelector'   => '#ShopSettingsSelector',
                                        'elementPosition'   => '#ShopSettingsPosition'
                                    )
                                )
                            ); ?>
                        </span>

                        <?php echo $this->Form->input(
                            'Shop.settings.selector',
                            array(
                                'type' => 'text',
                                'label' => false,
                                'div' => false,
                                'class' => 'form-control'
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>In Case You've Switched Store Theme</h3>
            </div>
            <div class="panel-body">
                <div class="box notice">
                    <div class="media">
                        <div class="media-left">
                            <span class="ico-notice"></span>
                        </div>
                        <div class="media-body">
                            <p class="media-heading">If after the app installation you change a theme for your web store, our app needs to do some updates in order for it to work with your theme. To do that, you just need to click the "Update" button.</p>
                        </div>
                    </div>
                </div>
                <?php echo $this->Html->link('Update', array('action' => 'update_templates'), array('class' => 'btn primary')); ?>
            </div>
        </div>
	<?php echo $this->Form->end(array('class' => 'btn primary', 'label' => 'Save')); ?>
</div>

<script type="text/javascript">
    var PS_Containers = new Array(
        new ProductSelector(
            {
                groupFieldName: 'group-variants',
                groupVariants: true,
                ignoreVids: true,
                quantity: false,
                quantityLabel: 'Quantity: ',
                pageLimit: 10,
                productContainerId: '#shopify-products',
                discountContainerId: '#Target',
                usedProducts: [],
                usedCategories: []
            }
        )
    );


    PS_Callback_SelectorReady = function (selector) {
        selector.rightBlockLoaded = true;
        selector.ignoreVids = true;
    };

    // Selected products.
    <?php if ($data != '[]') : ?>
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: '<?php echo Router::url(array("action" => $ajaxUrl)) ?>',
        cache: false,
        data: {
            'postParams': <?php echo(isset($data) ? json_encode($data) : '[]')  ?>
        },
        error: function () {
            $('#Target').html('');
            $('#Target-loader').html('');
        },
        success: function (data) {
            $('#Target').html(data);
            $('#Target-loader').html('');
            var selector = $('#shopify-products').data('_this');
            selector.rightBlockLoaded = true;
            selector.addedIndex = $('#Target input[name*="pid"]').length;
            if (selector.leftBlockLoaded) {
                selector.setATags();
            }
        }
    });
    <?php endif; ?>

    function PS_Callback_DisplayProduct(html) {
        var li = jQuery(html), returnHtml = '';
        jQuery(li).each(function (index, value) {
            var v = jQuery(value);
            if (v.find('img.visibility').length) {
                jQuery(v.find('a.simple_buttons')).attr(
                    {
                        'class': 'simple_buttons',
                        'onclick': 'return false;'
                    }
                );

                v.attr(
                    {
                        'onclick': 'return false;',
                        'style': 'cursor: pointer; opacity: 0.5',
                        'title': 'The product has a hidden status in your store'
                    }
                )
                .addClass('bdC');
            }
            if (typeof v.get(0).outerHTML != 'undefined') {
                returnHtml += v.get(0).outerHTML;
            }
        });
        return returnHtml;
    }
</script>

<script>
    function hidePop(elem){
        elem.popover('hide');
        elem.click();
    }

    function getCaretPosition(ctrl){
        // IE < 9 Support
        if (document.selection)
        {
            ctrl.focus();
            var range = document.selection.createRange();
            var rangelen = range.text.length;
            range.moveStart('character', -ctrl.value.length);
            var start = range.text.length - rangelen;
            return {'start': start, 'end': start + rangelen };
        }
        // IE >=9 and other browsers
        else if(ctrl.prop('selectionStart') || ctrl.prop('selectionStart') == '0'){
            return {'start': ctrl.prop('selectionStart'), 'end': ctrl.prop('selectionEnd') };
        }
        else return {'start': 0, 'end': 0};
    }

    var noSubmitBtns = $('.no-submit');
    noSubmitBtns.on('click', function(){ return false; });

    var emojiBtns = $('.emojiBtn');
    //emoji popover content
	emojiBtns.popover(
        {
            placement: 'bottom',
            title: 'Choose from default icons',
            trigger: 'click',
            html: true,
            content: '<span>😀</span><span>😎</span><span>🤑</span><span>👉</span><span>💕</span><span>💣</span><span>💥</span><span>🌋</span><span>⌛</span><span>⏰</span><span>⏱</span><span>☀</span><span>🌟</span><span>🌈</span><span>⚡</span><span>❄</span><span>🔥</span><span>🎃</span><span>🎄</span><span>🎆</span><span>✨</span><span>🎉</span><span>🎁</span><span>♥</span><span>📣</span><span>🔔</span><span>🛒</span><span>⚠</span><span>➡</span><span>🔅</span><span>✔</span><span>‼</span><span>❗</span><span>🏁</span><span>👓</span><span>👔</span><span>👕</span><span>👖</span><span>👙</span><span>👚</span><span>👗</span><span>👞</span><span>👠</span><span>👒</span><span>📿</span><span>💄</span><span>💍</span><span>💎</span><span>🎂</span><span>🌙</span><span>🌠</span><span>🚲</span><br><a href="http://www.unicode.org/emoji/charts/full-emoji-list.html" target="blank">or copy emoji from Browser column</a>'
        }
    );

    $('.content').on('click', function(){
        var pop = $('.popover-content');
        if (pop.length) hidePop(pop.parent().prev());
    });

    emojiBtns.on('show.bs.popover', function(){
        hidePop($('.popover').prev());
    });

    emojiBtns.on('shown.bs.popover', function(){
        var self = $(this);
        $('.popover-content span').on('click', function(){
            var input = $(this).parent().parent().parent().prev();
            var inputVal = input.val();
            var caretPosition = getCaretPosition(input);
            input.val(inputVal.slice(0, caretPosition.start) + $(this).html() + inputVal.slice(caretPosition.end));
        });
    });

    var iconBtns = $('.icon-picker');
    //icon popover content
    iconBtns.popover(
        {
            placement: 'bottom',
            title: 'Choose from default icons',
            trigger: 'click',
            html: true,
            content: '<span>&#125;</span><span>&#10497;</span><span>&#10509;</span><span>&#10513;</span><span>&#10547;</span><span>&#8594;</span><span>&#9654;</span><span>&#10132;</span><span>&#10139;</span><span>&#10140;</span><span> &#10144;</span><span>&#10146;</span><span>&#8608;</span><span>&#8640;</span><span>&#8658;</span><span> &#8667;</span><span>&#8669;</span><span>&#8674;</span><span>&#8680;</span><span>&#8694;</span><span>&#10596;</span><span>&#10151;</span><span>&#10154;</span><span>&#10162;</span><span>&#10163;</span><span>&#10165;</span><span>&#10170;</span><span>&#10173;</span><span>&#8860;</span><span>&#61;</span><span>&#124;</span><span>&#10650;</span><span>&#12339;</span><span>&#8214;</span><span>&#8286;</span><span>&#11838;</span><span>&#10996;</span><span>&#9478;</span><span>&#10704;</span><span>&#10572;</span><span>&#65072;</span><span>&#10233;</span><span>&#10239;</span><br><a href="https://unicode-table.com" target="blank">More icons here</a><input><span class="small-descr">or place your own icon(s) here</span>'
        }
    );

    iconBtns.on('show.bs.popover', function(){
        hidePop($('.popover').prev());
    });
    iconBtns.on('shown.bs.popover', function()
    {
        var button = $(this);
        var hiddenInput = button.prev();
        var pop = $('.popover-content');
        var input = pop.children('input');
        input.val(hiddenInput.val());

        pop.children('span').on('click', function(event){
            if (event.target.className == ''){
                button.html($(this).html());
                hiddenInput.val($(this).html());
            }
        });

        pop.on('click', function(){
            event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
        });

        input.on('input', function(){
            hiddenInput.val($(this).val());
            button.html($(this).val());
        });

        input.on('keydown', function(event){
            if (event.keyCode == 13){
                hidePop(button);
                ShopifyApp.Bar.loadingOff();
                return false;
            }
        });
    });

    var countModeElements = $('.count-mode');
    countModeElements.on('change', function(){
        var inputs = $(this).parent().children('label').children('.mini-input');
        if ($(this).val() == 1) inputs.removeAttr('readonly');
        else inputs.attr('readonly', true);
    });

    var msgSwitches = $('.input-group-addon input[type="checkbox"]');
    msgSwitches.on('change', function(){
        var btn = $(this).parent().next().next().children('button');
        var input = btn.parent().prev();
        var name = input.attr('name').replace(/visitors|quantity|hurry_up/, 'alt_$&');
        var altInput = $('[name="' + name + '"]');
        var altBtn = altInput.next().children('button');
        if (input.attr('readonly')){
            btn.removeAttr('disabled');
            input.removeAttr('readonly');
            altInput.removeAttr('readonly');
            altBtn.removeAttr('disabled');
        }
        else{
            btn.attr('disabled', true);
            input.attr('readonly', true);
            altBtn.attr('disabled', true);
            altInput.attr('readonly', true);
        }
    });

    //some validation
    ShopEditForm.onsubmit = function(){
        var valid = true;
        var minInputs = $('input[valid="min"]').not('[readonly]');
        minInputs.each(function(index){
            var maxInput = $(this).parent().children('input[valid="max"]');
            var min = parseInt($(this).val());
            var max = parseInt(maxInput.val());
            if (isNaN(min) || isNaN(max) || (max < min)){
                alert('The limits for some random value in step 3 is incorrect.');
                valid = false;
            }
        });

        if (!valid){
            ShopifyApp.Bar.loadingOff();
            return false;
        }

        return true;
    };
</script>

<?php echo $this->element('submit_shopify_primary_button'); ?>