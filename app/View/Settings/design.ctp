<?php
header('Cache-Control: no-cache');
header('Pragma: no-cache');
?>
<?php $this->start('script'); ?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <?php echo $this->Html->script('jquery-ui-1.8.21.min.js'); ?>
    <?php echo $this->Html->script('jquery.uniform.js'); ?>
    <?php echo $this->Html->script('ckeditor/ckeditor', false); ?>
    <?php echo $this->Html->css("preview"); ?>
    <?php echo $this->element('StyleEditor.assets') ?>
    <?php echo $this->Html->script('customInit'); ?>
<?php $this->end('script'); ?>

<div class="content">
    <div class="page-header">
        <h1><?php echo $currentPage; ?></h1>
    </div>

    <?php echo $this->Form->create('Shop') ?>
    <div class="section">
        <div class="section-fixed">
            <div class="section-content design-controls">
                <div class="section-row">
                    <div class="section-cell">
                        <div class="section-header">
                            <h2>Container Settings</h2>
                        </div>
                        <div class="cell-container">
                            <div class="cell-column">
                                <?php
                                echo $this->Utils->bgColor('Shop.style.bgColor', array('label' => __('Background:')));
                                echo $this->Utils->borderColor('Shop.style.borderColor', array('label' => __('Border Color:')));
                                ?>
                            </div>
                            <div class="cell-column">
                                <?php
                                echo $this->Utils->borderWidth('Shop.style.borderWidth', array('label' => __('Border Width:')));
                                echo $this->Utils->borderRadius(
                                    'Shop.style.borderRadius',
                                    array('label' => __('Border Radius:'))
                                );
                                echo $this->Utils->borderStyle('Shop.style.borderStyle', array('label' => __('Border Style:')));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-row">
                    <div class="section-cell">
                        <div class="section-header">
                            <h2>Text Settings</h2>
                        </div>
                        <div class="cell-container">
                            <div class="cell-column">
                                <?php
                                echo $this->Utils->color(
                                    'Shop.style.leftTextColor',
                                    array('label' => __('Left Block Text Color:'))
                                );
                                echo $this->Utils->numberSlider(
                                    'Shop.style.leftFontSize',
                                    array('label' => __('Left Block Font Size:')),
                                    'left-block-size'
                                );
                                echo $this->Utils->fontStyle(
                                    'Shop.style.leftStyle',
                                    array('label' => __('Left Block Style:'), 'id' => 'LeftStyle')
                                );
                                ?>
                            </div>
                            <div class="cell-column">
                                <?php
                                echo $this->Utils->color(
                                    'Shop.style.rightTextColor',
                                    array('label' => __('Right Block Text Color:'))
                                );
                                echo $this->Utils->numberSlider(
                                    'Shop.style.rightFontSize',
                                    array('label' => __('Right Block Font Size:')),
                                    'right-block-size'
                                );
                                echo $this->Utils->fontStyle(
                                    'Shop.style.rightStyle',
                                    array('label' => __('Right Block Style:'), 'id' => 'RightStyle')
                                );
                                echo $this->Utils->fontFamily(
                                    'Shop.style.font',
                                    array('label' => __('Font Family:'))
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-row">
                    <div class="section-cell">
                        <div class="section-header">
                            <h2>Middle Icon Settings</h2>
                        </div>
                        <div class="cell-container">
                            <div class="cell-column">
                                <?php
                                echo $this->Utils->bgColor(
                                    'Shop.style.iconBgColor',
                                    array('label' => __('Icon Background Color:'))
                                );
                                echo $this->Utils->borderRadius(
                                    'Shop.style.iconBorderRadius',
                                    array('label' => __('Rounding Background Corners:'))
                                );
                                echo $this->Utils->numberSlider(
                                    'Shop.style.iconSize',
                                    array('label' => __('Icon Size:')),
                                    'number-slider-icon-size'
                                );
                                ?>
                            </div>
                            <div class="cell-column">
                                <?php
                                echo $this->Utils->numberSlider(
                                    'Shop.style.iconOpacity',
                                    array('label' => __('Icon Opacity:')),
                                    'number-slider-icon-opacity'
                                );
                                echo $this->Utils->color(
                                    'Shop.style.iconColor',
                                    array('label' => __('Color:'))
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-row">
                    <div class="section-cell bottom-buttons">
                        <div>
                            <?php
                            echo $this->Utils->link(
                                'Reset All',
                                array('action' => 'reset_style'),
                                array('class' => 'btn'),
                                'Are you sure?'
                            );
                            ?>
                        </div>
                        <div>
                            <input class="btn primary" type="submit" value="<?php echo __('Save') ?>" name="save"/>
                        </div>
                    </div>
                </div>
            </div>
            <?php $settings = $shop['Shop']['settings']; ?>
            <div id="preview-section">
                <label>Message preview<span id="ico-down-gray" class="icon ico-down-gray"></span></label>
                <div id="preview">
                    <div id="hub-container" class="t_BgColor t_BorderWidth t_BorderRadius t_BorderStyle t_BorderColor ">
                        <div id="hub-left" class="t_LeftTextColor t_LeftStyle">
                            <span class="t_Font">
                                <?php echo str_replace(
                                    '[quantity]',
                                    '10',
                                    str_replace('[number]', '15', $settings['messages']['visitors'])
                                ); ?>
                            </span>
                            <span class="t_Font">
                                <?php echo str_replace(
                                    '[quantity]',
                                    '10',
                                    str_replace('[number]', '15', $settings['messages']['quantity'])
                                ); ?>
                            </span>
                        </div>
                        <div id="hub-middle">
                            <div class="flex">
                                <span class="t_Font t_IconBgColor t_IconOpacity t_IconColor t_IconBorderRadius">
                                    <?= $settings['icon'] ;?>
                                </span>
                            </div>
                        </div>
                        <div id="hub-right" class="t_RightTextColor">
                            <span class="t_Font t_RightStyle">
                                <?php echo str_replace(
                                    '[quantity]',
                                    '10',
                                    str_replace('[number]', '15', $settings['messages']['hurry_up'])
                                ); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="customStyles"></div>

    <?php  echo $this->Form->end(); ?>
</div>

<script>
    $(function () {
//        var ev = new jQuery.Event('border'),
//            orig = $.fn.css;
//        $.fn.css = function() {
//            if ( this.hasClass('t_TargetBorderWidth') ) {
//                console.log('css() element: ', this);
//                this.trigger(ev);
//                console.log('css() arguments: ', arguments);
//            }
//
//            return orig.apply(this, arguments);
//        }

        var custom = {};

        var App = {
            'previewVisible': false,
            'previewSection': $('#preview-section'),
            'preview': $('#preview'),
            'icon': $('#preview-section .ico-down-gray').first(),

            togglePreview : function() {
                if ( App.previewVisible ) {
                    App.previewSection.height(App.previewSection.height() - App.preview.height());
                    $('body').css('margin-bottom', 0);
                    App.previewVisible = false;
                    App.icon.addClass('rotated-up');
                } else {
                    var currentHeight = App.previewSection.height();
                    //console.log(currentHeight);
                    App.previewSection.css('visibility', 'hidden');
                    App.previewSection.height('auto');
                    var newHeight = App.previewSection.height();
                    //console.log(newHeight);
                    App.previewSection.height(currentHeight);
                    App.previewSection.css('visibility', 'visible');
                    App.previewSection.height(newHeight);
                    $('body').css('margin-bottom', App.previewSection.height()+20);
                    App.previewVisible = true;
                    App.icon.removeClass('rotated-up');
                }
            },

            resetPreviewHeight: function() {
                if (App.previewVisible) {
                    App.previewSection.height('auto');
                    App.previewSection.height(App.previewSection.height());
                }
            }
        };

//        $('#div_wTabs').tabs();
        var setCustomStyles = function() {
            var leftHeight = custom.leftSize * 3;
            if (leftHeight < custom.iconSize) leftHeight = custom.iconSize;
            var leftOuterStyles = 'line-height: ' + (leftHeight/2) + 'px;';
            var leftInnerStyles = 'font-size: ' + custom.leftSize + 'px;';
            var middleOuterStyles = '' +
                'line-height: ' + leftHeight + 'px;' +
                'width: ' + leftHeight + 'px;' +
                'height: ' + leftHeight + 'px;'
            ;
            var middleInnerStyles = '' +
                'font-size: ' + custom.iconSize + 'px;' +
                'opacity: ' + custom.opacity + ';'
            ;
            var rightOuterStyles = '' +
                'line-height: ' + leftHeight + 'px;' +
                'padding-left: ' + leftHeight + 'px;'
            ;
            var rightInnerStyles = 'font-size: ' + custom.rightSize + 'px;';

            $('#customStyles').html(
                '<style>#hub-left{' + leftOuterStyles +
                    '}#hub-left span{' + leftInnerStyles +
                    '}#hub-middle{' + middleOuterStyles +
                    '}#hub-middle span{' + middleInnerStyles +
                    '}#hub-right{' + rightOuterStyles +
                    '}#hub-right span{' + rightInnerStyles +
                '}</style>'
            );

            var hubWidth = 0;
            var left = $('#hub-left');
            if (left.length) hubWidth += parseInt(left.css('width'));
            var right = $('#hub-right');
            if (right.length) hubWidth += parseInt(right.css('width')) + parseInt(right.css('padding-left'));
            var hubStyles = 'width: ' + hubWidth + 'px;';

            $('#customStyles').html($('#customStyles').html() + '<style>#hub-container{' + hubStyles + '}</style>');
        }

        /*** Custom events handling ***/
        SpControls.init();

        $('.font-style-button img, .font-selector .radio-selector-option').on('click', function(){
            setTimeout(setCustomStyles, 10);
        });

        $('.number-slider-icon-opacity').numberSlider({
            min: 0,
            max: 100,
            step: 1,
            units: '%',
            type: function(value){
                custom.opacity = value/100;
                setCustomStyles();
            }
        });
        $('.number-slider-icon-size').numberSlider({
            min: 20,
            max: 72,
            step: 1,
            units: 'px',
            prefix: 'ShopStyle',
            type: function(value){
                custom.iconSize = value;
                setCustomStyles();
            }
        });
        $('.left-block-size').numberSlider({
            min: 8,
            max: 24,
            step: 1,
            units: 'px',
            type: function(value){
                custom.leftSize = value;
                setCustomStyles();
            }
        });
        $('.right-block-size').numberSlider({
            min: 8,
            max: 24,
            step: 1,
            units: 'px',
            type: function(value){
                custom.rightSize = value;
                setCustomStyles();
            }
        });

        $('#preview-section').find('>label').click(function(){
            App.togglePreview();
        });

        App.togglePreview();
	});
</script>

<?php echo $this->element('submit_shopify_primary_button'); ?>