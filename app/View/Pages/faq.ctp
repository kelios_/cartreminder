<?php $this->start('script'); ?>
    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
    <?php echo $this->Html->script('jquery-ui-1.8.21.min.js'); ?>
    <?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php $this->end('script'); ?>

<div class="section">
    <div class="section-fixed">
        <div class="section-content">
            <div class="section-row">
                <div class="section-cell">
                    <div class="faq">
                        <div class="box notice"><i class="ico-notice"></i>Just <?php echo $this->Html->link('contact us','https://spur-i-t.com/contacts/', array('target' => '_blank')); ?> if you have any difficulties. We are always happy to help!</div>
                        <div class="question">
                            <h2 id="emoji"><a href="">Why can't I see the emoji icons (or why are they corrupted)?</a></h2>
                            <div class="answer">
                                <p>The final icons look depends on your device as well as your browser version. We recommend to use the latest operation system for your device so that emoji are displayed correctly. <a href="http://www.unicode.org/emoji/charts/full-emoji-list.html" target="blank">Here</a>'s the list of all available emoji.</p>
                            </div>
                        </div>
                        <div class="question">
                            <h2 id="uninstall"><a href="">What activities should be undertaken after uninstallation of the app?</a></h2>
                            <div class="answer">
                                <p>Go to <strong>Shopify Template Editor</strong>:</p>
                                <ol>
                                    <li> in the <strong>Templates</strong> section in <strong>product.liquid</strong> file
                                        delete the short 3 lines block which starts from the line: <pre>&lt;!-- sh_hub-added --&gt;</pre>
                                        and end with the line: <pre>&lt;!-- /sh_hub-added --&gt;</pre></li>
                                    <li>(optional step) in the group <strong>Snippets</strong> delete the file <strong>sh_hub-product-snippet.liquid</strong></li>
                                    <li>(optional step) in the <strong>Templates</strong> section delete the template <strong>product-without-sh_hub.liquid</strong></li>
                                </ol>
                                <p>If you need help with this, please <a href="https://spur-i-t.com/contacts/" target="_blank">contact us</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var yScroll;
    $(function(){
        $('.question  h2').click(function(){
            location.hash = 'q_' + this.id;

            $(this).parent().find('.answer').toggle('slow');
            return false;
        });

        $('a.faq_link').click(function(e){
            e.preventDefault();
            location.href = this.href;
            processHash();
        });

        processHash();        

        function processHash() {
            if ( location.hash != '' ) {
                var element = $('h2' + location.hash.replace('q_', ''));
                element.trigger('click');
                element[0].scrollIntoView( true );
            }
        }

        // Show sample screenshots in popup
        var imgScreen = $("#imgScreen");
        var imgScreenImg = $('img', imgScreen);
        $("#imgScreen").dialog({
            width:'90%',
            autoOpen: false,
            show: "fadeIn",
            modal: true,
            resizable: false,
            maxWidth: 880,
            dialogClass: 'embedded-alert faq-screen-popup'
        });
        $('.lightbox').click(function(e){
            var imgScreen = $("#imgScreen");
            var imgScreenImg = $('img', imgScreen);

            $('img', imgScreen).attr('src', this.href);
            $("#imgScreen").dialog('open');

            return false;
        });
    });
</script>