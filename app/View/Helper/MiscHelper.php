<?php
App::uses('AppHelper', 'View');

/**
 * Utils helper
 *
 * @property FormHelper $Form
 * @property TextHelper $Text
 * @property HtmlHelper $Html
 */

class MiscHelper extends AppHelper
{
    public $helpers = array('Form', 'Text', 'Html');

    public function makeEmbeddedLinks($links)
    {
        $list = array();
        foreach ($links as $link) {
            if ( isset($link['url']) ) {
                $list[] = $this->makeLink($link);
            } else if ( isset($link['sublinks']) ) {
                $sublinks = $this->makeSublinks($link['sublinks']);
                $list[] = sprintf('{ label: "%s", type: "dropdown", links: [%s] }', $link['text'], $sublinks);
            }
        }

        return implode(",\n", $list);
    }

    public function makeSublinks($sublinks) {
        $list = array();
        foreach($sublinks as $sublink) {
            $list[] = $this->makeLink($sublink);
        }

        return implode(', ', $list);
    }

    public function makeLink($link) {

        $target = !empty($link['target']) ? ', target: "'.$link['target'].'"' : '';
        return sprintf('{ label: "%s", href: "%s" %s }', $link['text'], Router::url($link['url'],  true),$target);
    }

    public function link($title, $url = null, $options = array(), $confirmMessage = false)
    {
        if (!empty($title)) {
            $title = "<div>$title</div>";
            $options['escape'] = false;
        }

        return $this->Form->Html->link($title, $url, $options, $confirmMessage);
    }

    public function email($email, $truncateLength = 22)
    {
        return '<a href="mailto: ' . $email . '" title="' . $email .'">' . $this->Text->truncate($email, $truncateLength) . '</a>';
    }

    public function product($title, $handle, $shopDomain)
    {
        return '<a href="http://' . $shopDomain . '/products/' . $handle . '" target="_blank">' . $title . '</a>';
    }

    public function order($title, $id, $shopDomain)
    {
        return '<a href="http://' . $shopDomain . '/admin/orders/' . $id . '" target="_blank">' . $title . '</a>';
    }

    public function viewLink($url)
    {
        return $this->Html->link($this->Html->image('admin/Icons/32/i_32_view.png'), $url, array('escape' => false, 'title' => 'View details'));
    }

    public function editLink($url)
    {
        return $this->Html->link($this->Html->image('admin/Icons/32/i_32_edit.png'), $url, array('escape' => false, 'title' => 'Edit'));
    }

    public function translate($phrase, $translations)
    {
        return (isset($translations[$phrase]) ? $translations[$phrase] : $phrase );
    }

    public function status($status)
    {
//        switch ($status) {
//            case 'completed':
//                $color = 'green';
//                break;
//            case 'failed':
//                $color = 'pink';
//                break;
//            default:
//                $color = 'yellow';
//        }

        return '<span class="tag '.(($status) ? 'green' : 'pink').'">' . (($status) ? 'Active' : 'Disabled') . '</span>';
    }

    public function archived($text)
    {
        echo '<span class="tag gray">' . $text . '</span>';
    }

    public function toggleStatusLink($controller, $itemId, $currentStatus)
    {
        $text = 'Disable';
        $btnColor = '';

        if ( !$currentStatus ) {
            $text = 'Enable';
            $btnColor = 'green';
        }

        return $this->Html->link($text, array('controller' => $controller, 'action' => 'toggle_status', $itemId), array('class' => 'btn ' . $btnColor));
    }

    public function deleteItem($controller, $dealId, $text = 'Delete')
    {
        return $this->Html->link($text, array('controller' => $controller, 'action' => 'delete', $dealId), array('class' => 'btn destroy lnkDeleteItem'));
    }

    public function productImageThumb($imageUrl)
    {
        return $this->productImage($imageUrl, 'thumb');
    }

    public function productImageMedium($imageUrl)
    {
        return $this->productImage($imageUrl, 'medium');
    }

    public function productImageLarge($imageUrl)
    {
        return $this->productImage($imageUrl, 'large');
    }

    public function  productImage($imageUrl, $size = 'thumb')
    {
        $imageUrl = preg_replace('/\.[^.]*$/i', '_' . $size . '$0', $imageUrl);

        return $imageUrl;
    }

}
