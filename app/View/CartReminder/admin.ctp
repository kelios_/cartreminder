<?php

$this->start('script'); ?>
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
<script src="http://yulia.spurit.loc/hurry_up_to_buy/files/upload/common.js"></script>

<?php echo $this->Html->script('jquery-ui-1.9.2.custom.min.js'); ?>
<?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php $this->end('script'); ?>

<div class="content">
    <div class="page-header">
        <h1><?php echo $currentPage; ?></h1>
    </div>
    <div class="section-content">
    <?php echo $this->Form->create('CartReminder', array('url' => 'edit')); ?>

       <?= $this->Form->hidden('id');?>
       <?= $this->Form->hidden('shop_id',array('value'=>$catrreminder->shop_id)); ?>

        <div class="form-group">   
            <label class="switch">
            <?= $this->Form->checkbox('status_plugin', array(
                'hiddenField' => true,
                'default'=>'1',
                ));?>
                <span class="slider round"></span>
            </label>
            <label>Abandoned Cart Remindert</label>
            <small class="form-text text-muted">
                If a shopper navigates away from your store or becomes inactive, they will see a blinking browser tab
                and optionally a message on the bar when they come back.
            </small>
        </div> 

        <div class="form-group">    
                <?= $this->Form->input('message_tab',
                        array('label' => 'Message written on the browser Tab after user has added something to the cart',
                              'class'=>'form-control',
                              'div' => false,
                              'default'=>'Cart: you have some items'
                            )
                        );
                ?>
            <small class="form-text text-muted">
                Blinking tab title. If a shopper navigates away from your store, they will see it. E.g. "Come Back!"
                If empty, the default tab title will be used
            </small>
        </div>

        <div class="form-group">    
                <?= $this->Form->input('message_bar',
                        array('label' => 'Message written on the Bar',
                              'class'=>'form-control',
                              'div' => false,
                              'default'=>'Your cart will expire in 10 minutes'
                            )
                        );
                ?>
            <small class="form-text text-muted">
                Bar that shows up at the page top after the shopper comes back to your store.
                E.g. "You can use coupon GRTV2F for 10% discount" or "Your cart will expire in 10 minutes".
                Use square brackets [ ] to include a link to your Cart. E.g if you type [cart], then a link title will be cart.
            </small>
        </div>

        <div class="form-group">    
                <?= $this->Form->input('time',
                        array('label' => '* Time after which the tab starts blinking and the bar appears',
                              'div' => false,
                              'default'=>'3'
                            )
                        );
                ?>
             <?= $this->Form->select('time_description',[
                $catrreminder->time_param
                            ]
                     , array('escape' => false,
                          'default'=>'1'));
                        
                ?>
        </div>

        <div class="form-group">    
            <?= $this->Form->checkbox('status_bar', array(
                'hiddenField' => true,
                'default'=>'1',
                ));?>
            <label>Display the message bar regardless of whether the user is active or not</label>
            <small class="form-text text-muted">
                You can motivate the user who has added something to the cart, and is continuing shopping
            </small>
        </div>

        <div class="form-group">    
            <?= $this->Form->radio('position_bar',$catrreminder->position_bar, 
                array('default' => '1',
                    'legend'=>'Display the bar *')
                );?>
        </div>

    <?php echo $this->Form->end(array('class' => 'btn primary', 'label' => 'Save')); ?>
    </div>
</div>