<?php echo $this->element('embedded_top_menu') ?>
<!DOCTYPE html>
<html>
<head>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $this->fetch('title'); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
        echo $this->Html->css('bootstrap/bootstrap.min.css');
        echo $this->Html->css('bootstrap/bootstrap-theme.min.css');
        echo $this->Html->css('seaff');
        echo $this->Html->css('app');
		echo $this->fetch('css');
	?>

    <script src="//cdn.shopify.com/s/assets/external/app.js"></script>
    <?php echo $this->fetch('script'); ?>
    <script type="text/javascript">
        <?php if (!empty($shop)) : ?>
        ShopifyApp.init({
            apiKey: '<?php echo $apiKey ?>',
            shopOrigin: 'https://<?php echo $shop['Shop']['domain'] ?>',
            forceRedirect: <?php echo ($allowShopifyRedirect ? 'true' : 'false') ?>
//  TODO delete?          , debug: true
        });
        ShopifyApp.ready(function(){
            ShopifyApp.Bar.initialize({
                icon: '<?php echo Router::url('/img/upsell-bundles-icon60.png', true) ?>',
                title: '<?php echo (empty($pageTitle) ? '' : $pageTitle ) ?>',
                buttons: {
                    <?php
                    $secondaryButtons = array();
                    if ( $this->fetch('primary_button') ) {
                        $secondaryButtons[] = $this->fetch('primary_button');
                    }
                    if ( $this->fetch('secondary_buttons') ) {
                        $secondaryButtons[] = $this->fetch('secondary_buttons');
                    }

                    echo implode(",\n", $secondaryButtons);
                    ?>
                }
            });
        });
        <?php endif; ?>
    </script>
</head>
<body class="<?php echo Configure::read('AppConf.server_type') ?>">

<?php
if ( !$allowShopifyRedirect ) echo $this->element('top_menu');
//echo $menu;
echo $this->Flash->render();
echo $this->fetch('content');
?>

<script type="text/javascript">
    var App = App || {};
    App.submitMainForm = function(formSelector) {
        var formSelector = formSelector || 'form';
        $(formSelector).submit();
    }
    ShopifyApp.ready(function(){
        ShopifyApp.Bar.loadingOff();
    });
</script>

<!-- Button to ideas page -->
    <style>
        div#idea {
            z-index: 2;
            box-shadow: 0 2px 11px #5f5f5e !important;
            border-radius: 8px;
            margin-top: -50px;
            position: fixed;
            right: 0;
            top: 50%;
        }
    div#idea img {display: block;}
    </style>
    <div id="idea"><a href="http://hurry-up-to-buy.idea.informer.com/" target="_blank"><img src="//shopify-apps.spur-i-t.com/informer.png" title="Suggest New Idea" /></a></div>
<!-- /Button... -->

</body>
</html>