<?php
$links = array(
	array('url' => array('controller' => 'settings', 'action' => 'edit'), 'text'=>'Home'),
	array('url' => array('controller' => 'settings', 'action' => 'design'), 'text'=>'Design'),
    array('url' => array('controller' => 'pages', 'action' => 'display', 'faq'), 'text'=>'FAQ'),
	array('text'=>'Our Other Apps', 'sublinks' => array(
        array('target'=>'_blank','url' => 'https://apps.shopify.com/pre-order?utm_source=social-proof-dropdown&utm_campaign=social-proof-dropdown&utm_medium=referral', 'text'=>'Pre-Order Manager'),
        array('target'=>'_blank','url' => 'https://apps.shopify.com/discount-manager?utm_source=social-proof-dropdown&utm_campaign=social-proof-dropdown&utm_medium=referral', 'text'=>'Bulk Discounts & Sales Scheduler'),
        array('target'=>'_blank','url' => 'https://apps.shopify.com/unlimited-upsell?utm_source=social-proof-dropdown&utm_campaign=social-proof-dropdown&utm_medium=referral', 'text'=>'Upsell For Products - Buy X Get Y'),
        array('target'=>'_blank','url' => 'https://apps.shopify.com/upsell-popup-on-exit-visit?utm_source=social-proof-dropdown&utm_campaign=social-proof-dropdown&utm_medium=referral', 'text'=>'Popup Upsell on Exit & Visit'),
        array('target'=>'_blank','url' => 'https://apps.shopify.com/sales-conversion-optimizer?utm_source=social-proof-dropdown&utm_campaign=social-proof-dropdown&utm_medium=referral', 'text'=>'Products A/B Test - Your Sales Booster'),
        array('target'=>'_blank','url' => 'https://apps.shopify.com/in-stock-reminder?utm_source=social-proof-dropdown&utm_campaign=social-proof-dropdown&utm_medium=referral', 'text'=>'Back in Stock alerts'),
        array('target'=>'_blank','url' => 'https://apps.shopify.com/upsell-bundles-products?utm_source=social-proof-dropdown&utm_campaign=social-proof-dropdown&utm_medium=referral', 'text'=>'Upsell Bundled Products')
    ))
);
?>
<?php $this->start('secondary_buttons') ?>
secondary: [
    <?php echo $this->Misc->makeEmbeddedLinks($links); ?>
]
<?php $this->end(); ?>