<script>
	var HUBParams = {
		"id" : "<?php echo $shopHash ?>",
		"product" : {{ product | json }}
	};
	HUBParams.product.collections = {{ product.collections | json }};
</script>
<script src="<?php echo $commonJsFileUrl;?>"></script>