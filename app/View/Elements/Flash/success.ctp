<script>
    ShopifyApp.ready(function(){
        ShopifyApp.flashNotice("<?php echo h($message) ?>");
    });
</script>
<div id="<?php echo h($key) ?>Message" class="flash-message success"><?php echo h($message) ?></div>
