<?php $size = intval($leftFontSize)*3;
if ($size < intval($iconSize)) $size = intval($iconSize); ?>

/* container */
#hub-container{
    background-color: <?php echo @$bgColor ?>;
    border-width: <?php echo @$borderWidth ?>;
    border-style: <?php echo @$borderStyle ?>;
    border-color: <?php echo @$borderColor ?>;
    border-radius: <?php echo @$borderRadius ?>;
}

/* left block */
#hub-left{
    color: <?php echo @$leftTextColor ?>;
    line-height: <?php echo $size/2 ?>px;
}
#hub-left span{
    font-size: <?php echo @$leftFontSize ?>;
    font-family: <?php echo @$font ?>;
    font-style: <?php echo @$leftFontStyle ?>;
    text-decoration: <?php echo @$leftStyleDecoration ?>;
    font-weight: <?php echo @$leftStyleWeight ?>;
}

/* middle icon*/
#hub-middle span{
    background-color: <?php echo @$iconBgColor ?>;
    border-radius: <?php echo @$iconBorderRadius ?>;
    font-size: <?php echo @$iconSize ?>;
    font-family: <?php echo @$font ?>;
    opacity: <?php echo @$iconOpacity / 100 ?>;
    color: <?php echo @$iconColor ?>;
}

/* right-block */
#hub-right{
    line-height: <?php echo $size / 2 ?>px;
    color: <?php echo @$rightTextColor ?>;
}
#hub-right span{
    font-family: <?php echo @$font ?>;
    font-size: <?php echo @$rightFontSize ?>;
    font-weight: <?php echo @$rightStyleWeight ?>;
    font-style: <?php echo @$rightStyleStyle ?>;
    text-decoration: <?php echo @$rightStyleDecoration ?>;
}