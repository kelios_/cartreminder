<?php
if ( !isset($title) ) {
    $title = 'Save';
}
if ( !isset($callback) ) {
    $callback = 'App.submitMainForm';
}

$selector = isset($selector) ? '"' . $selector . '"' : '' ;
$this->start('primary_button');
?>
primary: {
    label: '<?php echo $title ?>',
    callback: function(){
        ShopifyApp.Bar.loadingOn();
        <?php echo $callback ?>(<?php echo $selector ?>);
    }
}
<?php
$this->end();
?>