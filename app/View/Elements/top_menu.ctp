<?php
$links = array(
    'settings' => array(
        'icon'=>'forms',
        'text'=>'Home',
        'subheader' => 'Home',
        'info'=>'',
        'url'=>array('controller' => 'settings', 'action' => 'edit')
    ),
    'design' => array(
        'icon'=>'forms',
        'text'=>'Design',
        'subheader' => 'Design',
        'info'=>'',
        'url'=>array('controller' => 'settings', 'action' => 'design')
    ),
    'faq' => array(
        'icon'=>'forms',
        'text'=>'FAQ',
        'subheader' => 'FAQ',
        'info'=>'',
        'url'=>array('controller' => 'pages', 'action' => 'display', 'faq')
    )
);

$conditions = array(
    'settings_edit' => 'settings',
    'settings_design' => 'design',
    'pages_display' => 'faq'
);
?>

<?php
$linkKey = $this->params['controller'].'_'.$this->params['action'];
$links[$conditions[$linkKey]]['class'] = 'active';
?>

<div class="section" id="top_menu_wrapper">
    <div id="mainHeader">
        <h2><?php echo Configure::read('AppConf.app_name'); ?></h2>
    </div>

    <div class="section-fixed">
        <div class="section-content">
            <ul id="top_menu">
                <?php
                foreach($links as $key=>$link)
                {
                    echo '<li class="' . ((isset($link['class']))? $link['class'].' ' : '') . 'i_32_' . $link['icon'].'">'
                        .$this->Html->link(
                            '<span class="tab_label">'
                                .$link['text'] . '</span><span class="tab_info">' . $link['info']
                            .'</span>',
                            $link['url'],
                            array('escape' => false)
                        )
                        .'</li>';
                }
                ?>
            </ul>
        </div>
    </div>
</div>