var SpControls = {};
SpControls.init = function(){
    // Colors
    $('.color-picker').colorSliders({
        prefix: 'ShopStyle'
    });
    $('.bg-color-picker').colorSliders({
        type: 'background-color',
        prefix: 'ShopStyle'
    });
    $('.border-color-picker').colorSliders({
        type: 'border-color',
        prefix: 'ShopStyle'
    });

    // FontFamily
    $('.font-selector').radioSelector({
        type: 'font-family',
        useUniform: false,
        prefix: 'ShopStyle'
    });

    // Font Sizes
    $('.number-slider-font').numberSlider({
        min: 8,
        max: 24,
        step: 1,
        units: 'px',
        prefix: 'ShopStyle'
    });

    // Border width
    $('.number-slider-border').numberSlider({
        min: 0,
        max: 20,
        step: 1,
        units: 'px',
        type: 'border-width',
        prefix: 'ShopStyle'
    });
// height
    $('.number-slider-height').numberSlider({
        min: 35,
        max: 100,
        step: 1,
        units: 'px',
        type: 'height',
        prefix: 'ShopStyle'
    });
    // Border radius
    $('.number-slider-border-radius').numberSlider({
        min: 0,
        max: 30,
        step: 1,
        units: 'px',
        type: 'border-radius',
        prefix: 'ShopStyle'
    });

    // Border styles
    $('.border-selector').radioSelector({
        type: 'border-style',
        useUniform: false,
        prefix: 'ShopStyle'
    });

    // Font styles
    $('.font-style').fontStyles({
        prefix: 'ShopStyle'
    });

    $('.color-picker-hover').colorSliders({
        subClass: 'hover',
        prefix: 'ShopStyle'
    });

    $('.bg-color-picker-hover').colorSliders({
        type: 'background-color',
        subClass: 'hover',
        prefix: 'ShopStyle'
    });
    $('.border-color-picker-hover').colorSliders({
        type: 'border-color',
        subClass: 'hover',
        prefix: 'ShopStyle'
    });

    // Opacity
    $('.number-slider-opacity').numberSlider({
        min: 0,
        max: 100,
        step: 1,
        units: '%',
        type: 'opacity',
        prefix: 'ShopStyle'
    });
    $('.opacity-hover').numberSlider({
        min: 0,
        max: 100,
        step: 1,
        units: '%',
        type: 'opacity',
        subClass: 'hover',
        prefix: 'ShopStyle'
    });
};