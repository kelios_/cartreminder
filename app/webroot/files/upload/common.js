(function () {

    /**
     * @param {string|[string]} url  One or more urls
     * @param {function()} callback
     * @param {string} [type]
     */
    function loadStatic(url, callback, type) {
        if (typeof (callback) !== 'function') {
            callback = function () {};
        }
        if (url.constructor.name === 'Array') {
            var loadedCnt = 0;
            for (var i = 0, l = url.length; i < l; i++) {
                loadStatic(
                        url[i],
                        function () {
                            loadedCnt++;
                            if (loadedCnt === url.length) {
                                callback();
                            }
                        },
                        type
                        );
            }
        } else {
            if (type !== 'js' && type !== 'css') {
                type = url.toLowerCase().split('?')[0].split('#')[0].split('.').pop();
            }
            if (type !== 'js' && type !== 'css') {
                console.error('Undefined type of static file "' + url + '"');
                callback();
            }
            var tag;
            if (type === 'js') {
                tag = document.createElement('script');
                tag.type = 'text/javascript';
            } else {
                tag = document.createElement('link');
                tag.type = 'text/css';
                tag.rel = 'stylesheet';
            }
            if (tag.readyState) {// If the browser is Internet Explorer.
                tag.onreadystatechange = function () {
                    if (tag.readyState === 'loaded' || tag.readyState === 'complete') {
                        tag.onreadystatechange = null;
                        callback();
                    }
                };
            } else { // For any other browser.
                tag.onload = function () {
                    callback();
                };
            }
            if (type === 'js') {
                tag.src = url;
            } else {
                tag.href = url;
            }
            document.getElementsByTagName('head')[0].appendChild(tag);
        }
    }

    /*
     * Spurit.cartReminder obj
     */
    if (typeof (Spurit) === 'undefined') {
        var Spurit = {
            cartReminder: {
                config: {},
                countProduct: 0,
                interval_message: {1: 1000, 2: 1000 * 60},
                positionbar: {1: 'top', 2: 'topfixed', 3: 'bottom'},
                time: '',
                interval_tab: 500,
                shoptitle: document.title,
                static_style: {1: 'http://yulia.spurit.loc/hurry_up_to_buy/files/upload/common.css'},
            }
        };
    }

    /*
     * 
     * Init cart reminder
     */

    Spurit.init = function ($) {
        Spurit.getConfig();
    }

    /* 
     * Show Tab
     */
    Spurit.ShowTab = function () {
        var i = 0;
        var show = ['************', Spurit.cartReminder.config.message_tab];

        var focusTimer = setInterval(function () {
            document.title = show[i++ % 2];
        }, Spurit.cartReminder.interval_tab);

        document.onmousemove = function () {
            clearInterval(focusTimer);
            document.title = Spurit.cartReminder.shoptitle;
            document.onmousemove = null;
        }

    }

    /* 
     * Show Bar
     */
    Spurit.ShowBar = function () {
        var bar = document.createElement('div');
        var pos = Spurit.cartReminder.config.position_bar;
        bar.innerHTML = '<div class="spuritcartreminder_bar ' + Spurit.cartReminder.positionbar[pos] + '">' + Spurit.cartReminder.config.message_bar + '</div>';
        if (!$('.spuritcartreminder_bar').length)
            document.body.prepend(bar);
    }



    /*Get interval show bar
     * 
     * @returns {time|int}
     */

    Spurit.getTime = function () {
        var timereminder = Spurit.cartReminder.config.time_description;
        Spurit.cartReminder.time = Spurit.cartReminder.config.time * Spurit.cartReminder.interval_message[timereminder];
    }


    /* Get count in cart(api cart.js)
     * set countProduct
     */
    Spurit.getCountInCart = function () {
        Spurit.cartReminder.countProduct = 1;
        Spurit.CartReminder();
        /* $.getJSON('/cart.js', function (cart) {
         Spurit.cartReminder.countProduct = cart.item_count;
         Spurit.CartReminder();
         });*/
    }

    /* Show message on bar and tab
     * 
     */
    Spurit.CartReminder = function () {
        if (Spurit.cartReminder.countProduct >= 1) {
            $.each(Spurit.cartReminder.static_style, function (index, value) {
                loadStatic(value);
            });


            if (Spurit.cartReminder.config.status_plugin > 0) {
                if (Spurit.cartReminder.config.status_plugin) {
                    Spurit.getTime();
                    setTimeout("Spurit.ShowTab()", Spurit.cartReminder.time);
                    setTimeout("Spurit.ShowBar()", Spurit.cartReminder.time);
                }
            }
        }
    }

    /*
     * 
     * @returns {config for plugin}
     */

    Spurit.getConfig = function () {
        /*$.ajaxSetup({
         async: false
         });*/

        $.getJSON("http://yulia.spurit.loc/hurry_up_to_buy/files/upload/cartreminder.json", {async: false}, function (json) {
            Spurit.setConfig(json.CartReminder);
            Spurit.getCountInCart();
        });
    }


    /* Set config reminder
     * 
     CartReminder":{    "id":"1",
     "shop_id":"864",
     "status_plugin":"1",
     "message_tab":"test",
     "message_bar":"test",
     "time":"1111",
     "time_description":"2",
     "status_bar":"1",
     "position_bar":"2"
     }
     */

    Spurit.setConfig = function (conf) {
        Spurit.cartReminder.config = conf;
    }

    /*================================================== JQUERY CHECKING ==================================================*/

    if (typeof jQuery === 'undefined') {
        loadStatic('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', function () {
            Spurit.init(jQuery);
        });
    } else {
        Spurit.init(jQuery);
    }
})();


