<?php

App::uses('Shell', 'Console');

/**
 * Load Shopify API library
 */
App::uses('ShopifyApi', 'Lib');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class AppShell extends Shell
{
	/**
     * Set Shopify API credentials for the store.
     *
     * @param array $shop Data of the Shop model.
     *
     * @return bool
     */
    protected function setApiParams($shop)
    {
        $this->setApi(
            $shop['Shop']['domain'],
            $shop['Shop']['token']
        );

        return true;
    }

    /**
     * Setup Shopify Client
     *
     * @param string $domain
     * @param string $token
     */
    protected function setApi($domain, $token)
    {
        if ( empty($this->api) ) {
            $this->api = new ShopifyApi($domain, $token);
        } else {
            $this->api->init($domain, $token);
        }
    }
}
