<?php

/**
 * Amazon Library.
 * Uses Amazon SDK.
 *
 * @copyright Copyright (c) 2016 SpurIT <contact@spur-i-t.com>, All rights reserved
 * @link http://spur-i-t.com
 * @version 1.0.0
 */
class Amazon
{
    private static $instance = null;

    /**
     * @var string
     */
    protected $_bucket = null;

    /**
     * @var string
     */
    protected $_contentType = 'application/octet-stream';

    /**
     * Init
     */
    public function __construct() {
        $this->_bucket = Configure::read('AppConf.amazonAPI.bucket');
    }

    /**
     * setBucket
     * @param $bucket
     */
    public function setBucket($bucket) {
        $this->_bucket = $bucket;
    }

    /**
     * getBucket
     * @return null|string
     */
    public function getBucket() {
        return $this->_bucket;
    }

    /**
     * Amazon Handler
     * @return object
     */
    public static function getInstance() {
        if (!self::$instance) {
            // Instantiate the AmazonS3 class
            App::import('Vendor', 'AWSSDK', array('file' => 'AWSSDKforPHP' . DS . 'sdk.class.php'));
            self::$instance = new AmazonS3(array(
                'key' => Configure::read('AppConf.amazonAPI.key'),
                'secret' => Configure::read('AppConf.amazonAPI.secret'),
            ));
        }

        return self::$instance;
    }

    /**
     * upload file to Amazon
     *
     * @param string $fileName
     * @param string $content
     * @param null $contentType
     *
     * @return
     */
    public function upload($fileName, $content, $contentType = null, $contentEncoding = null) {
        $instance = self::getInstance();
        $params = array(
            'body' => $content,
            'acl' => AmazonS3::ACL_PUBLIC,
            'contentType' => (empty($contentType) ? $this->_contentType : $contentType ),
            'storage' => AmazonS3::STORAGE_REDUCED
        );
        if($contentEncoding){
            $params['headers'] = array(
                'Content-Encoding' => $contentEncoding
            );
        }
        $response = $instance->create_object(
            $this->getBucket(),
            $fileName,
            $params
        );

        return $response->isOK();
    }

    /**
     * Upload file to Amazon
     * @param string $destFileName
     * @param string $sourceFile
     *
     * @return boolean
     */
    public function uploadFile($destFileName, $sourceFile) {
        $fileData = getimagesize($sourceFile);

        $response = self::getInstance()->create_object(
            $this->getBucket(),
            $destFileName,
            array(
                'fileUpload'  => $sourceFile,
                'acl'         => AmazonS3::ACL_PUBLIC,
                'contentType' => $fileData['mime'],
                'storage'     => AmazonS3::STORAGE_REDUCED
            )
        );

        return $response->isOK();
    }


    /**
     * remove File from Amazon
     * @param string $fileName
     */
    public function remove($fileName) {
        $response = self::getInstance()->delete_object(
            $this->getBucket(),
            $fileName
        );

        return $response;
    }
}