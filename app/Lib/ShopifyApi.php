<?php
//App::uses('ShopifyClient', 'Lib');
App::uses('Shopify', 'Lib/external-api');

class ShopifyApi extends Shopify
{
    protected $shop_domain;
    protected $token;
    protected $last_response_headers = null;
    protected $errors;

    /**
     * Initializes api.
     * @param string $shop - shop domain name (*.myshopify.com).
     * @param string $token - token.
     * @return Shopify
     */
    public function init( $shop, $token )
    {
        parent::init($shop, $token);
        $this->token = $token;
        $this->shop_domain = $shop;

        return $this;
    }

    public function getShopInfo()
    {
        return $this->get('shop');
    }

    public function getCustomers($params = array())
    {
        return $this->get('customers', null, $params);
    }

    public function getCustomer($customerId)
    {
        return $this->get('customers', $customerId);
    }

    public function getOrder($orderId)
    {
        return $this->get('orders', $orderId);
    }

    public function getOrders($params = array())
    {
        return $this->get('orders', null, $params);
    }

    public function updateOrder($orderId, $params)
    {
        $params['id'] = $orderId;

        return $this->put('orders', $orderId, array('order' => $params));
    }

    public function getProductTitle($productId)
    {
        $params = array('fields' => 'id,title');

        return $this->getProduct($productId, $params);
    }

    public function getProductVariants($productId, $params = array())
    {
        $params = array_merge(
            array('fields' => 'id,variants'),
            $params
        );

        return $this->getProduct($productId, $params);
    }

    public function getProduct($productId, $params = array())
    {
        return $this->get('products', $productId, $params);
    }

    public function getProducts($params = array()) {
        return $this->get('products', null, $params);
    }

    public function getProductsInCollection($collectionId, $params = array()){
        return $this->get('collection_listings/product_ids', $collectionId, $params);
    }

    public function editVariant($vid, $params){
        $params['id'] = $vid;
        return $this->put('variants', $vid, array('variant' => $params));
    }

    /**
     * Not checked! - May not work with new Api lib from external-api
     * @param string $url Metafields resource (shop/metafields, product/metafields, etc)
     * @param string $namespace Metafields namespace
     *
     * @return mixed
     */
    public function getMetafields($url, $namespace)
    {
        $params = array(
            'namespace' => $namespace,
        );
        $metafields = $this->get($url, null, $params);

        return $metafields;
    }

    /**
     * Not checked! - May not work with new Api lib from external-api
     *
     * @return mixed
     */
    public function getMetafieldByKey($url, $namespace, $key) {
        $res = array();

        $params = array(
            'namespace' => $namespace,
            'key'       => $key
        );
        $metafield = $this->get($url, null, $params);

        if (count($metafield)) {
            $res = $metafield[0];
        }

        return $res;
    }

    /**
     * Not checked! - May not work with new Api lib from external-api
     *
     * @return mixed
     */
    public function getMetafieldValueByKey($url, $namespace, $key)
    {
        $res = null;
        $mf = $this->getMetafieldByKey($url, $namespace, $key);
        if (isset($mf->value)) {
            $res = $mf->value;
        }

        return $res;
    }

    public function getAsset($key)
    {
        $themeTemplate = false;

        // Get main theme
        $mainThemeId = $this->getMainThemeId();

        if (!empty($mainThemeId)) {
            $themeTemplate = $this->get(
                'themes/assets', $mainThemeId, array('asset' => array('key' => $key))
            );
        }

        return $themeTemplate->asset;
    }

    public function getImage($imageName)
    {
        return $this->getAsset('assets/'.$imageName);
    }

    public function getMainThemeId()
    {
        if (empty($this->mainThemeId)) {
            $mainTheme = $this->getMainTheme();
            $this->mainThemeId = $mainTheme->id;
        }

        return $this->mainThemeId;
    }

    public function search($resource, $params = array())
    {
        return $this->get($resource . '/search', $params);
    }

    public function count($resource, $params = array())
    {
        return $this->get($resource . '/count', null, $params);
    }

    public function saveMetafield($url, $namespace, $key, $value, $type = 'string') {
        $metafieldData = array('metafield'=>array(
            'namespace' => $namespace,
            'key'       => $key,
            'value'     => $value,
            'value_type'=> $type
        ));

        $this->post($url, null, $metafieldData);
    }

    public function updateMetafield($url, $id, $value)
    {
        $metafieldData = array('metafield'=>array(
            'id' => $id,
            'value'     => $value,
        ));

        $this->put($url, null, $metafieldData);
    }

    public function editAsset($key, $textToAdd, $backupName = null, $isLayout = false)
    {
        $insertKey = Configure::read('AppConf.metafields_ns') . '-added';

        $themeTemplate = $this->getAsset($key);

        if (strpos($themeTemplate->value, '<!-- ' . $insertKey . ' -->') === false && !empty($themeTemplate->value)) {
            $themeStr = "\n<!-- {$insertKey} -->\n{$textToAdd}\n<!-- /{$insertKey} -->\n";
            if ($isLayout) {
                $themeTemplate->value = preg_replace(
                    '/<\/body>/i',
                    $themeStr . "\n</body>",
                    $themeTemplate->value
                );
            } else {
                $themeTemplate->value = $themeStr . $themeTemplate->value;
            }

            // Backup template
            if (!empty($backupName)) {
                $this->copyAsset($key, $backupName);
            }

            // Save modified template
            $this->uploadAsset($key, $themeTemplate->value);
        }
    }

    public function uploadImage($pathToImage, $assetName)
    {
        $fileContent = base64_encode(file_get_contents($pathToImage));
        $this->uploadAsset('assets/' . $assetName, $fileContent, true);
    }

    /**
     * Upload asset file to main theme
     *
     * @param string $key Asset key
     * @param string $value Asset value
     * @param boolean $isImage Is asset image or not OPTIONAL
     *
     * @return void
     */
    public function uploadAsset($key, $value, $isImage = false)
    {
        // Get main theme
        $mainThemeId = $this->getMainThemeId();

        if (!empty($mainThemeId)) {
            // Save in themes assets
            if ($isImage) {
                $assetData = array('key' => $key, 'attachment' => $value);
            } else {
                $assetData = array('key' => $key, 'value' => $value);
            }
            $this->put(
                'themes/assets',
                $mainThemeId,
                array('asset' => $assetData)
            );
        }
    }

    /**
     * Alias to $this->uploadAsset
     *
     * @param string $key Asset key
     * @param string $value Asset value
     * @param boolean $isImage Is asset image or not OPTIONAL
     *
     * @return void
     */
    public function updateAsset($key, $value, $isImage = false)
    {
        $this->uploadAsset($key, $value, $isImage);
    }

    public function copyAsset($sourceName, $copyToName)
    {
        $mainThemeId = $this->getMainThemeId();

        if (!empty($mainThemeId)) {
            $res = $this->put(
                'themes/assets',
                $mainThemeId,
                array('asset' => array(
                    'key' => $copyToName,
                    'source_key' => $sourceName
                ))
            );
        }

        return $res;
    }

    public function removeAssetFile($imageFile)
    {
        $activeThemeId = $this->getMainThemeId();

        if (!empty($activeThemeId)) {
            $this->delete(
                'themes/' . $activeThemeId . '/assets',
                null,
                array('asset' => array(
                    'key' => 'assets/' . $imageFile
                ))
            );
        }
    }

    public function installWebhook($topic, $callbackUrl)
    {
        $this->post('webhooks', null, array('webhook' => array(
            'topic' => $topic,
            'address' => $callbackUrl,
            'format' => Configure::read('AppConf.api_format'),
        )));
    }

    public function createScriptTag($event, $scriptUrl)
    {
        $this->post('script_tags', null, array('script_tag' => array(
            'event' => $event,
            'src' => $scriptUrl,
        )));
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function createAddress($id, $fields){
        $this->post('customers/addresses', $id, $fields);
    }

    public function getWebhooks($params = array()){
        return $this->get('webhooks', null, $params);
    }

    public function deleteWebhook($id){
        $this->delete('webhooks', $id);
    }
}