<?php
	/**
	 * Provides interaction with BigCommerce API.
	 *
	 * @author Polyakov Ivan
	 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
	 * @link http://spur-i-t.com
	 * @version 1.0.1
	 */
	class BigCommerce extends AbstractApi
	{
		protected $apiName = 'Bigcommerce';

		protected $apiUrl = 'https://%s/api/v2/%s';

		protected $apiResouces = array (
			'brands', 'categories', 'customers', 'customers/addresses', 'customer_groups', 'coupons', 'countries', 'countries/states',
			'orders', 'orders/coupons', 'orders/products', 'orders/shipments', 'orders/shippingaddresses', 'options', 'options/value', 'optionsets', 'optionsets/options', 'orderstatuses',
			'products', 'products/skus', 'products/configurablefields', 'products/customfields', 'products/discountrules', 'products/images', 'products/options', 'products/rules', 'products/videos',
			'redirects', 'shipping/methods', 'store', 'time',
		);

		/**
		 * Constructor.
		 * Initializes api.
		 * @param string $shop - shop name.
		 * @param string $apiUser - api user of shop.
		 * @param string $apiKey - api key of shop.
		 * @return BigCommerce
		 */
		public function __construct( $shop, $apiUser, $apiKey )
		{
			parent::__construct();
			$this->init( $shop, $apiUser, $apiKey );
			return $this;
		}

		/**
		 * Initializes api.
		 * @param string $shop - shop name.
		 * @param string $apiUser - api user of shop.
		 * @param string $apiKey - api key of shop.
		 * @return BigCommerce
		 */
		public function init( $shop, $apiUser, $apiKey )
		{
			$this->shop = $shop;
			curl_setopt( $this->client, CURLOPT_USERPWD, $apiUser .':'. $apiKey );
			return $this;
		}

		/**
		 * @see AbstractAPI::_processResponse()
		 */
		protected function _processResponse( $response, $responseHeaders )
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
			$codeFirstNumber = substr( $code, 0, 1 );
			if ( isset ( $response->errors ) ) {
				$errors = json_encode( $response->errors );
				throw new Exception(
					$this->_message( $errors )
				);
			} else if ( ( $codeFirstNumber == 4 ) || ( $codeFirstNumber == 5 ) ) {
				throw new Exception(
					$this->_message( $response[0]->message )
				);
			}
			return $response;
		}

		/**
		 * @see AbstractAPI::_processNoResponse()
		 */
		protected function _processNoResponse()
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
			if ( $code >= 400 ) {
				throw new Exception(
					$this->_message( 'No response: ' . $code )
				);
			}
			return false;
		}

		/**
		 * @see AbstractAPI::_getMethodResponse()
		 */
		protected function _getMethodResponse( $resource, $response, $idBunch )
		{
			if ( isset ( $response->count ) ) {
				$response = $response->count;
			}
			return $response;
		}
	}
