<?php
require_once('PluginCenterException.php');
require_once('PluginCenterApi.php');

/**
 * @author Kuksanau Ihnat
 * @package Application
 * @copyright 2015 SpurIT <contact@spur-i-t.com>, All rights reserved.
 * @link http://spur-i-t.com
 * @version 2.0.0
 */
class PluginCenterRest
{
	/*
	 * mandatory config params
	 */
	private $config = array(
		'pc_api_key' => '',
		'pc_api_secret' => '',
		'pc_api_url' => '',
	);

	private $params = array();

	private $client;

	public function __construct($config, $params = array())
	{
		$this->checkConfigParams($config);
		$this->config = $config;
		$this->client = new PluginCenterApi($config['pc_api_key'], $config['pc_api_secret']);
		$this->setParams($params);
	}

	public function setParams($params)
	{
		$this->params = array_merge($this->params,$params);
		return $this;
	}

	public function setParam($param, $value)
	{
		$this->params[$param] = $value;
		return $this;
	}

	protected function checkConfigParams($config)
	{
		foreach($this->config as $key => $value){
			if(empty($config[$key]))
				throw new PluginCenterException('The config param is missed: '.$key);
		}
	}

	protected function checkParams($params, $mandatoryParams = array())
	{
		foreach ( $mandatoryParams as $param ) {
			if ( empty($params[$param]) ) {
				throw new PluginCenterException( 'The param is missed: '.$param );
			}
		}
	}

	private function connect( $path, $method='GET', $params = array(), $bg = false )
	{
	    $path = $this->camelCaseToDashed($path);
		$isBodyParams = ($method == 'POST' || $method == 'PUT') ? true : false;
		if(!empty($params['id'])){
			$path .= '/'.$params['id'];
			unset($params['id']);
		}
		$method = strtoupper($method);
		if($params){
			foreach($params as $key => $value){
				if($key == 'plugin'){
					$params[$key] = base64_encode($value);
				}
			}
		}
		if(!$isBodyParams){
			$path = $path.'?'.http_build_query($params);
		}
		$url = $this->config['pc_api_url'].'/'.$path;
		if($isBodyParams){
			$response = $this->client->connect($url, $method, $params, $bg);
		}else{
			$response = $this->client->connect($url, $method, array(), $bg);
		}
		return $response;
	}

	private function camelCaseToDashed($str)
    {
        if(strpos($str,'-')!==false){
            return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $str));
        }
        return $str;
    }

	public function get($resource, $params = array())
	{
		$checkedParams = $this->checkResourceParams('GET', $resource, $params);
		return $this->connect($resource,'GET', $checkedParams);
	}

	public function post($resource, $params = array(), $async = false)
	{
		$checkedParams = $this->checkResourceParams('POST', $resource, $params);
		return $this->connect($resource,'POST', $checkedParams, $async);
	}

	public function put($resource, $params = array(), $async = false)
	{
		$checkedParams = $this->checkResourceParams('PUT', $resource, $params);
		return $this->connect($resource,'PUT', $checkedParams, $async);
	}

	public function delete($resource, $params = array(), $async = false)
	{
		$checkedParams = $this->checkResourceParams('DELETE', $resource, $params);
		return $this->connect($resource,'DELETE', $checkedParams, $async);
	}

	private function checkResourceParams($method, $resource, $params = array())
	{
		$params = array_merge($this->params, $params);
		$methodName = 'check'.ucfirst(strtolower($method)).ucfirst($resource).'Params';
		if(method_exists( $this, $methodName)){
			call_user_func(__NAMESPACE__ ."\\".__CLASS__.'::'.$methodName, $params);
		}
		return $params;
	}

	protected function checkGetInstancesParams($params = array())
	{
		$this->checkParams($params, array('plugin_id','user_id'));
	}

	protected function checkPostWebhooksParams($params = array())
	{
		$this->checkParams($params, array('user_id','plugin_id','shop', 'topic','callback_url'));
	}

    protected function checkGetWebhooksParams($params = array())
    {
        $this->checkParams($params, array('user_id','plugin_id'));
    }

	protected function checkDeleteWebhooksParams($params = array())
	{
		$this->checkParams($params, array('user_id','plugin_id','topic'));
	}

	protected function checkPostEmailsParams($params = array())
    {
        $this->checkParams($params, array('to','from','subject','message','plugin_id'));
    }

    protected function checkGetTemplatesParams($params = array())
    {
        $this->checkParams($params, array('user_id','plugin_id'));
    }

    protected function checkDeleteInstancesParams($params = array())
    {
        $this->checkParams($params, array('user_id','plugin_id'));
    }

    protected function checkPostCronsParams($params = array())
    {
        $this->checkParams($params, array('action','hash','key'));
    }

    protected function checkPostCurrenciesParams($params = array())
    {
        $this->checkParams($params, array('to','amount'));
    }

    protected function checkPostOptionStatisticsParams($params = array())
    {
        $this->checkParams($params, array('action','user_id','plugin_id','option','value'));
    }

    protected function checkPostOptionParams($params = array())
    {
        $this->checkParams($params, array('user_id','plugin_id','option'));
    }
}
