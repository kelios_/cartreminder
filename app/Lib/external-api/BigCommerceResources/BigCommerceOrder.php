<?php

/**
 * Retrieves order resources
 *
 * Class BigCommerceOrder
 */
class BigCommerceOrder extends BigCommerceResource
{

	/**
	 * TODO: will be create after
	 * @return array
	 */
	public function shipments()
	{
		return array();
	}

	/**
	 * Gets the order products
	 * @return mixed
	 */
	public function products()
	{
		return $this->api->get( 'orders/products', $this->id );
	}

	/**
	 * Gets the order shipping addresses
	 * @return mixed
	 */
	public function shipping_addresses()
	{
		return $this->api->get( 'orders/shippingaddresses', $this->id );
	}

	/**
	 * @return array
	 */
	public function coupons()
	{
		return $this->api->get( 'orders/coupons', $this->id );
	}
}