<?php 
	class ApiException extends Exception
	{
		public function __construct( $message, $data = null, $code = 0, Exception $previous = null )
		{
$commonData = CakeSession::read( 'exception.common' );
$data = json_encode( $data );
$message = <<<MSG
$message
$commonData
data: $data

MSG;
			parent::__construct( $message, $code, $previous );
		}
	}

