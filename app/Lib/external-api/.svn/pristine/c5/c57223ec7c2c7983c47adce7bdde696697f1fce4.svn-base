<?php
    App::uses('AbstractApi', 'Lib/external-api');

	/**
	 * Provides interaction with Shopify API.
	 *
	 * @author Polyakov Ivan
	 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
	 * @link http://spur-i-t.com
	 * @version 3.0.2
	 */
	class Shopify extends AbstractApi
	{
		protected $apiName = 'Shopify';
		protected $apiLimit = 250;

		protected $apiUrl = 'https://%s/admin/%s';

		protected $apiResouces = array (
			'application_charges', 'application_charges/activate', 'articles/authors', 'articles/tags', 'blogs', 'blogs/articles',
			'checkouts', 'collects', 'comments', 'comments/spam', 'comments/not_spam', 'comments/approve', 'comments/remove', 'comments/restore',
			'countries', 'countries/provinces', 'custom_collections', 'customers', 'customers/addresses', 'draft_orders', 'draft_orders/send_invoice', 'draft_orders/complete', 'draft_orders/count', 'events', 'fulfillment_services',
			'fulfillments', 'metafields', 'orders', 'orders/close', 'orders/open', 'orders/risks', 'orders/cancel', 'orders/events', 'orders/fulfillments',
			'pages', 'products', 'products/images', 'products/variants', 'products/metafields', 'product_search_engines',
			'recurring_application_charges', 'recurring_application_charges/activate', 'redirects', 'script_tags', 'shop',
            'smart_collections', 'themes', 'themes/assets', 'transactions', 'variants', 'variants/metafields', 'webhooks'
		);

		/**
		 * Constructor.
		 * Initializes api.
		 * @param string $shop - shop name.
		 * @param string $token - token.
		 * @return Shopify
		 */
		public function __construct( $shop, $token )
		{
			parent::__construct();
			$this->init( $shop, $token );
			return $this;
		}

		/**
		 * Initializes api.
		 * @param string $shop - shop name.
		 * @param string $token - token.
		 * @return Shopify
		 */
		public function init( $shop, $token )
		{
			$this->shop = $shop;
			$this->headers = array (
				"X-Shopify-Access-Token: {$token}"
			);
			return $this;
		}

        public function getAll( $resource, $idBunch = null, $params = array(), $format = 'json' )
        {
//CakeLog::write('params', print_r($params, true));
            $allItems = array();
            $params['limit'] = $this->apiLimit;
            $ids = array();
            if ( isset($params['ids']) && is_array($params['ids']) ) {
                if ( count($params['ids']) > $this->apiLimit  ) {
                    $ids = array_chunk($params['ids'], $this->apiLimit);
                    unset($params['ids']);
                } else {
                    $params['ids'] = implode(',', $params['ids']);
                }
            }
            $page = 1;
            do {
//                $params['page'] = $page;
                if (!empty($ids)) {
                    $params['ids'] = implode(',', $ids[$page-1]);
                }
                $part = $this->get($resource, $idBunch, $params, $format);
// CakeLog::write('parts', print_r($part, true));
                if (is_array($part) && !empty($part)) {
                    $allItems = array_merge($allItems, $part);
                }
                $page++;
            } while(!empty($ids[$page-1]));

            return $allItems;
        }

		/**
		 * @see AbstractAPI::_processResponse()
		 */
		protected function _processResponse( $response, $responseHeaders )
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
			if ( $code == 429 ) {
                CakeLog::write('sh_api', 'Shop domain: ' . $this->shop . '. Error: (429)Request limit reached, sleep 2s...');
				sleep( 2 );

				$response = $this->_processResponse(
                    $this->_decodeResponse(curl_exec($this->client)),
                    array()
                );
			}
			if ( isset ( $response->errors ) ) {
				$errors = json_encode( $response->errors );
				throw new Exception(
					$this->_message( '(' . $code . ') - ' . $errors ),
                    $code
				);
			}
			return $response;
		}

		/**
		 * @see AbstractAPI::_processNoResponse()
		 */
		protected function _processNoResponse()
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
			if ( $code != 200 ) {
				throw new Exception(
					$this->_message( 'No response: ' . $code )
				);
			}
			return true;
		}

		/**
		 * @see AbstractAPI::_getMethodResponse()
		 */
		protected function _getMethodResponse( $resource, $response, $idBunch )
		{
			if ( isset ( $response->count ) ) {
				$response = $response->count;
			}
			$bunch = array (
				'products' => 'product',
				'variants' => 'variant'
			);
			if ( in_array( $resource, array_keys( $bunch ) ) && $idBunch ) {
				$resource = $bunch[ $resource ];
			}
			if ( isset ( $response->$resource ) ) {
				$response = $response->$resource;
			}
			return $response;
		}

		/**
		 * Returns api key.
		 * @return string
		 */
		public function key() {
			return $this->apiKey;
		}

		/**
		 * Returns api secret.
		 * @return string
		 */
		public function secret() {
			return $this->sharedSecret;
		}

		/**
		 * Returns main theme name.
		 * @return string|null
		 */
		public function getMainTheme()
		{
			if ( $themes = $this->get( 'themes' ) ) {
				foreach ( $themes as $_theme ) {
					if ( $_theme->role == 'main' ) {
						return $_theme;
					}
				}
			}
			return null;
		}

        /**
         * Get store information
         * @return string|null
         */
        public function getShop()
        {
            return $this->get('shop');
        }
	}
