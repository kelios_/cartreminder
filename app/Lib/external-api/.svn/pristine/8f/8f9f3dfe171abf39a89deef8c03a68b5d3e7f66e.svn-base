<?php
	/**
	 * Provides interaction with OpenCart API.
	 *
	 * @author Kovalev Yury
	 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
	 * @link http://spur-i-t.com
	 * @version 1.0.1
	 */
	class OpenCart extends AbstractApi
	{
		protected $apiName = 'OpenCart';

		protected $apiUrl = 'http://%s/index.php?route=dapi/resources/%s';

		protected $apiResouces = array (
			'stores', 'categories', 'products', 'orders', 'count', 'orderstatuses'
		);

		/**
		 * @param string $shop - shop name.
		 * @param string $apiKey - api key of shop.
		 * @return OpenCart
		 */
		public function __construct( $shop, $apiKey )
		{
			parent::__construct();
			$this->init( $shop, $apiKey );
			return $this;
		}

		/**
		 * Initializes api.
		 * @param string $shop - shop name.
		 * @param string $apiKey - api key of shop.
		 * @return OpenCart
		 */
		public function init( $shop, $apiKey )
		{
			$this->shop = $shop;
			$this->headers = array (
				"Dapi-Token: {$apiKey}"
			);
			return $this;
		}

		/**
		 * Overload parent method
		 * Sends HTTP request to API server and returns response.
		 * @param string $method
		 * @param string $url
		 * @param string $format
		 * @param null $fields
		 * @return object|void
		 */
		public function request( $method, $url, $format, $fields = null )
		{
			// Remove json extension from request string
			$url = str_replace( '.json', '', $url );

			return parent::request( $method, $url, $format, $fields );
		}

		/**
		 * Overload parent method
		 * Sets request params
		 * @param $params
		 * @return string
		 */
		protected function _setParams( $params )
		{
			curl_setopt( $this->client, CURLOPT_POSTFIELDS, http_build_query( $params ) );
			return '';
		}

		/**
		 * @see AbstractAPI::_processResponse()
		 */
		protected function _processResponse( $response, $responseHeaders )
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
			$codeFirstNumber = substr( $code, 0, 1 );
			if ( isset ( $response->errors ) ) {
				$errors = json_encode( $response->errors );
				throw new Exception(
					$this->_message( $errors )
				);
			} else if ( ( $codeFirstNumber == 4 ) || ( $codeFirstNumber == 5 ) ) {
				throw new Exception(
					$this->_message( $response[0]->message )
				);
			} elseif ( $response->status == 'failure' ) {
				throw new Exception(
					$response->message
				);
			}
			return $response->data;
		}

		/**
		 * @see AbstractAPI::_processNoResponse()
		 */
		protected function _processNoResponse()
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
			if ( $code != 204 ) {
				throw new Exception(
					$this->_message( 'No response: ' . $code )
				);
			}
			return true;
		}

		/**
		 * @see AbstractAPI::_getMethodResponse()
		 */
		protected function _getMethodResponse( $resource, $response, $idBunch )
		{
			if ( isset ( $response->count ) ) {
				$response = $response->count;
			}
			return $response;
		}
	}
