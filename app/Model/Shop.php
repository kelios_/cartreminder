<?php
/**
 * Shop model
 *
 * @author SpurIT <contact@spur-i-t.com>
 */

App::uses('AppModel', 'Model');

/**
 * Class Shop
 *
 */
class Shop extends AppModel
{
    /**
     * @var int
     */
    private $cycle = 3;

    /**
     * @var null
     */
    private $lastActiveCharge = null;

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['settings'])){
            $this->data[$this->alias]['settings'] = json_encode($this->data[$this->alias]['settings']);
        }

        if (isset($this->data[$this->alias]['style'])){
            $this->data[$this->alias]['style'] = json_encode($this->data[$this->alias]['style']);
        }

        return true;
    }

    public function afterFind($results, $primary = false)
    {
        foreach($results as &$result){
            if (isset($result[$this->alias]['settings'])){
                $result[$this->alias]['settings'] = json_decode($result[$this->alias]['settings'], true);
            }

            if (isset($result[$this->alias]['style'])){
                $result[$this->alias]['style'] = json_decode($result[$this->alias]['style'], true);
            }
        }

        return $results;
    }

    /**
     * Set the shop as active
     *
     * @return string
     * @see    ____func_see____
     * @since  1.0.0
     */
    public function activate()
    {
        if (!empty($this->id)) {
            $this->saveField('is_active', 1);
        }
    }

    /**
     * Create hash for the store.
     * Hash generated with the shop's domain and Security.salt(set in Config/core.php)
     *
     * @param array $shop
     *
     * @return string
     */
    public function getHash($shop)
    {
        return md5($shop['Shop']['domain'] . Configure::read('Security.salt'));
    }

    /**
     * Find store by hash
     *
     * @param string $hash
     *
     * @return array
     */
    public function findByHash($hash)
    {
        $shop = $this->find('first', array('conditions' => array(
            'md5(CONCAT(Shop.domain, "'.Configure::read('Security.salt').'"))' => $hash
        )));

        return $shop;
    }

    public function getHashedName($fileName, $shop)
    {
        $parts = explode('.', $fileName);
        $ext = array_pop($parts);

        return $this->getHash($shop) . '.' . $ext;
    }

    /**
     * Make name of the file stored on Amazon S3. File name based on store's hash.
     *
     * @param $hashedName
     *
     * @return string
     */
    public function getStorageName($hashedName)
    {
        return Configure::read('AppConf.amazonAPI.folderStoreImg') . $hashedName;
    }

    public function encrypt($password)
    {
        return mcrypt_encrypt( MCRYPT_DES,
            Configure::read('AppConf.crypt_key'),
            $password, MCRYPT_MODE_ECB
        );
    }

    public function decrypt($hash = '')
    {
        if($hash){
            return str_replace("\0",'',mcrypt_decrypt( MCRYPT_DES, Configure::read('AppConf.crypt_key'),$hash,MCRYPT_MODE_ECB));
        }else{
            return '';
        }
    }

    public function resetStyles($shopId)
    {
        $default = array('Shop' => array('style' => Configure::read('AppConf.default_styles')));
        if ( !$default ) return false;
        $default['Shop']['id'] = $shopId;

        return $this->save($default);
    }
}