<?php
class Target extends AppModel
{
	public function saveTargets($data, $shopId)
	{
        $pids = array();
		$toSave = $toDelete = array('collections' => array(), 'products' => array());
		$this->setSource('target_products');
        $currentProducts = $this->find('list', array('conditions' => array('shop_id' => $shopId), 'fields' => array('pid', 'id')));
        $this->setSource('target_collections');
        $currentCollections = $this->find('list', array('conditions' => array('shop_id' => $shopId), 'fields' => array('cid', 'id')));

       	foreach($data as $target){
            if (isset($target['cid'])){
            	if (isset($currentCollections[$target['cid']])){
            		$toSave['collections'][] = array('id' => $currentCollections[$target['cid']], 'cid' => $target['cid']);
            		unset($currentCollections[$target['cid']]);
            	}
            	else $toSave['collections'][] = array('shop_id' => $shopId, 'cid' => $target['cid']);
            } 
            else{
                if ( !in_array($target['pid'], $pids) ){
                    $targetData = array('shop_id' => $shopId, 'pid' => $target['pid']);
                    if (isset($currentProducts[$target['pid']])){
                        $targetData['id'] = $currentProducts[$target['pid']];
                        unset($currentProducts[$target['pid']]);
                    }
                    $toSave['products'][] = $targetData;
                    $pids[] = $target['pid'];
                }
            }
        }

        $this->deleteAll( array('id' => array_values($currentCollections)) );
        $this->saveMany($toSave['collections']);
        $this->setSource('target_products');
        $this->deleteAll( array('id' => array_values($currentProducts)) );
        $this->saveMany($toSave['products']);

        return true;    
	}

    public function getTargets($shopId)
    {
        $targets = array();
        foreach(array('target_products', 'target_collections') as $source){
            $this->setSource($source);
            $rows = $this->find( 'all', array('conditions' => array('shop_id' => $shopId)) );
            if ( !empty($rows) ){
                foreach($rows as $row) $targets[] = $row['Target'];
            }
        }
        
        return $targets;
    }
}