<?php
/**
 * Controller to run actions for ProductSelector plugin
 *
 * @author Kuksanau Ihnat, SpurIT <contact@spur-i-t.com>
 * @copyright Copyright (c) 2013 SpurIT <contact@spur-i-t.com>, All rights reserved
 * @link http://spur-i-t.com
 * @package Application
 * @version 1.0.0
 */
App::uses( 'AppController', 'Controller' );

class AppaController extends AppController
{

	public $components = array( 'ProductSelector.Product' );

	public function beforeFilter()
	{

		parent::beforeFilter();

		$shop = $this->getShopInDb();
		// Plugins.
		$this->Product->setConfig( array(
			'shopName' => $shop[ 'Shop' ][ 'domain' ]
		) );
	}

	public function beforeRender()
	{
	}

}
