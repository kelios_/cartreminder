<?php
class FixController extends AppController{

	public function cleanWebhooks(){
        $topics = array('products/delete', 'collections/delete');
        $shops = $this->Shop->find('all');
        var_dump(count($shops));

        foreach($shops as $shop){
            print_r('<br>Serving: ' . $shop['Shop']['domain']);
            $this->setApi($shop['Shop']['domain'], $shop['Shop']['token']);

            $webhooks = array();
            try {
                $webhooks = $this->api->getWebhooks();
                print_r('<br>Webhooks for this shop: <br>');
                var_dump($webhooks);
            }
            catch(Exception $e){
                print_r('<br>Error: ' . $e->getMessage());
                if (!empty($shop)) print_r('<br>In Shop: ' . $shop['Shop']['domain']);
            }

            if (!empty($webhooks)){
                foreach($webhooks as $webhook){
                    print_r('<br>Topic: ' . $webhook->topic);
                    if (in_array($webhook->topic, $topics)/* && strpos($webhook->address, 'social-proof') !== false*/){
                        try{
                            $this->api->deleteWebhook($webhook->id);
                            print_r('<br>DELETED<br>');
                        }
                        catch(Exception $e){
                            print_r('<br>Error: ' . $e->getMessage());
                        }
                    }
                }
                print_r('<br>------------------------------<br>');
            }
            else print_r('<br>Empty<br>');
        }
    }

	public function showWebhooksForThisShop()
    {
        try {
            $webhooks = $this->api->getWebhooks();
            foreach ($webhooks->webhooks as $webhook) pr($webhook->topic);
        } catch (Exception $e) {
            pr('Error: ' . $e->getMessage());
        }
    }

}