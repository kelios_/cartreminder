<?php

App::uses('AppController', 'Controller');

/**
 * Install our asset, Webhooks and make other required actions.
 *
 * @author user
 */
abstract class BaseSetupController extends AppController
{
    public $name = 'Setup';
    public $uses = array();
    public $autoLayout = false;
    public $autoRender = false;

    /*
     * Create default shop and save default data
     */
    public function index()
    {
        $this->addUserShop();
        $shop = $this->getShopInDb(true);
        if (!empty($shop)) $this->installAndActivate($shop);
        $this->redirect($this->getStartPage());
    }

    /**
     * Gets homepage of the app.
     *
     * @return array Returns array used in Controller::redirect() method.
     */
    protected function getStartPage(){
        return array('controller' => 'settings', 'action' => 'edit');
    }

    /**
     * Do installation of all data and store activation.
     * @param shop
     */
    protected function installAndActivate($shop){
        // Activate shop
        $this->Shop->id = $shop['Shop']['id'];
        $this->Shop->activate();
    }

    /**
     * Save store data in App's DB.
     * Store saved as inactive. It will be activated after setup all data.
     *
     *
     * @return bool
     */
    protected function addUserShop()
    {
        $user = $this->Auth->user();
        $exists = $this->checkExistence('Shop', array('Shop.user_id' => $user['id']));
        if ($exists) return true;
        //Save shop in our DB 
        $shop['user_id'] = $user['id'];
        $shop['is_active'] = 0;
        
        return $this->Shop->save(array('Shop' => $shop));
    }

    /**
     * Checks if database item already created.
     *
     * @param $model
     * @param $conditions
     *
     * @return mixed
     */
    protected function checkExistence($model, $conditions){
        $res = $this->$model->find('count', array('conditions' => $conditions));
        return $res;
    }
}