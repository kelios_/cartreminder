<?php
App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class SpuritAuthenticate extends BaseAuthenticate {

    public function authenticate(CakeRequest $request, CakeResponse $response)
    {
        $res = false;

        if (Configure::read('AppConf.auth_type') === CONFIG_AUTH_TYPE_TEST) {
            return $this->_findUser();
        }

        if (
            !empty($_COOKIE[$this->settings['cookieNames']['user']])
            && !empty($_COOKIE[$this->settings['cookieNames']['installed']])
            && ($_COOKIE[$this->settings['cookieNames']['user']] == $_COOKIE[$this->settings['cookieNames']['installed']])
        )
        {
            $res = $this->_findUser();
        } else {
            CakeSession::delete('Auth.User');
            CakeSession::delete('Auth.redirect');
            CakeSession::renew();
        }

        return $res;
    }

    /**
    * Find a user record using the standard options.
    *
    * @param Mixed $conditions The username/identifier, or an array of find conditions.
    * @param Mixed $password The password, only use if passing as $conditions = username.
    *
    * @return Mixed Either false on failure, or an array of user data.
    */
    protected function _findUser($conditions = null, $password = null)
    {
        if (Configure::read('AppConf.auth_type') === CONFIG_AUTH_TYPE_TEST) {
            return array('id' => 166);
        }

        $decodeKey = $this->settings['key'];
        $userId = trim( mcrypt_decrypt( MCRYPT_DES, $decodeKey, $_COOKIE[$this->settings['cookieNames']['user']], MCRYPT_MODE_ECB ) );
        
        return array('id'=>$userId);
    }
}