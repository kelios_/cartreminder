<?php 

App::uses('Component', 'Controller');

/**
 * ThemeAsset Component
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Controller.Component
 * @since         CakePHP(tm) v 1.2.0.4213
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
class AssetsComponent extends Component {
    
    /**
     * Used models
     * @var array
     */
    public $uses = array('Style');
    
    /**
     * Pattern to find parsable params in CSS template
     * @var string
     */
    public $mask = '/.*(%(.*)%).*/i';

    /**
     * Name of the CSS template
     * @var string
     */
    public $baseCssName = 'style.css';
    
    /**
     * Errors
     * @var array 
     */
    protected $__errors = array();
    
    /**
     *
     * @var ShopifyComponent Link to ShopifyComponent
     * @property ShopifyComponent $_shopify
     */
    protected $_shopify;
    
    protected $configStyles;
    protected $configModify;
    
    /**
     * The initialize method is called before the controller’s beforeFilter method.
     * 
     * @param Controller $controller
     *
     * @return void
     */
    public function initialize(Controller $controller)
    {
        //        $this->_shopify = $this->_Collection->getController()->Shopify;
        $this->_shopify = $controller->api;

        if (!empty($controller->noAssets)) {
            return true;
        }
		//load required for component models
		if ($this->uses !== false) {
			foreach($this->uses as $modelClass) {
				$controller->loadModel($modelClass);
				$this->$modelClass = $controller->$modelClass;
			}
		}
        
        Configure::load('themes' . DS . 'styles');
	}
    
    /**
     * The startup method is called after the controller’s beforeFilter method 
     * but before the controller executes the current action handler.
     * 
     * @param Controller $controller
     */
    public function startup(Controller $controller)
    {
        
    }
    
    public function parseCssTemplate($themeName, $pageType = '', $data = null)
    {
        $config = Configure::read('Styles.default');
        if (isset($config[$themeName])) {
            $usedThemeConfig = $themeName;
        } else {
            $usedThemeConfig = 'default';
        }
        $default = $config[$usedThemeConfig];
        
        $templateFile = $themeName . DS . $this->baseCssName;
        
        $fileName = APP . 'View'
            . DS . 'Elements'
            . DS . 'LiquidTemplates'
            . DS . $templateFile . '.ctp';

        if (file_exists($fileName)) {
            $cssTemplate = 'LiquidTemplates' . DS . $templateFile;
        } else {
            $cssTemplate = 'LiquidTemplates' . DS .'default' . DS . $this->baseCssName;
        }
        
        $cssFile = $this->renderTemplate($cssTemplate, array('pageType' => $pageType));
        
        $pxItems = Configure::read('Styles.px_params'); //$this->Style->getPxFields();
        
        $matches = array();
        preg_match_all($this->mask, $cssFile, $matches);
        
        $masks = array();
        $values = array();
        foreach($matches[2] as $index => $var) {
            list($cont,$el,$attr) = explode('_', $var);
            
            if (!empty($data[$cont][$el][$attr])) {
            //if param exists in data then use it's value
                $masks[] = '/'.$matches[1][$index].'/i';
                $values[] = $data[$cont][$el][$attr] . ((in_array($var, $pxItems)) ? 'px' : '');
            } elseif ( ($pageType == '') && (!empty($default[$cont][$el][$attr])) ) {
            //if param exists in theme's style config then use it's value
                $masks[] = '/'.$matches[1][$index].'/i';
                $values[] = $default[$cont][$el][$attr];
            } elseif ( ($pageType == '') && ($usedThemeConfig != 'default') && (!empty($config['default'][$cont][$el][$attr])) ) {
            //if param exists in default style config then use it's value
                $masks[] = '/'.$matches[1][$index].'/i';
                $values[] = $config['default'][$cont][$el][$attr];
            } else {
            //param's value unknown - remove it from css template
                $masks[] = '/'.$matches[0][$index].'/i';
                $values[] = '';//"\n";
            }
        }
        $cssCompiled = preg_replace($masks, $values, $cssFile, -1);
        
        return $cssCompiled;
    }
    
    /**
     * Upload dynamic CSS files to shop's main theme.
     * CSS content is generated from base template.
     * Files will be generated for every template provided in seond param.
     * Name of the generated CSS file will be "{global app prefix}-{template}-style.css".
     * If second param is not provided only one file "{global app prefix}-style.css" will be generated and uploaded.
     * 
     * @param string $themeName Shop's main theme in lowe case
     * @param array  $pageTypes Theme templates css files should be generated for. | OPTIONAL
     */
    public function uploadAllCss($themeName, $pageTypes = null)
    {
        if (empty($pageTypes)) $pageTypes = array('');
        
        foreach($pageTypes as $pType => $pName) {
            $cssDefault = $this->parseCssTemplate($themeName, $pType);
            $this->uploadCss($pType, $cssDefault);
        }
    }
    
    /**
     * Generate name and upload CSS file
     * 
     * @param string $pageType      Name of the liquid template - can be = '' or null
     * @param type $css             File content
     * 
     * @return boolean
     */
    public function uploadCss($pageType, $css)
    {
        return $this->_Collection->getController()->api->uploadAsset(
            'assets/' 
                . $this->getAssetsPrefix() 
                . ((empty($pageType)) ? '' : $pageType . '-' )
                . $this->baseCssName . '.liquid',
            $css
        );
    }
    
    public function uploadAssets($assets = array(), $themeName = 'default', $isUpdate = false)
    {
        foreach($assets as $asset) {
            $prefix = empty($asset['noPrefix']) ? $this->getAssetsPrefix() : '';
            $fullAssetName = $asset['assetDir'] . '/' . $prefix . $asset['template'] . '.liquid';
            
            if ($isUpdate && isset($asset['update']) && empty($asset['update'])) {
                //continue;
                try {
                    $existentAsset = $this->_Collection->getController()->api->getAsset($fullAssetName);
                    if (!empty($existentAsset)) {
                        continue;
                    }
                } catch (Exception $e) {
                    $code = $e->getCode();
                    if ($code == 404) {
                        //if asset file not exists then upload it regardless the value of $isUpdate
                        
                    } else {
                        // do not upload - keep the file
                        continue;
                    }
                }
            }
            
            $fileName = APP . 'View'
                . DS . 'Elements'
                . DS . 'LiquidTemplates'
                . DS . $themeName
                . DS . $asset['template'] . '.ctp';
            
            if (file_exists($fileName)) {
                $template = 'LiquidTemplates' . DS . $themeName . DS . $asset['template'];
            } else {
                $template = 'LiquidTemplates' . DS .'default' . DS . $asset['template'];
            }
            
            // Save liquid template
            $this->uploadAsset(
                $template, 
                $fullAssetName, 
                (isset($asset['data']) ? $asset['data'] : array())
            );
        }
    }
    
    /**
     * Render and upload to shopify asset, snippet, template, etc
     * 
     * @param string $sourceTemplate Name of the .ctp template
     * @param string $assetName      Name of the asset file in Shopify theme
     * @param array $data            Data which will be available in .ctp template during rendering.
     */
    protected function uploadAsset($sourceTemplate, $assetName, array $data=array())
    {
        $assetContent = $this->renderAssetFromTemplate($sourceTemplate, $data);

        // Save liquid template
        $this->_Collection->getController()->api->uploadAsset($assetName, $assetContent);
    }
    
    /**
     * Upload snippets to Shopify
     * 
     * @param type $themeName Name of the store's main theme in lower case
     */
    public function uploadSnippets($themeName, $snippets = array())
    {
        foreach($snippets as $templateName) {
            $fileName = APP . 'views'
                . DS . 'elements'
                . DS . 'LiquidTemplates'
                . DS . $themeName
                . DS . $templateName . '.ctp';

            if (file_exists($fileName)) {
                $template = 'LiquidTemplates' . DS . $themeName . DS . $templateName;
            } else {
                $template = 'LiquidTemplates' . DS .'default' . DS . $templateName;
            }
            
            $snippetContent = $this->renderAssetFromTemplate($template);
            
            // Save liquid template
            $assetName = 'snippets/' . $this->getAssetsPrefix() . $templateName . '.liquid';
            $this->_Collection->getController()->api->uploadAsset($assetName, $snippetContent);
        }
    }
    
    /**
     * Modify source code of the liquid templates replacing some content.
     * Replacing rules can be set in config file - Config/themes/setup.php.
     * 
     * @param string $themeName Name of the current main theme
     * @return boolean
     */
    public function modifyTemplates($themeName)
    {
        Configure::load('themes' . DS . 'setup');
        $config = Configure::read('Modify');
        
        if (isset($config[$themeName])) {
            $templates = $config[$themeName];
        } else {
            $templates = (isset($config['default']) ? $config['default'] : false );
        }
        if(empty($templates)) {
            return;
        }
        
        foreach ($templates as $template => $params) {
            if (strpos($template, '/') == false) {
                $template = 'templates/' . $template;
            }
            $key = $template . '.liquid';
            try {
                $templateCode = $this->_shopify->getAsset($key);
            } catch (Exception $e) {
                try{
                    $templateCode = $this->_shopify->getAsset($key);
                } catch (Exception $e) {
                    echo 'Error: ' .$e->getMessage();
                    return false;
                }
            }
            if (!empty($params['data']) && strpos($templateCode['value'], $params['data']) === false) {
                $newCode = $this->replaceCode($templateCode['value'], $params);
                $this->_shopify->updateAsset($key, $newCode);
            } else {
            }
        }
        
        return true;
    }
    
    /**
     * Replace text defined in params with provided data.
     * 
     * @param string $source Subject
     * @param type $params Array with params for replacement
     * @return string Modified template
     * @throws Exception
     */
    public function replaceCode($source, $params)
    {
        if (!isset($params['replacement'])) {
            switch ($params['insert']) {
                case 'before':
                    $replaceTo = $params['data'] . "\n" . '$0';
                    break;
                case 'replace':
                    $replaceTo = $params['data'];
                    break;
                case 'after':
                default:
                    $replaceTo = '$0' . "\n" . $params['data'];
            }
        } else {
            $replaceTo = $params['replacement'];
        }
        
        $res = preg_replace($params['pattern'], $replaceTo, $source);
        
        if ($res === null) {
            throw new Exception('Can not process source template with preg_replace!');
        }
        
        return $res;
    }
    
    /**
     * Prefix to generate unique names for uploading assets
     * 
     * @return string
     */
    public function getAssetsPrefix()
    {
        return Configure::read('AppConf.metafields_ns') . '-';
    }
    
//    public function getDefaultCssSettings()
//    {
//        if (empty($this->configStyles)) {
//            $this->configStyles = Configure::read('Styles.default');
//        }
//
//        return $this->configStyles;
//    }
    
//    public function getModifySettings()
//    {
//        if (empty($this->configModify)) {
//            Configure::load('themes' . DS . 'setup');
//            $this->configModify = Configure::read('Modify');
//        }
//
//        return $this->configStyles;
//    }
    
    protected function renderTemplate($template, $data = array())
    {
        $view = new View($this->_Collection->getController());
        $view->set($data);
        $view->viewPath = 'Elements';

        // Grab output into variable without the view actually outputting!
        return $view->render($template, false);
    }
    
    public function renderAssetFromTemplate($template, $data = array())
    {
        $data['namespaceMf'] = Configure::read('AppConf.metafields_ns');
        
        return $this->renderTemplate($template, $data);
    }
    
}