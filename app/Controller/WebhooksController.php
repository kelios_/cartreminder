<?php

/**
 * Description of WebhooksController
 *
 * @author user
 */
App::uses('AppController', 'Controller');
class WebhooksController extends AppController
{
    public $name = 'Webhooks';
    public $uses = array('Target');
    public $requestHeaders;
    public $requestBody;
    const HTTP_STATUS_ERROR = 400;
    const HTTP_STATUS_OK = 200;
    
    /**
     * Verify Wewbhook and prepare data
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->addToLog(
            "------------------------------------------------------------------------\n"
            . 'Webhook received. Domain: "' . $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'] . '". '
            . "\n" . 'Event from Headers: "'.$_SERVER['HTTP_X_SHOPIFY_TOPIC'].'".'
        );
        /*
         * Read request body
         */
        $this->requestBody = $this->request->input();

        /*
         * Verify webhook request
         */
        //Get shop info from DB
        $this->shopInDb = $this->Shop->findByDomain($_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN']);
        if (empty($this->shopInDb)) {
            $this->addToLog('Domain of the shop - "'. $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN']. '" - is unknown.');
            /*
             * send OK status because this error can't be fixed and
             * we don't want that shopify resend this webhook for this domain
             */
            $this->response->statusCode(200);
            $this->response->send();
            exit;
        }

        // Init PluginCenter to get ApiSecret in _verifyWebhook().
        PluginCenter::init(array(
            'platform' => strtolower(Configure::read('AppConf.app_platform')),
            'plugin' => Configure::read('AppConf.app_name'),
            'user' => $this->shopInDb['Shop']['user_id']
        ));

        // Is Verified?
        $calculatedHmac = $this->_verifyWebhook($this->requestBody, $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256']);
        //Uncomment in production
        if (!$calculatedHmac) {
            $this->addToLog(
                'Webhook not verified: HMAC strings does not match. From request:"'
                . $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256']
            );
            $this->response->statusCode(400);
            $this->response->send();
            exit;
        }
		$this->setApiParams($this->shopInDb);
        $this->addToLog('Webhook successfully verified: HMAC strings are match. From request:"'. $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256']  . '", Calculated: "'.$calculatedHmac.'"' );
        $this->Auth->allow('uninstall_app', 'register_product');
    }
    
    public function index(){
        exit;
    }

    /**
     * Callback for Webhooks: app/uninstall
     */
    public function uninstall_app() {
        if ($this->_uninstallAll()) {
            $this->addToLog( 
                'App unistalled for domain: "'.$_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'].'"'
            );
            $this->response->statusCode(200);
        } else {
            $this->addToLog(
                'Error: App was not unistalled for domain: "'.$_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'].'".'
            );
            $this->response->statusCode(400);
        }
        $this->response->send();
        exit;
    }

    protected function getAllData(){
        return $this->_getData();
    }

    /**
     * Called by Webhooks controller in response to webhook app/uninstalled.
     * Maybe we should remove shop information from our DB.
     */
    protected function  _uninstallAll()
    {
        $shop = $this->shopInDb;
        try {
            if (!empty($shop)) {
                // Remove shop from DB
                $this->Target->setSource('target_products');
                $this->Target->deleteAll(array('shop_id' => $shop['Shop']['id']));
                $this->Target->setSource('target_collections');
                $this->Target->deleteAll(array('shop_id' => $shop['Shop']['id']));
                $this->Shop->delete($shop['Shop']['id']);
                $this->_removeShopFiles($shop);
                // Call action to unistall plugin in Plugin Center
                PluginCenter::init(array(
                    'platform' => strtolower(Configure::read('AppConf.app_platform')),
                    'plugin' => Configure::read('AppConf.app_name'),
                    'shop' => $shop['Shop']['domain']
                ));
                PluginCenter::plugin('uninstall');
            } else {
                $this->addToLog('Error: uninstallation failed - shop data is empty.');
            }
        } catch (Exception $e) {
            $this->addToLog('Error: uninstallation failed'. "\n" . 'Error message: "' . $e->getMessage() . '".');
            return false;
        }
        return true;
    }

    /**
     * Remove store related JS from Amazon S3
     *
     * @param integer $shopId Shop Id
     *
     * @return bool
     */
    protected function _removeShopFiles($shop)
    {
        $this->Amazon = new Amazon();
        $folder = Configure::read('AppConf.amazonAPI.folderStore');
        $hash = $this->Shop->getHash($shop);
        $path = $folder.$hash;
        try {
            $this->Amazon->remove($path . '.js');
            $this->Amazon->remove($path . '.css');
        } catch (Exception $e) {
            $this->addToLog('Error in deleting store files from Amazon: ' . $e->getMessage());
        }
        return true;
    }

    protected function _verifyWebhook($data, $hmac_header) {
        $credentials = $this->getAppCredentials();
        $calculatedHmac = trim(base64_encode(hash_hmac(
            "sha256", 
            $data, 
            $credentials['client_secret'],
            true
        )));
        
        return ( ($hmac_header == $calculatedHmac) ? $calculatedHmac : false );
    }
    
    protected function _getData() {
        $data = null;
        switch (Configure::read('AppConf.api_format')) {
            case 'json':
                $data = json_decode($this->requestBody, true);
				//$this->addToLog( 'Webhook JSON data: "'.print_r($data, true).'"');
                break;
            default: break;
        }
        return $data;
    }
    
    protected function sendStatus($status = self::HTTP_STATUS_OK, $isExit = true)
    {
        $this->response->statusCode($status);
        $this->response->send();
        if ($isExit) exit;
    }
    
    protected function logRequest(){
        $this->logHttpHeaders();
        $this->logRequestData();
    }
    
    protected function logHttpHeaders(){
        $this->addToLog('Headers data($_SERVER): "'.print_r($_SERVER, true).'".');
    }
    
    protected function logRequestData(){
        $this->addToLog('Request data(php/input): "'.print_r($this->requestBody, true).'".');
    }
    
    protected function logData($data){
        $this->addToLog('Request data(Array): "'.print_r($data, true).'".');
    }
    
    protected function addToLog($message){
        $this->log($message, 'webhooks');
    }
}