<?php

App::uses('ShopifySetupController', 'Controller');

/**
 * Install our asset, Webhooks and make other required actions.
 *
 * @author user
 */
class SetupController extends ShopifySetupController
{
    public $name = 'Setup';
    public $autoLayout = false;
    public $autoRender = false;

    protected $shopHash;

    // Turn off Assets component
    public $noAssets = true;


    /**
     * List of theme templates and assets(static CSS, JS and images) to upload.
     * Content of the files is always the same and don't depends on any conditions.
     * Location of the files depends on shop's main theme.
     * If template file will not be found in directory "LiquidTemplates/{themename}/{template}"
     * then default file placed in "LiquidTemplates/default/{template}" will be used.
     *
     * Every array item includes:
     *  template - Name of the .ctp template.
     *  assetDir - Directory of the .liquid file in Shopify theme.
     *
     * Optional item fields:
     *  update   - Should it be updated if user pressed "Update theme files" button in app administration page.
     *             Default value is "true".
     *  data     - Associated array of the template vars where key is a variable name and value is a value.
     *
     * @return array List of files
     */
    protected function getAssetList()
    {
        $fileUrl = $this->appConfig['amazonAPI']['commonJsUrl'];
        $data = array(
            'shopHash' => $this->getShopHash(),
            'commonJsFileUrl' => $fileUrl
        );
        $assets = array(
            array(
                'template' => 'product-snippet',
                'assetDir' => 'snippets',
                'data' => $data
            )
        );

        return $assets;
    }

    /**
     * List of templates in which snippets will be added
     *
     * Returned array can contain any or every of the following key => values.
     * $templates = array(
     *     'layouts'   => array(),
     *     'templates' => array(),
     *     'snippets'  => array()
     * );
     *
     * @return array Grouped list of templates
     */
    protected function getTemplatesList()
    {
        $templates = parent::getTemplatesList();
        $templates['templates'] = array('product');

        return $templates;
    }

    /**
     * Install webhooks, activate shop, insert default email template,
     * insert default settings and generate configuration JS file
     *
     * @param integer $shopId
     */
    protected function installAndActivate($shopId)
    {
        $this->updateUserTemplates();
        $this->installWebHooks();
        //$this->installScriptTags();

        $this->Shop->id = $shopId;
        $this->Shop->activate();

        $this->saveDefaultSettings();
        $this->uploadJsFiles($shopId);
        $this->uploadShopCSS();
        //$this->saveSettingsToMetafields();
    }

    /*
     * Reinstall templates.
     * User can do that manually via administration interface of the app.
     */
    public function reinstall_templates()
    {
        if (empty($this->data)) return false;
        $shop = $this->Shop->findByDomain($this->data['shop']);
        if ($this->data['token'] != $shop['Shop']['token']) return false;
        $this->setApiParams($shop);
        $this->updateUserTemplates(true);
        $this->uploadJsFiles();
        $this->uploadShopCSS();
        //$this->removeScriptTags();
        //$this->saveSettingsToMetafields();

        return true;
    }

    /**
     * Get default settings from app config file and save for the shop.
     *
     * @return bool
     */
    protected function saveDefaultSettings()
    {
        $shop = $this->getShopInDb();
        $shopId = $shop['Shop']['id'];
        $exists = $this->checkExistence('Shop', array('Shop.id' => $shopId, 'Shop.settings !=' => ''));
        if ($exists) {
            return true;
        }
        try {
            $data = array('Shop' => array('id' => $shopId, 'settings' => Configure::read('AppConf.default_settings')));
            $res = $this->Shop->save($data);
        } catch (Exception $e) {
            $res = false;
            $this->log('Shop ID: ' . $shopId .' | Default settings saving failed! Error: ' . $e->getMessage());
        }

        return $res;
    }

    /**
     * Upload App configuration JS file to Amazon
     *
     * @return bool
     */
    protected function uploadJsFiles($shopId = null)
    {
        return $this->uploadShopConfig($shopId);
    }

    /**
     * Gets hash for shop.
     *
     * @return string
     */
    protected function getShopHash()
    {
        if (empty($this->shopHash)) {
            $shop = $this->getShopInDb();
            $this->shopHash = $this->Shop->getHash($shop);
        }

        return $this->shopHash;
    }

}