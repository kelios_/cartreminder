<?php

App::uses( 'AppController', 'Controller' );

class MainController extends AppController
{
	public $name = 'Main';

	public function beforeFilter(){
		$this->Auth->allow();
	}

	public function session_expired()
	{
		//$this->autoRender = false;
		$this->autoLayout = false;
		$this->Session->delete( Configure::read( 'AppConf.session_key' ) . '.logged_domain' );
		$this->Session->delete( 'Auth.User' );
		$this->Session->delete( 'Auth.redirect' );
		$this->Session->renew();

		if ( !empty($this->request->query['shop']) )
		{
            $app_id = Configure::read('AppConf.app_id');
            $this->set('shopDomain', $this->request->query['shop']);
            $this->set(
            	'authUrl',
            	Configure::read('AppConf.plugins_center') . "/plugin/{$app_id}?shop={$this->request->query['shop']}"
            );
            /*$this->redirect(
                Configure::read('AppConf.plugins_center') . "/plugin/{$app_id}?shop={$this->request->query['shop']}"
            );*/
		}
		else $this->redirect(Configure::read( 'AppConf.appstore_url' ));
	}
}