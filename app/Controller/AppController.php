<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.
 */

App::uses('Controller', 'Controller');
//App::uses('HttpSocket', 'Network/Http');

/**
 * Load Shopify API library
 */
App::uses('ShopifyApi', 'Lib');

/**
 * Load library to work with Amazon SDK
 */
App::uses('Amazon', 'Lib');

/**
 * Load PluginCenter class
 */
App::uses('PluginCenter', 'Lib/external-api');
App::uses('ShopifyPrivateApi', 'Lib');

/**
 * Load App config
 */
Configure::load('app_config');

/**
 * Application Controller.
 * All controllers extends it.
 *
 * @package       Discount Reminder
 *
 * @property ShopifyApi $api
 * @property Shop $Shop
 * @property FlashComponent $Flash
 * @property Amazon $Amazon
 */
class AppController extends Controller {

    public $components = array('Session', 'Flash', 'RequestHandler', 'ProductSelector.Product', 'Auth' => array(
        'loginAction' => array('controller' => 'main', 'action' => 'session_expired'),
    ));

    public $helpers = array('Html', 'Session', 'StyleEditor.Utils', 'Misc', 'Form' => array('className' => 'SeaffForm')); //'Form' => array('className' => 'StyleEditor.ThemeForm'),
    public $uses = array('Shop', 'Target');

    public $layout = 'shopify';

    protected $api;
    protected $shopInfo;
    protected $shopInDb = array();
    protected $currentTheme = null;
    protected $shopHash;
    protected $platform;
    protected $sessionKey;

    /**
     * Check authentication and start installation if necessary.
     */
    public function beforeFilter() {
        parent::beforeFilter();

        if(!empty($this->request->query['shop'])){
            $this->Auth->loginAction['?'] = array('shop'=>$this->request->query['shop']);
        }

        $this->appConfig = Configure::read('AppConf');
        $this->platform = Configure::read('AppConf.app_platform');
        $this->sessionKey = Configure::read('AppConf.session_key');

        // Auth by PC
        $this->Auth->authenticate = array(
            'Spurit' => array(
                'key'         => Configure::read('AppConf.crypt_key'),
                'cookieNames' => Configure::read('AppConf.cookie_names'),
            )
        );

        if ($this->name !== 'CakeError') {
            $this->Auth->login();
        }

        $user = $this->Auth->user();

        if ($user) {
            PluginCenter::init(array(
                'platform' => strtolower($this->platform),
                'plugin' => Configure::read('AppConf.app_name'),
                'user' => $user['id'],
            ));
            $this->Amazon = new Amazon();
            $shop = $this->getShopInDb();

            if ( !empty($this->request->query['shop']) && empty($this->request->query['token']) && empty($shop) ) {
                $this->redirect($this->Auth->loginAction);
            }

            if (empty($shop) && ($this->name != 'Setup') && ($this->name != 'Charges') && ($this->name != 'CakeError') ) {
                $this->setInstallData();
                $this->redirect(array('controller' => 'setup'));
            }

            if ( !empty($this->request->query['shop']) && ($shop['Shop']['domain'] != $this->request->query['shop']) ) {
                $this->redirect($this->Auth->loginAction);
            }

            if (!empty($shop)) {
                $this->setApiParams($shop);
                if(!$this->isPaid($user['id']) && $this->name != 'Payments'){
                    $this->redirect(array('controller' => 'payments'));
                }
                //$this->getShopLevel(true);
            }
        }
        $this->Auth->allow('index', 'display', 'uninstall');
    }

    /**
     * Get and set view vars for top menu
     */
    public function beforeRender()
    {
        parent::beforeRender();

        $this->set('app_name', Configure::read('AppConf.app_name'));

        $showReviewMessage = false;
        $user = $this->Auth->user();

        if (!empty($user)) {
           // $this->set('menu', PluginCenter::template('menu'));

            $this->set('user', $this->Auth->user());

            $shop = $this->getShopInDb();

            if (!empty($shop)) {
                $this->set('shop', $shop);
                $this->set('shopInfo', $this->getShopInfo());

                if (
                    Configure::read('AppConf.show_review_message')
                    && (strtotime($shop['Shop']['created']) > strtotime(Configure::read('AppConf.review_message_date')))
                ) {
                    $showReviewMessage = true;
                }

                $appCredentials = $this->getAppCredentials();
                $this->set('apiKey', $appCredentials['api_key']);
            }
        }

        // set the value according to how user logged: through PC or Shopify,
        // if from PC - value must be false
        if ( $this->isEmbedded() ) {
            $this->set('allowShopifyRedirect', true);
        } else {
            $this->set('allowShopifyRedirect', false);
        }

        //Shop views in iframe in simple layout
        if (!empty($this->request->named['popup'])) {
            $this->layout = 'popup';
            Configure::write('debug', 0);
        }

        $this->set('showReviewMessage', $showReviewMessage);

    }

    /**
     * Action for PluginCenter.
     * User redirected to the action after initial setup in PC.
     */
    public function install()
    {
        $this->setInstallData();

        // Redirect to Setup
        $this->redirect(array('controller'=>'setup'));
    }

    public function uninstall($id = null)
    {
        $this->log("Id: '$id'", 'uninstall');

        exit();
    }

    protected function isPaid($userId = null, $redirect = true)
    {
        if (Configure::read('AppConf.auth_type') == CONFIG_AUTH_TYPE_TEST) {
            return true;
        }
        if (!empty($userId)) {
            PluginCenter::init( array(
                'user'      => $userId
            ));
        } else {
            $this->log('Error in AppController->isPaid():  User Id is empty.');
            return false;
        }

        $response = PluginCenter::payment( 'is-paid' );

        if ( ($response != 'paid') && $redirect ) {
            if($this->name != 'Payments')
            $this->redirect(array('controller' => 'payments'));
        } elseif ($response != 'paid') {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get Domain Name
     * @return string Domain of the store in *.myshopify.com format.
     */
    public function getLoggedDomain() {
        if ($this->Session->check($this->sessionKey . '.logged_domain')) {
            return $this->Session->read($this->sessionKey . '.logged_domain');
        }

        return false;
    }

    /**
     * Prepare data for Setup controller
     *
     */
    protected function setInstallData()
    {
        if (empty($this->params['url']['shop']) || empty($this->params['url']['token'])) {
            $this->redirect($this->Auth->loginAction);
        }

        $this->Session->write($this->sessionKey . '.shopInfo', array(
            'domain' => $this->params['url']['shop'],
            'token'  => $this->params['url']['token'],
        ));
//        $this->Session->write($this->sessionKey.'.logged_domain', $this->params['url']['shop']);
    }

    /**
     * Gets store's information from Shopify.
     *
     * @param bool $reset Reset or not store's information saved in session.
     *
     * @return array One-dimension associated array.
     */
    protected function getShopInfo($reset = false)
    {
        if (empty($this->shopInfo) || $reset) {
            $key = $this->sessionKey . '.' . $this->getShopId() . '.shopifyInfo';
            if (!$this->Session->check($key)) {
                $shopInfo = $this->api->getShopInfo();
                if ( !empty($shopInfo->money_format) ) {
                    $shopInfo->money_format = strip_tags($shopInfo->money_format);
                }
                $this->Session->write($key, $shopInfo);
            }

            $this->shopInfo = $this->Session->read($key);
        }

        return $this->shopInfo;
    }

    /**
     * Set Shopify API credentials for the store.
     *
     * @param array $shop Data of the Shop model.
     *
     * @return bool
     */
    protected function setApiParams($shop)
    {
        $this->setApi(
            $shop['Shop']['domain'],
            $shop['Shop']['token']
        );

        return true;
    }

    /**
     * Gets API key and secret from Plugin Center
     *
     * @return array
     */
    protected function getAppCredentials()
    {
        $sessionKey = $this->sessionKey . '.' . $this->getShopId() . '.appCredentials';
        if ($this->Session->check($sessionKey)) {
            $data = $this->Session->read($sessionKey);
        } else {
            $raw = PluginCenter::auth( 'credentials' );
            $data = json_decode($raw, true);

            if (empty($data['api_key']) && !empty($data['client_id'])) {
                $data['api_key'] = $data['client_id'];
            }
            if (empty($data['api_secret']) && !empty($data['client_secret'])) {
                $data['api_secret'] = $data['client_secret'];
            }
            $this->Session->write($sessionKey, $data);
        }

        return $data;
    }

    /**
     * Gets ID of the shop in app's db.
     *
     * @param bool $selectInactive Select store with flag is_active = 0 or not.
     *
     * @return integer|bool
     */
    protected function getShopId($selectInactive = false)
    {
        $shop = $this->getShopInDb($selectInactive);

        if ($shop) {
            return $shop['Shop']['id'];
        }

        return false;
    }

    /**
     * Find shop by User Id and save it in controller's field
     *
     * @param bool $selectInactive Select or not shops with is_active = 0
     *
     * @return array
     */
    protected function getShopInDb($selectInactive = false) //$selectInactive = false
    {
        if (empty($this->shopInDb)) { //$this->shopInDb = array() => true
            // Get the current user
            $user = $this->Auth->user();
            //$user = 
            $this->setShopInDb($user['id'], $selectInactive);
        }

        return $this->shopInDb;
    }

    /**
     * Save shop in controller's field
     *
     * @param integer $userId
     * @param bool $selectInactive Select or not shops with is_active = 0
     */
    protected function setShopInDb($userId, $selectInactive = false)
    {
        $this->Shop->recursive = -1;
        if ($selectInactive) {
            $this->shopInDb = $this->Shop->findByUserId($userId);
        } else {
            $this->shopInDb = $this->Shop->findByUserIdAndIsActive($userId, 1);
        }
    }

    /**
     * Check if shop install app before required date.
     *
     * @param array $shop
     *
     * @return bool
     */
    protected function isOldCustomer($shop)
    {
        return ( strtotime($shop['Shop']['created']) < strtotime(Configure::read('AppConf.upgrade_app_date')) );
    }

    /**
     * Gets shop's settings from db
     *
     * @return array
     */
    protected function getSettings()
    {
        $shop = $this->getShopInDb();
        $settings = $shop['Shop']['settings'];
        return $settings;
    }

    /**
     * Setup Shopify Client
     *
     * @param string $domain
     * @param string $token
     */
    protected function setApi($domain, $token)
    {
        if ( empty($this->api) ) {
            $this->api = new ShopifyApi($domain, $token);
        } else {
            $this->api->init($domain, $token);
        }
    }

    protected function getShopCondition($modelName = null)
    {
        $shop = $this->getShopInDb();

        return array(
            (empty($modelName) ? '' : $modelName . '.' ) . 'shop_id' => $shop['Shop']['id']
        );
    }

    /**
     * Upload App configuration JS file to Amazon
     *
     * @param integer $shopId Id of the shop in DB
     *
     * @return bool
     */
    protected function uploadShopConfig($shopId = null)
    {
        if (empty($shopId)) $shop = $this->getShopInDb();
        else $shop = $this->Shop->findById($shopId);
        $settings = $shop['Shop']['settings'];
        $settings['targets'] = $this->Target->getTargets($shop['Shop']['id']);

        if (!empty($settings)) {
            $shopHash = $this->Shop->getHash($shop);
            $settingsJs = 'var HUBConfig = ' . json_encode($settings);

            return $this->Amazon->upload(
                Configure::read('AppConf.amazonAPI.folderStore') . $shopHash . '.js',
                $settingsJs,
                'application/javascript'
            );
        }

        return false;
    }

    /**
     * Upload Shop's Custom CSS file to Amazon
     *
     * @param integer $shopId Id of the shop in DB
     *
     * @return bool
     */
    protected function uploadShopCSS($shopId = null)
    {
        if (empty($shopId)) $shop = $this->getShopInDb();
        else $shop = $this->Shop->findById($shopId);
        $style = empty($shop['Shop']['style'])? Configure::read('AppConf.default_styles') : $shop['Shop']['style'];

        $shopHash = $this->Shop->getHash($shop);
        $styles = $this->renderTemplate('Settings/custom_css.css', $style);

        return $this->Amazon->upload(
            Configure::read('AppConf.amazonAPI.folderStore') . $shopHash . '.css',
            $styles,
            'text/css'
        );
    }

    protected function setPluginCenter()
    {
        $shop = $this->getShopInfo();
        PluginCenter::platform('shopify');
        PluginCenter::shop($shop['domain']);
        PluginCenter::plugin(Configure::read('AppConf.app_name'));
        
//        $this->isPaid();
    }

    /**
     * Render .ctp template as a string. Template have to be placed under View/Elements directory.
     *
     * @param $template File name to render. Relative to View/Elements directory.
     * @param array $data Data for template
     *
     * @return string
     */
    protected function renderAssetFromTemplate($template, $data = array())
    {
//        $data['namespaceMf'] = Configure::read('AppConf.metafields_ns');

        return $this->renderTemplate($template, $data);
    }

    protected function getMainThemeName()
    {
        if (empty($this->currentTheme)) {
            $theme = $this->api->getMainTheme();
            $this->currentTheme = preg_replace('/\s+/', '_', strtolower($theme->name));
        }

        return $this->currentTheme;
    }

    /**
     * Render template from the Elements directory
     *
     * @param string $template Path to the file relative to View/Elements directory
     * @param array $data Data to use in template
     * @return string Rendered template
     */
    protected function renderTemplate($template, $data = array())
    {
        $view = new View($this);
        $view->set($data);
        $view->viewPath = 'Elements';

        // Grab output into variable without the view actually outputting!
        return $view->render($template, false);
    }

    /**
     * Gets settings from app_config file.
     *
     * @param string $key If set value for this key will be returned. If not set settings array will be returned.
     *
     * @return string|array
     */
    protected function getDefaultSettings($key = null)
    {
        if ($key) {
            $res = Configure::read('AppConf.default_settings.' . $key);
        } else {
            $res = Configure::read('AppConf.default_settings');
        }
        return $res;
    }

    /**
     * Send error 400 response to client.
     */
    protected function _sendErrorStatus() {
        $this->response->statusCode(400);
        $this->response->send();
    }

    /**
     * Sets error message for Session::flash.
     * @param string $message
     */
    protected function setErrorMessage($message)
    {
        $this->setFlashMessage($message, 'error');
    }

    /**
     *  Sets normal(success) message for Session::flash.
     * @param string $message
     */
    protected function setMessage($message)
    {
        $this->setFlashMessage($message);
    }

    /**
     * Sets message for Session::flash.
     * @param string $message
     * @param string $type
     */
    protected function setFlashMessage($message, $type = 'success')
    {
        // @TODO: Shopify message flashes will be used!
        $this->Flash->set($message, array('element' => $type));
    }

    /**
     * @return string
     */
    protected function getEmbeddedUrl()
    {
        return 'https://'.$this->shopInDb['Shop']['domain'].'/admin/apps/'.str_replace(' ','-',strtolower(Configure::read('AppConf.app_name')));
    }

    /**
     * @return bool
     */
    protected function isProdEnvironment()
    {
        return Configure::read('AppConf.server_type') == 'production';
    }

    /**
     * @return bool
     */
    protected function authThroughPc()
    {
        $user = $this->Auth->user();
        return !empty($_COOKIE['admin-app-logged-' . Configure::read('AppConf.app_id')]);
    }

    /**
     * @return bool
     */
    protected function isEmbedded()
    {
        if($this->isProdEnvironment() && !$this->authThroughPc()){
            return true;
        }
        return false;
    }

    protected function _setShopifyInfo($params)
    {
        $info = (array)$this->api->get('shop', '', $params);
        $info = array_map(function($value){return strip_tags($value);},$info);
        if(!empty($info)) {
            $this->_shopifyInfo = $info;
        }
    }

    protected function _getShopifyInfo($key = false)
    {
        if($key === false) {
            return $this->_shopifyInfo;
        }
        return $this->_shopifyInfo[$key];
    }
}