<?php

App::uses('BaseSetupController', 'Controller');

/**
 * Install our asset, Webhooks and make other required actions.
 *
 * @author user
 *
 * @property AssetsComponent $Assets
 */
abstract class ShopifySetupController extends BaseSetupController
{
    public $name = 'Setup';
    public $uses = array(/*'ProductVariant', 'ShopSetting'*/);
    public $components = array('Assets');
    public $autoLayout = false;
    public $autoRender = false;

    /*
     * Start setup. Prepare all setup data.
     */
    public function index()
    {
        /**
         * Install our asset, Webhooks and make other required actions and save
         * shop info in our DB.
         */
        if ($this->Session->check(Configure::read('AppConf.session_key') . '.shopInfo')) {
            $info = $this->Session->read(Configure::read('AppConf.session_key') . '.shopInfo');
        } else {
            $this->Session->delete(Configure::read('AppConf.session_key').'.logged_domain');
            $this->Session->delete('Auth.User');
            $this->Session->delete('Auth.redirect');
            $this->Session->renew();
            $this->setErrorMessage('Shop info is not found.');
            $this->redirect(array('controller' => 'main', 'action' => 'session_expired'));
        }

        $this->setApi(
            $info['domain'],
            $info['token']
        );

        $this->addUserShop();

        $shop = $this->getShopInDb(true);

        if (!empty($shop)) {
//            if (Configure::read('AppConf.charge_user') != 'free') {
//                $this->redirect(array('controller' => 'charges', 'action' => 'index', $shop['Shop']['id']));
//            }
        $this->setupApp($shop);
        $this->installAndActivate($shop['Shop']['id']);
        } else {
            throw new Exception('Free installation: Shop is unknown!');
        }

        $this->finishSetup();
    }

    /**
     * Setup all data and files and activate store.
     *
     * @param $shop
     */
    protected function setupApp($shop)
    {
        $this->installAndActivate($shop['Shop']['id']);
        $this->log('Activated: ' . $shop['Shop']['domain'], 'installs');
        $this->finishSetup();
    }

    public function setup_paid_app($shopId)
    {
        if (empty($shopId)) {
            throw new Exception('Paid installation: Shop is unknown!');
        }

        $shop = $this->Shop->findById($shopId);
        $this->setApi(
            $shop['Shop']['domain'],
            $shop['Shop']['token']
        );

        $this->setupApp($shop);
    }

    /*
     * Reinstall templates.
     * User can do that manually via administration interface of the app.
     */
    public function reinstall_templates()
    {
        if (empty($this->data)) {
            return false;
        }

        $shop = $this->getShopInDb();
        $this->setApiParams($shop);

        $this->updateUserTemplates(true);

        return true;
    }

    /*
     * User redirected to this page by charges/confirm action if he clicked on link "decline charge"
     */
    public function remove_declined_shop($shopId) {
        $shop = $this->Shop->findById($shopId);

        $this->Shop->delete($shopId);

        $this->redirect('https://'.$shop['Shop']['domain'].'/admin/applications');
    }

    /**
     * Do installation of all data and store activation.
     * @param $shopId
     */
    protected function installAndActivate($shopId)
    {
        $this->updateUserTemplates();
        $this->installWebHooks();
        //$this->installScriptTags();

        $this->Shop->id = $shopId;
        $this->Shop->activate();

        // $this->installShopMetafields();
    }

    /**
     * Remove temporary setup data and redirect to App's homepage.
     */
    protected function finishSetup()
    {
        /**
         * Redirect back to main application url
         */
        $this->Session->delete(Configure::read('AppConf.session_key') . '.shopInfo');
        $this->setMessage('Application installation completed!');
        $this->redirect($this->getStartPage());
    }

    /**
     * Save store data in App's DB.
     * Store saved as inactive. It will be activated after setup all data.
     *
     * @param $info Shop info received as GET params.
     *
     * @return bool
     * @throws BadRequestException
     */
    protected function addUserShop()
    {
        $user = $this->Auth->user();
        $info = $this->Session->read(Configure::read('AppConf.session_key') . '.shopInfo');

        $exists = $this->checkExistence('Shop', array('Shop.domain' => $info['domain']));
        if ($exists) {
            return true;
        }

        /**
         * Save shop in our DB
         */
        $shopData = array();
        $shop = $this->api->getShopInfo();
        if (empty($shop->id)) {
            throw new BadRequestException('Bad Request: shop id is not found.');
        }
        $shopData['shopify_id'] = $shop->id;
        $shopData['name'] = $shop->name;
        $shopData['email'] = $shop->email;
        $shopData['domain'] = $shop->myshopify_domain;
        $shopData['token'] = $info['token'];
        $shopData['user_id'] = $user['id'];
        $shopData['is_active'] = 0;
        // $shopData['shopify_plan'] = $shop->plan_name;

        $this->log('Domain: ' .$shop->myshopify_domain. ' should be installed.' , 'installs');

        return $this->Shop->save(array('Shop' => $shopData));
    }

    /*
     * Create Webhooks for current Shopify shop
     */
    protected function installWebHooks()
    {
        /*
         * Webhooks used by the app should be defined in app_config.php
         */
        $webhooks = Configure::read('AppConf.webhooks');

        try {
            $webhooksData = array();
            $shop = $this->getShopInDb(true);
            foreach ($webhooks as $topic => $callbackUrlArray) {
                $callbackUrl = Router::url($callbackUrlArray, true);
                $webhooksData[] = array(
                    'topic' => $topic,
                    'callbackUrl' => $callbackUrl
                );
                /*$this->api->installWebhook($topic, $callbackUrl);*/
            }
            // Register webhooks in PluginCenter for production server
            PluginCenter::registerWebhooks(array(
                'shop' => $shop['Shop']['domain'],
                'webhooksData' => $webhooksData
            ));
        } catch (Exception $e) {
//            echo "<pre>" . print_r($e->getResponseHeaders(), true) . "</pre>";
//            echo "<pre>" . print_r($e->getResponse(), true) . "</pre>";
        }
    }

    /**
     * Save liquid snippets and include its into liquid templates.
     * Original content of the templates will be saved in backup files.
     *
     * @param boolean $isUpdate False if new installation, true if update.
     */
    protected function updateUserTemplates($isUpdate = false)
    {
        try {
            // Prepare and save snippet files
            $this->uploadAssets($isUpdate);
            // Include snippets into theme templates
            $this->updateMainTemplates();
            // Update theme templates adding some data
            $this->modifyTemplates();

        } catch (ShopifyApiException $e) {
            $shop = $this->getShopInDb(true);
            $this->log('Error in ShopifySetupController.updateUserTemplates: ' . $e->getMessage());
            $this->log('Shop ID: ' . $shop['Shop']['id']);
        }
    }

    /**
     * Upload assets to store's theme.
     *
     * @param bool $isUpdate Update assets if exists.
     */
    protected function uploadAssets($isUpdate = false)
    {
        $assets = $this->getAssetList();
        $themeName = $this->getMainThemeName();

        $this->Assets->uploadAssets($assets, $themeName, $isUpdate);
    }

    /**
     * Backup liquid templates and insert "include" commands into it.
     *
     * @return bool
     */
    protected function updateMainTemplates()
    {
        $namespace = Configure::read('AppConf.metafields_ns');

        $templates = $this->getTemplatesList();

        foreach($templates['layouts'] as $layout) {
            $this->api->editAsset(
                'layout/' . $layout . '.liquid',
                "{% include '{$namespace}-{$layout}-snippet' %}",
                'layout/' . $layout . '-without-' . $namespace . '.liquid',
                true
            );
        }

        foreach($templates['templates'] as $template) {
            $this->api->editAsset(
                'templates/' . $template . '.liquid',
                "{% include '{$namespace}-{$template}-snippet' %}",
                'templates/' . $template . '-without-' . $namespace . '.liquid'
            );
        }

        foreach($templates['snippets'] as $snippet) {
            $this->api->editAsset(
                'snippets/' . $snippet . '.liquid',
                "{% include '{$namespace}-{$snippet}-snippet' %}",
                'snippets/' . $snippet . '-without-' . $namespace . '.liquid'
            );
        }

        return true;
    }

    /**
     * Replace code in liquid templates of the store's main theme.
     *
     * @return bool
     */
    protected function modifyTemplates()
    {
        $themeName = $this->getMainThemeName();

        return $this->Assets->modifyTemplates($themeName);
    }

    /**
     * List of theme templates and assets(static CSS, JS and images) to upload.
     * Content of the files is always the same and don't depends on any conditions.
     * Location of the files depends on shop's main theme.
     * If template file will not be found in directory "LiquidTemplates/{themename}/{template}"
     * then default file placed in "LiquidTemplates/default/{template}" will be used.
     *
     * Every array item includes:
     *  template - Name of the .ctp template.
     *  assetDir - Directory of the .liquid file in Shopify theme.
     *
     * Optional item fields:
     *  update   - Should it be updated if user pressed "Update theme files" button in app administration page.
     *             Default value is "true".
     *  data     - Associated array of the template vars where key is a variable name and value is a value.
     *
     * @return array List of files
     */
    protected function getAssetList()
    {
        $assets = array();

        return $assets;
    }

    /**
     * List of templates in which snippets will be added
     *
     * Returned array can contain any or every of the following key => values.
     * $templates = array(
     *     'layouts'   => array(),
     *     'templates' => array(),
     *     'snippets'  => array()
     * );
     *
     * @return array Grouped list of templates
     */
    protected function getTemplatesList()
    {
        $templates = array(
            'layouts'   => array(),
            'templates' => array(),
            'snippets'  => array()
        );

        return $templates;
    }
}