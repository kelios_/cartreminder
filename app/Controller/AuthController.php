<?php

/**
 * Description of AuthController
 *
 * @author SpurIT <contact@spur-i-t.com>
 */
class AuthController extends AppController
{
    public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->deny();
    }
}